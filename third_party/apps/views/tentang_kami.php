<!DOCTYPE HTML>
<html>
	<head>
	<title><?php echo $html['title']; ?></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <?php echo $html['metaname']; ?>
        <link href="<?php echo base_url() ?>images/favi.png" rel="shortcut icon" type="image/x-icon" />
	<?php echo $combine['css']; ?>
	</head>
	<body onload="StartTimers();" onmousemove="ResetTimers();">
	<div class="gtco-loader"></div>
	<div id="page">
	<div class="page-inner">
	<?php echo $template['navigation'] ?>
	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(images/dana-marin-225091.jpg)">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">

						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Toko Pandai Club</span>
							<h1>Tentang Kami</h1>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="gtco-section border-bottom">
		<div class="gtco-container">
                <div class="row">
                    <div class="col-md-12" style="text-align: justify;">
					<h2>Profile Capital Life Indonesia</h2>
					<p>
                                            PT Capital Life Indonesia adalah bagian dari Capital Financial Group dan merupakan sister company PT Bank Capital Indonesia Tbk. Grup perusahaan nasional ini berfokus pada industri jasa keuangan yang terintegrasi. PT Capital Life Indonesia terdaftar dan diawasi oleh Otoritas Jasa Keuangan (OJK)
                                            Capital Life berkomitmen memberikan pelayanan cepat dan terbaik, mendahulukan kepentingan pemegang polis serta pihak yang berhak memperoleh manfaat. Hal ini sesuai dengan visi dan misi perusahaan. 
                                            Kinerja Capital Life tahun 2015, memberikan efek positif bagi perusahaan yang pada akhirnya Capital Life mendapatkan beberapa penghargaan. Penghargaan yang didapat antara lain “The Best Insurance 2016” dari Majalah Media Asuransi and predikat “Sangat Bagus” atas kinerja keuangan perusahaan asuransi selama tahun 2015 dari Majalah Infobank.
                                         </p>
				</div>
			</div>		</div>
	</div>
	<?php echo $template['footer']?>
	</div>
	</div>
	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	<!-- jQuery -->
	 <?php echo $combine['js']; ?>
        <script>
        // Set timeout variables.
        var timoutWarning = 840000; // Display warning in 14 Mins.
        var timoutNow = 300000; // Timeout in 15 mins.
        var logoutUrl = '<?php echo base_url() ?>home'; // URL to logout page.

        var warningTimer;
        var timeoutTimer;

        // Start timers.
        function StartTimers() {
            warningTimer = setTimeout("IdleWarning()", timoutWarning);
            timeoutTimer = setTimeout("IdleTimeout()", timoutNow);
        }
        // Reset timers.
        function ResetTimers() {
            clearTimeout(warningTimer);
            clearTimeout(timeoutTimer);
            StartTimers();
            $("#timeout").dialog('close');
        }

        // Show idle timeout warning dialog.
        function IdleWarning() {
            $("#timeout").dialog({
                modal: true
            });
        }
    // Logout the user.
    function IdleTimeout() {
        window.location = logoutUrl;
    }
        </script>
                <script type="text/javascript">
        $(document).ready(function () {
            //Disable full page
            $('body').bind('cut copy paste', function (e) {
                e.preventDefault();
            });

            //Disable part of page
            $('#id').bind('cut copy paste', function (e) {
                e.preventDefault();
            });
        });
        </script>
        <script type="text/javascript">function disableselect(n){return!1}function reEnable(){return!0}document.onselectstart=new Function("return false"),document.oncontextmenu=new Function("return false"),window.sidebar&&(document.onmousedown=disableselect,document.onclick=reEnable),window.history.forward(),window.onload=function(){window.history.forward()},window.onunload=function(){};</script>
	</body>
</html>

