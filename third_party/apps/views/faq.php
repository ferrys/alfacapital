<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Toko Pandai Club :: Frequently asked questions</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="PT Capital Life Indonesia adalah bagian dari Capital Financial Group dan merupakan sister company PT Bank Capital Indonesia Tbk. Grup perusahaan nasional ini berfokus pada industri jasa keuangan yang terintegrasi. PT Capital Life Indonesia terdaftar dan diawasi oleh Otoritas Jasa Keuangan (OJK)
              Capital Life berkomitmen memberikan pelayanan cepat dan terbaik, mendahulukan kepentingan pemegang polis serta pihak yang berhak memperoleh manfaat. Hal ini sesuai dengan visi dan misi perusahaan. 
              Kinerja Capital Life tahun 2015, memberikan efek positif bagi perusahaan yang pada akhirnya Capital Life mendapatkan beberapa penghargaan. Penghargaan yang didapat antara lain “The Best Insurance 2016” dari Majalah Media Asuransi and predikat “Sangat Bagus” atas kinerja keuangan perusahaan asuransi selama tahun 2015 dari Majalah Infobank." />
        <meta name="keywords" content=""/>
        <meta name="author" content="Valdo Inc." />
        <link href="<?php echo base_url() ?>images/favi.png" rel="shortcut icon" type="image/x-icon" />
        <!--  //////////////////////////////////////////////////////
       Capital Life-AlfaMart ND
       � Valdo Inc. 2017
       //////////////////////////////////////////////////////
        -->
        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content=""/>
        <meta property="og:image" content=""/>
        <meta property="og:url" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="og:description" content=""/>
        <meta name="twitter:title" content="" />
        <meta name="twitter:image" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:card" content="" />
        <!-- Animate.css<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet"> -->
        <?php echo $combine['css']; ?>

    </head>
    <body onload="StartTimers();" onmousemove="ResetTimers();">
        <div class="gtco-loader"></div>
        <div id="page">
            <div class="page-inner">
                <?php echo $template['navigation'] ?>
                <header id="gtco-header"  role="banner">
                    <!--                    <div class="overlay"></div>
                                        <div class="gtco-container">
                                            <div class="row">
                                                  <div class="col-md-12 col-md-offset-0 text-right">
                                                    <div class="row row-mt-15em">
                                                        <div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
                                                            <span class="intro-text-small"></span>
                                                            <h1>TERIMA KASIH </h1>
                                                            <h5 style="color: #ffffff;">ANDA SUDAH MENDAFTAR SEBAGAI PEMEGANG POLIS ASURANSI SILAHKAN MELAKUKAN PEMBAYARAN DI ALFAMART TERKDEKAT</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>-->
                </header>
                <div></div><br>
                <div class="gtco-section border-bottom">
                    <div class="gtco-container">
                        <!--<button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">Simple collapsible</button>-->
                        <div class="row">
                            <div class="col-md-8 text-left margin_auto">
                                <h2>Frequently asked questions (FAQ)</h2>
                                <p>Berikut merupakan tata cara untuk menjadi member </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12"><a href="#" style="color:#000000; font-weight: bold;" data-toggle="collapse" data-target="#cara_mendaftar">Bagaimana cara mendaftar asuransi Capital Life?</a>
                                <div id="cara_mendaftar" class="collapse">
                                    <ol type="a">
                                        <li> 
                                            <a href="#" class="link" data-toggle="collapse" data-target="#reg_alfa">Pendaftaran di Alfamart</a>
                                            <div id="reg_alfa" class="collapse">
                                                Dengan membawa eKTP <i>(electronic ktp)</i> ke Alfamart terdekat, lalu pendaftaran bisa dilakukan dengan menunjukan eKTP anda. Kasir akan melakukan pengecakan data apakah eKTP yang bersangkutan 
                                                sudah pernah daftar atau belum, lalu member bisa langsung membayar untuk iuran atau premi pertama yang akan aktif selama 6 bulan
                                            </div>
                                            <br>
                                        </li>
                                        <li>
                                            <a href="#" class="link" data-toggle="collapse" data-target="#reg_web">Pendaftaran di Website</a>
                                            <div id="reg_web" class="collapse">
                                                Untuk <a style="color: #003399; font-weight: bold;" href="<?php base_url() ?>member-register">Registrasi</a> di web 
                                                <a href="<?php base_url() ?>">www.tokopandai.club</a>, setelah selesai mendaftar maka calon member harus membayar di alfamart terdekat.
                                            </div>
                                        </li>
                                    </ol>
                                </div><br>
                                <a href="#" class="link" style="color:#000000 ; font-weight: bold;" data-toggle="collapse" data-target="#pembayaran">Bagaimana cara membayar?</a>
                                <div id="pembayaran" class="collapse">
                                    Untuk Saat ini pembayaran bisa di lakukan di Alfamart terdekat
                                </div><br>
                                <a href="#" class="link" style="color:#000000 ; font-weight: bold;" data-toggle="collapse" data-target="#perpanjangan">Bagaimana jika masa aktif habis?</a>
                                <div id="perpanjangan" class="collapse">
                                    Jika masa aktif asuransi anda habis maka anda bisa memperpanjang kembali di alfamart terdekat, Hanya dengan membawa E-ktp anda.
                                </div>
                                <br><br>

                                <a href="#" class="link" style="font-weight: bold;" data-toggle="collapse" data-target="#siapahalodoc">Siapa itu Halodoc?</a>
                                <div id="siapahalodoc" class="collapse">
                                    Halodoc merupakan aplikasi kesehatan yang memberikan solusi kesehatan lengkap dan terpercaya untuk memenuhi kebutuhan kesehatan kamu dan keluarga dengan menawarkan berbagai layanan yaitu Hubungi Dokter, Apotik Antar & Lab Service.
                                    Untuk informasi lebih lanjut silahkan kunjungi website kami di <a target="_blank" href="https://www.halodoc.com/">www.halodoc.com</a>
                                </div>
                                <br>

                                <a href="#" style="font-weight: bold;"  class="link" data-toggle="collapse" data-target="#mendapathalodoc">Bagaimana cara mendapatkan keuntungan dari Halodoc?</a>
                                <div id="mendapathalodoc" class="collapse">
                                    Registrasi asuransi kamu di tokopandai.club
                                    Download aplikasi Halodoc di sini (hyperlink di sini: <a target="_blank" href="https://ch5xj.app.goo.gl/JaiN">https://ch5xj.app.goo.gl/JaiN</a>)
                                    Tunggu SMS masuk dari Halodoc untuk mendapatkan voucher yang bisa digunakan di fitur “Hubungi Dokter”. Halodoc akan mengirimkan SMS secara berkala kepada nasabah yang sudah teregistrasi di tokopandai.club

                                </div>
                                <br>

                                <a href="#" style="font-weight: bold;" class="link" data-toggle="collapse" data-target="#keuntunganhalodoc">Apa saja keuntungan yang bisa didapatkan dari Halodoc?</a>
                                <div id="keuntunganhalodoc" class="collapse">
                                    GRATIS 4 x konsultasi kesehatan dengan dokter spesialis menggunakan voucher dari Halodoc
                                    <br>GRATIS konsultasi kesehatan dengan dokter umum yang telah standby 24/7 <br>
                                    GRATIS ongkos kirim untuk setiap pembelian obat/vitamin ke tempat tujuan kamu
                                </div>
                                <br>
                            </div> 
                        </div>
                    </div>
                    <!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/hiGyXqaEFtI" frameborder="0" allowfullscreen></iframe>-->
                    <?php echo $template['footer'] ?>
                </div>
            </div></div>
        <div class="gototop js-top">
            <a href="#" class="js-gotop"><img src="<?php echo base_url() ?>images/icon-top.png" width="50px"></a>
        </div>
        <!-- jQuery -->
        <?php echo $combine['js']; ?>
    </body>
</html>