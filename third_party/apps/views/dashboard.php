<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Toko Pandai Club :: Profile</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="PT Capital Life Indonesia adalah bagian dari Capital Financial Group dan merupakan sister company PT Bank Capital Indonesia Tbk. Grup perusahaan nasional ini berfokus pada industri jasa keuangan yang terintegrasi. PT Capital Life Indonesia terdaftar dan diawasi oleh Otoritas Jasa Keuangan (OJK)
                                            Capital Life berkomitmen memberikan pelayanan cepat dan terbaik, mendahulukan kepentingan pemegang polis serta pihak yang berhak memperoleh manfaat. Hal ini sesuai dengan visi dan misi perusahaan. 
                                            Kinerja Capital Life tahun 2015, memberikan efek positif bagi perusahaan yang pada akhirnya Capital Life mendapatkan beberapa penghargaan. Penghargaan yang didapat antara lain “The Best Insurance 2016” dari Majalah Media Asuransi and predikat “Sangat Bagus” atas kinerja keuangan perusahaan asuransi selama tahun 2015 dari Majalah Infobank." />
        <meta name="keywords" content=""/>
        <meta name="author" content="Valdo Inc." />
        <!--  //////////////////////////////////////////////////////
       Capital Life-AlfaMart ND
       � Valdo Inc. 2017
       //////////////////////////////////////////////////////
        -->
        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content=""/>
        <meta property="og:image" content=""/>
        <meta property="og:url" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="og:description" content=""/>
        <meta name="twitter:title" content="" />
        <meta name="twitter:image" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:card" content="" />
        <link href="<?php echo base_url() ?>images/favi.png" rel="shortcut icon" type="image/x-icon" />
        <style>
            [data-tooltip] {
                position: relative;
                z-index: 2;
                cursor: pointer;
            }

            /* Hide the tooltip content by default */
            [data-tooltip]:before,
            [data-tooltip]:after {
                visibility: hidden;
                -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
                filter: "progid: DXImageTransform.Microsoft.Alpha(Opacity=0)";
                opacity: 0;
                pointer-events: none;
            }

            /* Position tooltip above the element */
            [data-tooltip]:before {
                position: absolute;
                bottom: 40%;
                left: 50%;
                margin-top: 50px;
                margin-left: -80px;
                padding: 7px;
                width: 100px;
                -webkit-border-radius: 3px;
                -moz-border-radius: 3px;
                border-radius: 3px;
                background-color: #000;
                background-color: hsla(0, 0%, 20%, 0.9);
                color: #fff;
                content: attr(data-tooltip);
                text-align: center;
                font-size: 14px;
                line-height: 1.2;
            }

            /* Triangle hack to make tooltip look like a speech bubble */
            [data-tooltip]:after {
                position: absolute;
                bottom: 150%;
                left: 50%;
                margin-left: -5px;
                width: 0;
                border-top: 5px solid #000;
                border-top: 5px solid hsla(0, 0%, 20%, 0.9);
                border-right: 5px solid transparent;
                border-left: 5px solid transparent;
                content: " ";
                font-size: 0;
                line-height: 0;
            }

            /* Show tooltip content on hover */
            [data-tooltip]:hover:before,
            [data-tooltip]:hover:after {
                visibility: visible;
                -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
                filter: "progid: DXImageTransform.Microsoft.Alpha(Opacity=100)";
                opacity: 1;
            }
        </style>
        <?php echo $combine['css']; ?>
    </head>
    <body onload="StartTimers();" onmousemove="ResetTimers();">
        <div class="gtco-loader"></div>
        <div id="page">
            <div class="page-inner">
                <?php echo $template['navigation'] ?>
                <header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(<?php echo base_url() ?>images/jessica-ruscello-196422.jpg)">
                    <div class="overlay"></div>
                    <div class="gtco-container">
                        <div class="row">
                            <div class="col-md-12 col-md-offset-0 text-left">
                                <div class="row row-mt-15em">
                                    <div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
                                        <span class="intro-text-small">Toko Pandai Club</span>
                                        <h1>Membership Area</h1>	
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <div class="gtco-section border-bottom">
                    <div class="gtco-container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-12 animate-box">
                                    <h3>Profile</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form action="<?php echo base_url() ?>update_cos" method="post" runat="server"  onsubmit="ShowLoading()" class="form-horizontal">
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="nopolis">Policy No:</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" disabled value="<?php echo $this->session->userdata('policy_no'); ?>" name="policy_no" class="form-control" id="policy_no">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="nopolis">KTP No:</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" disabled value="<?php echo $this->session->userdata('ktp_no'); ?>" name="ktp_no" class="form-control" id="ktp_no">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="cname">Nama:</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" required value="<?php echo $cos[0]['cname'] ?>" name="cname" class="form-control" id="cname" placeholder="Nama Polis">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="nopolis">Tgl Lahir:</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" disabled value="<?php echo tanggalindo($cos[0]['dob']); ?>" name="dob" class="form-control" id="dob">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="age">Usia:</label>
                                                            <div class="col-sm-10">
                                                                <input type="text"  minlength="2" disabled onkeypress="return isNumberKey(event)" min="18" max="65" maxlength="2" name="age" value="<?php echo $cos[0]['age']; ?>" class="form-control" id="age" placeholder="Usia">
                                                            </div>
                                                        </div>									    
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2"  for="phone_no">Phone No:</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" required onkeypress="return isNumberKey(event)" name="phone_no" value="<?php echo $cos[0]['phone_no']; ?>" class="form-control" id="phone_no" placeholder="Phone No">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="email">Email:</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" required name="email" value="<?php echo $cos[0]['email']; ?>" class="form-control" id="email" placeholder="Email">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="adrress">Alamat:</label>
                                                            <div class="col-sm-10">
                                                                <textarea name="address" required class="form-control" rows="5" id="address"><?php echo $cos[0]['address']; ?></textarea>
                                                            </div>
                                                        </div>

                                                        <?php if ($cos[0]['cname'] == "" OR $cos[0]['phone_no'] == "" OR $cos[0]['email'] == "" OR $cos[0]['address'] == "") { ?>
                                                            <div class="form-group">
                                                                <div class="col-sm-offset-2 col-sm-10">
                                                                    <button type="submit" id="disable" class="btn btn-default">Submit</button>
                                                                </div>	
                                                            </div>  <?php } ?>
                                                    </form>
                                                </div>
                                                <?php if ($cos[0]['cname'] != "" && $cos[0]['phone_no'] != "" && $cos[0]['email'] != "") { ?>
                                                    <a download href="<?php echo base_url() ?>update_cos/create_pdf">Download E-polis <img width="20" src="<?php echo base_url() ?>/images/pdf.png"></a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo $template['footer'] ?>
            </div>
        </div>
        <div class="gototop js-top">
            <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
        </div>
        <!-- jQuery -->
        <?php echo $combine['js']; ?>
        <script type="text/javascript">
            $(function () {
                $.fn.datepicker.defaults.format = "yyyy-mm-dd";
                $(".datepicker").datepicker({
                    startDate: '-3d'
                });
                //alert("hdhdhd");
            });
            $("#usia").click(function ()
            {
                var stdob = $("#tgl_lahir").val();
                //alert("d -->"+stdob);
                var st_url = 'data/index.php/json/age';
                //alert(st_url);	       	
                //var dataString = $.toJSON(stdob);	
                //alert(st_url);
                $.post(st_url, {tgl_lahir: stdob},
                        function (res, status) {
                            $("#usia").val(res);
                            //alert(res);
                        });
            });
        </script>


        <script type="text/javascript">




            $(document).ready(function () {
                //Disable full page
                $('body').bind('cut copy paste', function (e) {
                    e.preventDefault();
                });

                //Disable part of page
                $('#id').bind('cut copy paste', function (e) {
                    e.preventDefault();
                });
            });
            $(":submit").closest("form").submit(function () {
                $(':submit').attr('disabled', 'disabled');
            });
        </script>
        <script type="text/javascript">

            function ShowLoading(e) {
                var div = document.createElement('div');
                var img = document.createElement('img');
                img.src = '<?php echo base_url() ?>images/loading.gif';
                div.innerHTML = "Please Wait...<br />";
                div.style.cssText = 'position: fixed;  top: 50%; left: 400px; z-index: 5000; width: 422px; text-align: center;';
                div.appendChild(img);
                document.body.appendChild(div);
                window.onscroll = function () {
                    window.scrollTo(0, 900);
                };
                return true;
                // These 2 lines cancel form submission, so only use if needed.
                //window.event.cancelBubble = true;
                //e.stopPropagation();
            }
        </script>
        <script>
            var timoutWarning = 840000;
            var timoutNow = 160000;
            var logoutUrl = '<?php echo base_url() ?>home';
            var warningTimer;
            var timeoutTimer;
            
            function StartTimers() 
            {
                warningTimer = setTimeout("IdleWarning()", timoutWarning);
                timeoutTimer = setTimeout("IdleTimeout()", timoutNow);}
            function ResetTimers() {
                clearTimeout(warningTimer);
                clearTimeout(timeoutTimer);
                StartTimers();$("#timeout").dialog('close');}
            function IdleWarning() {$("#timeout").dialog({modal: true});}
            function IdleTimeout() {window.location = logoutUrl;}</script>
        <script type="application/javascript">function isNumberKey(e){var h=e.which?e.which:event.keyCode;return h>31&&(48>h||h>57)?!1:!0}</script>
    </body>
</html>

