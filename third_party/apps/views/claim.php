<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $html['title']; ?></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <?php echo $html['metaname']; ?>
        <link href="<?php echo base_url() ?>images/favi.png" rel="shortcut icon" type="image/x-icon" />
	<?php echo $combine['css']; ?>
	</head>
	<body onload="StartTimers();" onmousemove="ResetTimers();">
	<div class="gtco-loader"></div>
	<div id="page">
	<div class="page-inner">
	<?php echo $template['navigation'] ?>
<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(images/jill-heyer-1798.jpg)">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">
						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Toko Pandai Club</span>
							<h1>Prosedur Klaim</h1>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="gtco-section border-bottom">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12 animate-box">
					<h3>Prosedur Klaim</h3>
					<div class="row">
						<div class="col-md-12">
                                                    <table class="table table-striped" style="text-align: justify">
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Pengajuan klaim atas manfaat meninggal dunia harus dilaporkan/diajukan ke Penanggung selambat-lambatnya 90 (sembilan puluh) Hari Kalender terhitung sejak tanggal meninggalnya Tertanggung.</td>
                                                    </tr>
                                                     <tr>
                                                        <td>2</td>
                                                        <td>Klaim yang dilaporkan/diajukan setelah atau melebihi jangka waktu sebagaimana dimaksud di atas, maka Klaim dianggap kadaluarsa dan Penanggung tidak bertanggung jawab dan berhak menolak pengajuan Klaim tersebut.</td>
                                                    </tr>
                                                     <tr>
                                                        <td>3</td>
                                                        <td>Pengajuan Klaim meninggal dunia karena Penyakit harus disertai dengan dokumen pendukung sebagai berikut :
                                                            <ol type="a">
                                                            <li>Form pengajuan Klaim dari Pemegang Polis atau Penerima Manfaat; dan</li>
                                                            <li>Copy Polis atau bukti kepesertaan asuransi lainnya dari Tertanggung; dan</li>
                                                            <li>Copy kartu identitas Tertanggung yang masih berlaku (KTP/SIM/Paspor); dan</li>
                                                            <li>Copy kartu identitas diri Pemegang Polis atau Penerima Manfaat yang masih berlaku (KTP/Passpor/SIM); dan</li>
                                                            <li>Copy kartu keluarga atau alat bukti sah bahwa Penerima Manfaat adalah keluarga atau yang telah ditunjuk Pemegang Polis atau Tertanggung; dan</li>
                                                            <li>Surat keterangan meninggal asli/legalisir dari Instansi yang berwenang/Pamong Praja setempat (jika Tertanggung meninggal dunia bukan di Rumah Sakit) atau dari Dokter/Rumah Sakit (jika Tertanggung meninggal di Rumah Sakit); dan</li>
                                                            <li>Surat keterangan kronologis kematian dari Penerima Manfaat (jika Tertanggung meninggal dunia bukan di Rumah Sakit); dan</li>
                                                            <li>Copy catatan/resume medis, seluruh hasil pemeriksaan laboratorium dan radiologi (jika ada); dan</li>
                                                            <li>Surat keterangan pemakaman/kremasi yang dilegalisir dari Instansi yang berwenang; dan</li>
                                                            <li>Surat keterangan dari Kedutaan Besar Republik Indonesia (KBRI) jika Tertanggung meninggal di luar negeri; dan</li>
                                                            <li>Surat penetapan pengadilan dalam hal Tertanggung dinyatakan hilang sesuai dengan ketentuan perundangan yang berlaku.</li></ol>
                                                            </td>
                                                    </tr>
                                                     <tr>
                                                        <td>4</td>
                                                        <td>Apabila diperlukan, Penanggung berhak mengadakan penyelidikan (investigasi) dan memperoleh informasi lebih detail atas Klaim yang diajukan baik itu meminta keterangan medis, hasil otopsi atau visum et repertum serta meminta dokumen tambahan lainnya kepada Pemegang Polis atau Penerima Manfaat atau dari Dokter yang merawat Tertanggung dan Penanggung berhak untuk menunjuk dan menyewa seorang praktisi medis untuk melakukan pemeriksaan terhadap Klaim yang diajukan. Penanggung tidak akan melakukan pembayaran apapun untuk memperoleh keterangan atau laporan medis apapun.</td>
                                                    </tr>
                                                     <tr>
                                                        <td>5</td>
                                                        <td>Dokumen pengajuan Klaim sebagaimana dimaksud di atas, jika dibuat dalam bahasa asing maka harus diterjemahkan ke dalam bahasa Indonesia dan dilakukan oleh penterjemah tersumpah. Biaya-biaya yang timbul sehubungan dengan hal tersebut akan menjadi tanggung jawab pihak pengaju.</td>
                                                    </tr>
                                                     <tr>
                                                        <td>6</td>
                                                        <td>Pengajuan Klaim adalah sah apabila syarat dan ketentuan sebagaimana disebutkan di atas telah dipenuhi dan Penanggung mempunyai hak untuk menolak Klaim yang diajukan apabila syarat dan ketentuan tersebut tidak dipenuhi.</td>
                                                    </tr>
                                                     <tr>
                                                        <td>7</td>
                                                        <td>Penanggung akan membayarkan Manfaat Asuransi setelah syarat dan ketentuan sebagaimana disebutkan di atas telah dipenuhi dan telah disetujui oleh Penanggung.</td>
                                                    </tr>
                                                     <tr>
                                                        <td>8</td>
                                                        <td>Pembayaran Manfaat Asuransi dapat dilakukan melalui pemindahbukuan antar bank (transfer) ke rekening Penerima Manfaat atau dengan cara lain yang ditetapkan oleh Penanggung.</td>
                                                    </tr>
                                                     <tr>
                                                        <td>9</td>
                                                        <td>Penanggung akan membayarkan Manfaat Asuransi selambat-lambatnya 14 (empat belas) Hari Kerja setelah pengajuan klaim disetujui oleh Penanggung.</td>
                                                    </tr>
                                                     </tbody>
                                                     </table>
                                                    <a target="_blank" download href="<?php echo base_url();?>assets/pdf/Prosedur Klaim_Alfa_valdo.pdf">Download Prosedur Klaim.pdf</a><br>
                                                    <a target="_blank" download href="<?php echo base_url();?>assets/pdf/FORM_KLAIM_Meninggal.pdf">Download Form Klaim Meninggal.pdf</a><br>
                                            </div>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
	<?php echo $template['footer']?>
	</div>
	</div>
	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	<!-- jQuery -->
	<?php echo $combine['js']; ?>
        <script>
        // Set timeout variables.
        var timoutWarning = 840000; // Display warning in 14 Mins.
        var timoutNow = 30000; // Timeout in 15 mins.
        var logoutUrl = '<?php echo base_url()?>home'; // URL to logout page.

        var warningTimer;
        var timeoutTimer;

        // Start timers.
        function StartTimers() {
            warningTimer = setTimeout("IdleWarning()", timoutWarning);
            timeoutTimer = setTimeout("IdleTimeout()", timoutNow);
        }
        // Reset timers.
        function ResetTimers() {
            clearTimeout(warningTimer);
            clearTimeout(timeoutTimer);
            StartTimers();
            $("#timeout").dialog('close');
        }

        // Show idle timeout warning dialog.
        function IdleWarning() {
            $("#timeout").dialog({
                modal: true
            });
        }
    // Logout the user.
    function IdleTimeout() {
        window.location = logoutUrl;
    }
        </script>
        <script type="text/javascript">
        $(document).ready(function () {
            //Disable full page
            $('body').bind('cut copy paste', function (e) {
                e.preventDefault();
            });

            //Disable part of page
            $('#id').bind('cut copy paste', function (e) {
                e.preventDefault();
            });
        });
        </script>
        <script type="text/javascript">function disableselect(n){return!1}function reEnable(){return!0}document.onselectstart=new Function("return false"),document.oncontextmenu=new Function("return false"),window.sidebar&&(document.onmousedown=disableselect,document.onclick=reEnable),window.history.forward(),window.onload=function(){window.history.forward()},window.onunload=function(){};</script>
	</body>
</html>

