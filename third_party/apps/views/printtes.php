<?php ob_start(); ?>
<!DOCTYPE html>
<html>
<head>
    <style>
        .col
        {
            float: left;
            width: 20%;
        }
        .last{
            float: right;
            width: 20%;
        }
        .row{
            height: auto;
            overflow: auto;
        }
    </style>
</head>
<body>
    <div style="background-image: url(https://s28.postimg.org/bwk85as3x/image.jpg); background-size: 100% 100%; background-position: center; background-repeat: no-repeat;">
        <?php
        $total = count($list);
        for ($i = 0; $i < $total; $i++) {
            ?>  <br><br><br><br><br><br><br><br><br>
            <div align="center" style="font-weight: bold;">SERTIFIKAT ASURANSI</div>
            <div align="center">Dikeluarkan oleh PT Capital Life Indonesia <br>
                Mengacu pada Polis Induk No: 0200000018</div><br><br>
                <table align="center" style="font-size: 14px">
                <tr>
                    <td>Nomor Peserta </td>
                    <td>: <?php echo $list[0]['policy_no'] ?></td>
                    <td style="padding-left: 50px">Tanggal Mulai Pertanggungan </td>
                    <td>: <?php echo tanggalindo($list[0]['start_date']) ?></td>
                </tr>
                <tr>
                    <td>Nama Tertanggung </td>
                    <td>: <?php echo word_limiter($list[0]['cname'], 2) ?></td>
                    <td style="padding-left: 50px">Tanggal Berakhir Pertanggungan </td>
                    <td>: <?php echo tanggalindo($list[0]['deadline_time']) ?></td>
                </tr>
                <tr>
                    <td>Tanggal Lahir </td>
                    <td>: <?php echo tanggalindo($list[0]['dob']) ?></td>
                    <td style="padding-left: 50px">Mata Uang  </td>
                    <td>: Rupiah</td>
                </tr>
                <tr>
                    <td>Jenis Asuransi Dasar  </td>
                    <td>: <b>Capital Eka Proteksi Group (CAKAP)</b> </td>
                    <td style="padding-left: 50px">Uang Pertanggungan(*) </td>
                    <td>: Rp 15.000.000 (lima belas juta rupiah)</td>
                </tr>
                <tr>
                    <td>Periode Asuransi </td>
                    <td>: 6 (enam) Bulan</td>
                </tr>
                <tr>
                    <td>Premi </td>
                    <td>: Rp. 20.000 (dua puluh ribu rupiah)</td>
                </tr>
            </table>
            <br><br>
            <div style="padding: 10px 100px 10px 100px; font-size: 14px; text-align: justify;">Sertifikat Asuransi ini diterbitkan sebagai bukti kepesertaan asuransi untuk dan atas nama Tertanggung dengan jenis pertanggungan sebagaimana tertera di atas. Sertifikat Asuransi ini tunduk pada syarat-syarat dan ketentuan ketentuan sebagaimana tercantum di dalam Ringkasan Polis Induk, Ketentuan Umum Polis Induk, Ketentuan  Khusus  Polis Induk, Ketentuan  Tambahan  dan  ketentuan  lainnya  (apabila  diadakan)  yang  dilampirkan  atau  dicantumkan  pada  Polis Induk yang secara keseluruhan merupakan satu kesatuan dan menjadi bagian yang tidak terpisahkan dari Polis Induk.</div>
            <div style="padding: 10px 100px 10px 100px; font-size: 14px; text-align: justify;">Syarat dan Ketentuan Umum untuk menjadi Tertanggung pada saat pengajuan  permohonan pertanggungan dengan  dasar <i>Guaranted Acceptance</i> dimana Tertanggung harus dalam keadaan sehat jasmani dan rohani, tidak sedang menjalani Rawat Inap di Rumah Sakit atau Rawat Jalan dan tidak sedang menderita Penyakit akut atau Penyakit menahun. Untuk Usia masuk Tertanggung yang diperkenankan adalah minimum 18 tahun dan maksimum 60 tahun.</div>
            <div style="padding: 10px 100px 10px 100px; font-size: 12px;"><i>(*) Rincian Uang Pertanggungan:  Rp. 9.000.000,- untuk santunan ahli waris dan Rp. 6.000.000,-dalam bentuk voucher belanja.</i></div>
            <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
        <?php } ?></div> 
    <div class="row" style=" background-image: url(https://s1.postimg.org/qbidzc9in/image.jpg); background-size: 100% 100%; background-position: center; background-repeat: no-repeat;">
        <div class="col" style="padding: 90px 0px 25px 85px; font-size: 8px; text-align: justify;">
            
            <b>MANFAAT ASURANSI</b><br>
            Dengan tetap mengindahkan ketentuan-ketentuan lain yang tercantum pada Ketentuan husus serta ketentuan-ketentuan lain di dalam Polis, maka Manfaat Asuransi yang akan Penanggung bayarkan dapat dijelaskan sebagai berikut:
            <ol style="padding:0 0 0 15px;">
                <li> Apabila Tertanggung meninggal dunia dalam masa pertanggungan yang diakibatkan karena Penyakit(bukan karena Kecelakaan) dan pertanggungan masih berlaku,maka Penanggung akan membayarkan Manfaat Asuransi sebesar 100% (seratus persen) Uang Pertanggungan setelah tanggal disetujuinya Klaim meninggal berdasarkan Ketentuan Khusus Polis dan selanjutnya pertanggungan berakhir.</li>
                <li> Meninggal dunianya Tertanggung sebagaimana dimaksud pada poin 1 di atas bukan sebagai akibat hal-hal yang tidak dijamin atau dikecualikan dalam Polis.</li>
            </ol>
            <b>Masa Tunggu</b><br>
            Manfaat Asuransi berlaku Masa Tunggu selama 30 (tiga puluh) Hari Kalender terhitung sejak Tanggal Mulai Pertanggungan untuk risiko meninggal dunia yang disebabkan karena Penyakit apapun. Dalam hal terjadi Klaim atas risiko yang dipertanggungkan dalam periode Masa Tunggu, maka Penanggung berhak untuk menolak Klaim yang diajukan dan tidak membayarkan Manfaat Asuransi apapun kepada Pemegang Polis, dan pertanggungan berakhir pada tanggal Tertanggung mengalami resiko.
           <br><br>
           <b style="margin-top: 10px">HAL-HAL YANG TIDAK DIJAMIN (PENGECUALIAN)</b><br>
            Manfaat Asuransi tidak dapat dibayarkan apabila Tertanggung meninggal dunia  sebagai  akibat  dari  hal-hal tersebut dibawah ini :
            <ol style="padding:0 0 0 15px;">
                <li> Kematian yang terjadi sebelum Tanggal Mulai Pertanggungan; atau </li>
                <li> Kondisi yang telah ada Sebelumnya(Pre-Existing Condition), menurut jangka waktu sebelum Tanggal Mulai Pertanggungan sebagaimana tercantum dalam Ringkasan Polis; atau </li>
                <li> Tindakan melukai dan mencederai diri sendiri, usaha atau tindakan pembunuhan dan percobaan bunuh diri atau tindakan lainnya yang membahayakan diri yang dilakukan dengan maksud jahat atau tidak, dalam keadaan sadar atau tidak sadar, dalam keadaan waras atau tidak waras yang dilakukan oleh Tertanggung atau pihak lain atas permintaan Tertanggung atau Pemegang Polis; atau </li>
                <li> Keikutsertaan dalam suatu kegiatan atau olahraga berbahaya, seperti semua olahraga beladiri (tinju, karate, judo, silat, gulat, kempo, taekwondo, kungfu atau sejenisnya), semua olahraga dirgantara (terjun  payung, terbang layang, parasailing atau sejenisnya),hang gliding, ballooning, panjat tebing, mendaki gunung, semua jenis olah raga kontak fisik, semua perlombaan ketangkasan atau kecepatan yang menggunakan kendaraanbermotor, sepeda, kuda, perahu, pesawat udara atau sejenisnya, berlayar seorang diri, menyelam, 
                    
                   </li>
            </ol>
            <br><br><br><br><br><br>
        </div>
        <div class="col" style="padding: 90px 0px 0px 20px;font-size: 8px; text-align: justify;"><ol start="5" style="padding:0 0 0 15px;">
                arum jeram, ski air, ski es, hockey, 
                    rugby, bungee jumping, surfing  atau olahraga air sejenisnya, memasuki gua-gua atau lubang-lubang yang dalam,  berburu binatang, segala jenis perlombaan yang menyangkut daya tahan dan olahraga berbahaya lainnya dan berisiko tinggi baik resmi maupun tidak resmi; atau 
                <li> Pengaruh  penggunaan  alkohol, obat bius, narkotik dan sejenisnya, termasuk obat-obatan dalam arti yang seluas-luasnya terkecuali zat-zat dan/atau obat-obatan dimaksud dipergunakan atas petunjuk Dokter dan tidak 
                terkait dengan upaya perawatan kecanduan obat (upaya rehabilitasi) atau mengalami gangguan lemah mental/sakit jiwa; atau </li>
                <li> Dengan sengaja ikut serta mengambil bagian dalam suatu tindakan melanggar hukum, tindak pidana kejahatan,  perkelahian  (kecuali  jika  sebagai  orang  yang bertindak mempertahankan diri) dan sejenisnya (termasuk mengendarai kendaraan bermotor tanpa Surat Ijin Mengemudi yang sah dan berlaku); atau
                <li> Tindak kejahatan (pembunuhan) yang dilakukan dengan sengaja, atau kekhilafan besar oleh pihak yang berkepentingan dalam Polis ini dan ahli warisnya; atau
                <li> Keterlibatan sebagai pelaku aktif dalam tindakan terorisme, sabotase, bom, dan/atau huru-hara (SRCC); atau
                <li> Segala  Penyakit yang berkembang akibat dari terinfeksi HIV, atau Penyakit yang timbul  baik  langsung  maupun tidak langsung oleh AIDS (Acquired Immune Deficiency Syndrome) dan/atau komplikasinya (AIDS Related Complex/ARC); atau jenis Penyakit lain yang menyebabkan hilangnya kekebalan tubuh, serta Penyakit kelamin lainnya; atau
                <li> Perang (baik yang dinyatakan atau tidak oleh Pemerintah), invasi, perang saudara, tugas militer, pembajakan, pemogokan, huru-hara,
                     kerusuhan atau pemberontakan, revolusi, kekuatan militer, makar, terorisme, sabotase, perlawanan terhadap Pemerintah, pengambil-alihan kekuasaan dengan kekerasan; atau
                <li> Tertanggung dikenakan hukuman mati berdasarkan keputusan Pengadilan yang telah memiliki kekuatan hukum yang tetap; atau
                <li> Menggunakan alat transportasi yang membawa bahan peledak atau bahan berbahaya lainnya; atau
                <li> Apapun baik langsung maupun tidak langsung karena atau terjadi pada reaksi-reaksi inti atom dan atau nuklir, termasuk namun tidak terbatas kepada radiasi nuklir, ionisasi, fusi, fisi atau pencemaran radioaktif dari setiap bahan nuklir, limbah  nuklir,  bahan  kimia,  reaksi  biologi, gas beracun; atau
                <li> Tindakan aborsi, komplikasi kehamilan dan/atau kelahiran (bagi wanita).
            </ol> 
            <b>PROSEDUR DAN DOKUMEN PENGAJUAN KLAIM</b>
            <ol>
                <li> Pengajuan klaim atas manfaat meninggal dunia harus dilaporkan/diajukan ke Penanggung selambat-lambatnya 90 (sembilan puluh) Hari Kalender terhitung sejak tanggal meninggalnya </li>
               </ol>
        </div>
        <div class="col" style="padding: 90px 0px 0px 20px;font-size: 8px; text-align: justify;">
            <ol style="padding: 13px;" style="padding:0 0 0 15px;" start="2">
                Tertanggung. 
                <li> Klaim yang dilaporkan/diajukan setelah atau melebihi jangka waktu sebagaimana dimaksud pada poin 1 di atas, maka Klaim dianggap kadaluarsa dan Penanggung tidak bertanggung jawab dan berhak menolak pengajuan Klaim tersebut.</li>
            
                <li> Pengajuan Klaim meninggal dunia karena Penyakit harus disertai dengan dokumen pendukung sebagai berikut :</li>
                <ol type="a" style="padding:0 0 0 15px;">
                    <li> Form pengajuan Klaim dari Pemegang Polis atau Penerima Manfaat; dan </li>
                    <li> Copy Polis atau bukti kepesertaan asuransi lainnya dari Tertanggung; dan</li>
                    <li> Copy bukti pembayaran Premi; dan</li>
                    <li> Copy kartu identitas Tertanggung yang masih berlaku (KTP/SIM/Paspor); dan</li>
                    <li> Copy kartu identitas diri Pemegang Polis atau Penerima Manfaat yang masih berlaku (KTP/Passpor/SIM); dan</li>
                    <li> Copy kartu keluarga atau alat bukti sah bahwa Penerima Manfaat adalah keluarga  atau  yang  telah ditunjuk Pemegang Polis atau Tertanggung; dan</li>
                    <li> Surat keterangan meninggal asli/legalisir dari Instansi yang berwenang/Pamong Praja setempat (jika Tertanggung meninggal dunia bukan di Rumah Sakit)atau dari Dokter/Rumah Sakit (jika Tertanggung meninggal di Rumah Sakit); dan</li>
                    <li> Surat keterangan kronologis kematian dari Penerima Manfaat (jika Tertanggung meninggal dunia bukan di Rumah Sakit); dan</li>
                    <li> Copy catatan/resume medis, seluruh hasil pemeriksaan laboratorium dan radiologi (jika  ada); dan</li>
                    <li> Surat keterangan pemakaman/kremasi yang dilegalisir dari Instansi yang berwenang; dan</li>
                    </ol>
                    <li> Apabila diperlukan, Penanggung berhak mengadakan penyelidikan (investigasi) dan memperoleh  informasi lebih detail atas Klaim yang diajukan baik itu meminta keterangan medis, hasil otopsi atau visum etrepertum serta meminta dokumen tambahan lainnya kepada Pemegang Polis atau Penerima Manfaat atau dari Dokter yang merawat Tertanggung dan Penanggung berhak untuk menunjuk dan menyewa seorang praktisi medis untuk melakukan pemeriksaan terhadap Klaim yang diajukan. Penanggung tidak akan melakukan pembayaran apapun   untuk   memperoleh   keterangan atau laporan medis apapun. di atas telah dipenuhi dan telah disetujui oleh Penanggung.</li>
                <li>Dokumen pengajuan Klaim sebagaimana dimaksud di atas, jika dibuat dalam bahasa asing maka harus diterjemahkan ke dalam bahasa Indonesia dan dilakukan oleh penterjemah tersumpah. Biaya-biaya yang timbul sehubungan dengan hal tersebut akan menjadi tanggung jawab pihak pengaju.</li>
                <li>Pengajuan Klaim adalah sah apabila syarat dan ketentuan sebagaimana disebutkan diatas telah dipenuhi dan Penanggung mempunyai hak untuk menolak Klaim  </li>
                
            </ol>
        </div>
        <div class="col" style="padding: 90px 0px 0px 20px;font-size: 8px; text-align: justify;"> <ol start="7" style="padding:0 0 0 15px;">  
                yang diajukan apabila syarat dan ketentuan tersebut tidak dipenuhi.
                <li>Penanggung akan Membayarkan Manfaat Asuransi setelah syarat dan ketentuan sebagaimana disebutkan</li>
                <li>Pembayaran Manfaat Asuransi dapat dilakukan melalui pemindahbukuan antar bank (transfer) ke rekening Penerima Manfaat atau dengan cara lain yang ditetapkan oleh Penanggung. </li>
                <li>Penanggung akan membayarkan Manfaat Asuransi selambat-lambatnya 14 (empat belas) Hari Kerja setelah pengajuan klaim disetujui oleh Penanggung. </li>
            
            </ol>
            <b>BERAKHIRNYA PERTANGGUNGAN</b><br>
            Pertanggungan ini akan berakhir akibat terjadinya peristiwa-peristiwa berikut ini (tergantung peristiwa mana yang terjadi lebih dahulu :
            <ol style="padding:0 0 0 15px;"> 
                <li> Terjadinya salah satu dari hal-hal yang menyebabkan berakhirnya pertanggungan berdasarkan Ketentuan Umum Polis, Ketentuan Khusus Polis dan/atau Ketentuan Tambahan (endorsemen atau addendum, jika diadakan) sehubungan dengan Polis; atau</li>
                <li> Pemegang Polis tidak menjalankan kewajibannya membayar Premi; atau</li>
                <li> Usia Tertanggung telah melebihi syarat yang ditentukan; atau</li>
                <li> Tertanggung mendapat panggilan atau masuk wajib militer; atau</li>
                <li> Tertanggung meninggal dunia; atau</li>
                <li> Pada tanggal Tertanggung mengundurkan diri dari pertanggungan dengan membatalkan pertanggungan sebelum Tanggal Berakhir Pertanggungan sebagaimana tercantum dalam Ringkasan Polis, atau</li>
                <li> Pada Tanggal Berakhir Pertanggungan sebagaimana tercantum dalam Ringkasan Polis.</li>
            </ol><br>
            <div align="center">-00-</div>
        </div> 
    </div>
</body>
</html>
<?php ob_end_flush(); ?>
