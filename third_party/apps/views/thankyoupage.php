<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Toko Pandai Club :: Terima Kasih</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="PT Capital Life Indonesia adalah bagian dari Capital Financial Group dan merupakan sister company PT Bank Capital Indonesia Tbk. Grup perusahaan nasional ini berfokus pada industri jasa keuangan yang terintegrasi. PT Capital Life Indonesia terdaftar dan diawasi oleh Otoritas Jasa Keuangan (OJK)
                                            Capital Life berkomitmen memberikan pelayanan cepat dan terbaik, mendahulukan kepentingan pemegang polis serta pihak yang berhak memperoleh manfaat. Hal ini sesuai dengan visi dan misi perusahaan. 
                                            Kinerja Capital Life tahun 2015, memberikan efek positif bagi perusahaan yang pada akhirnya Capital Life mendapatkan beberapa penghargaan. Penghargaan yang didapat antara lain “The Best Insurance 2016” dari Majalah Media Asuransi and predikat “Sangat Bagus” atas kinerja keuangan perusahaan asuransi selama tahun 2015 dari Majalah Infobank." />
        <meta name="keywords" content=""/>
        <meta name="author" content="Valdo Inc." />
        <link href="<?php echo base_url() ?>images/favi.png" rel="shortcut icon" type="image/x-icon" />
        <!--  //////////////////////////////////////////////////////
       Capital Life-AlfaMart ND
       � Valdo Inc. 2017
        
       //////////////////////////////////////////////////////
        -->
        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content=""/>
        <meta property="og:image" content=""/>
        <meta property="og:url" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="og:description" content=""/>
        <meta name="twitter:title" content="" />
        <meta name="twitter:image" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:card" content="" />

        <!-- Animate.css<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet"> -->
        <?php echo $combine['css']; ?>

    </head>
    <body onload="StartTimers();" onmousemove="ResetTimers();">
        <div class="gtco-loader"></div>
        <div id="page">
            <div class="page-inner">
                <?php echo $template['navigation'] ?>
              <header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner">
                    <div class="overlay"></div>
                    <div class="gtco-container">
                        <div class="row">
                            <div class="col-md-12 col-md-offset-0 text-right">
                                <div class="row row-mt-15em">
                                    <div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
                                        <span class="intro-text-small"></span>
                                        <h1>TERIMA KASIH </h1>
                                        <h5 style="color: #ffffff;">Anda sudah mendaftar sebagai pemegang polis asuransi.
Silakan melakukan pembayaran di Alfamart terdekat dengan membawa e-KTP Anda.</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
<!--                
                <iframe width="560" height="315" src="https://www.youtube.com/embed/hiGyXqaEFtI" frameborder="0" allowfullscreen></iframe>-->
                
<?php echo $template['footer'] ?>
            </div>
        </div>
        <div class="gototop js-top">
            <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
        </div>
        <!-- jQuery -->
<?php echo $combine['js']; ?>
    </body>
</html>

