<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Toko Pandai Club :: Profile</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="PT Capital Life Indonesia adalah bagian dari Capital Financial Group dan merupakan sister company PT Bank Capital Indonesia Tbk. Grup perusahaan nasional ini berfokus pada industri jasa keuangan yang terintegrasi. PT Capital Life Indonesia terdaftar dan diawasi oleh Otoritas Jasa Keuangan (OJK)
                                            Capital Life berkomitmen memberikan pelayanan cepat dan terbaik, mendahulukan kepentingan pemegang polis serta pihak yang berhak memperoleh manfaat. Hal ini sesuai dengan visi dan misi perusahaan. 
                                            Kinerja Capital Life tahun 2015, memberikan efek positif bagi perusahaan yang pada akhirnya Capital Life mendapatkan beberapa penghargaan. Penghargaan yang didapat antara lain “The Best Insurance 2016” dari Majalah Media Asuransi and predikat “Sangat Bagus” atas kinerja keuangan perusahaan asuransi selama tahun 2015 dari Majalah Infobank." />
        <meta name="keywords" content=""/>
        <meta name="author" content="Valdo Inc." />
        <link href="<?php echo base_url() ?>images/favi.png" rel="shortcut icon" type="image/x-icon" />
        <!--  //////////////////////////////////////////////////////
       Capital Life-AlfaMart ND
       � Valdo Inc. 2017
       //////////////////////////////////////////////////////
        -->
        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content=""/>
        <meta property="og:image" content=""/>
        <meta property="og:url" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="og:description" content=""/>
        <meta name="twitter:title" content="" />
        <meta name="twitter:image" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:card" content="" />
        
        <!-- Animate.css<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet"> -->
        <?php echo $combine['css']; ?>
         
    </head>
    <body onload="StartTimers();" onmousemove="ResetTimers();">
        <div class="gtco-loader"></div>
        <div id="page">
            <div class="page-inner">
               <nav   class="gtco-nav" role="navigation">
    <div style=" margin-top: 10px;" class="gtcos-container">
        <div class="row">
            <div class="col-sm-5 col-xs-12">
             <?php $is_logged_in = $this->session->userdata('policy_no'); if (!isset($is_logged_in) || $is_logged_in != true) { ?>
              <a href="<?php echo base_url(); ?>"><img style="color: #ffffff;" width="202" src="<?php echo base_url() ?>/images/new-cli-logo.png"></a>
                      <a class="menu-1" href="http://www.valdoinc.com"><img width="90" style="color: #ffffff;" src="<?php echo base_url() ?>/images/new-cli-logo-old.png"></a>
                
                  <?php } else
                    {
                    ?>
                    <a href="<?php echo base_url(); ?>home-login"><img style="color: #ffffff;" width="202" src="<?php echo base_url() ?>/images/new-cli-logo.png"></a>
                    <?php } ?>
            </div>
            <div class="col-xs-7 text-right menu-1">
                 <ul nav-list>
                    <li><a href="<?php echo base_url() ?>about-us">Tentang Kami</a></li>
                    <li><a href="<?php echo base_url() ?>product-information">Informasi Produk</a></li>
                    <li><a href="<?php echo base_url() ?>claim-page">Klaim</a></li>
                    <li><a href="<?php echo base_url() ?>contact-us">Hubungi Kami</a></li>
                    <?php $is_logged_in = $this->session->userdata('policy_no'); if (!isset($is_logged_in) || $is_logged_in != true) { ?>
                   <li class="btn-cta"><a href="#"><span>Aktif</span></a></li>
<!--                     <li class="btn-cta"><a href="<?php echo base_url() ?>member-register"><span>Aktif</span></a></li>-->
                   <?php } else { ?>
                    <li class="btn-cta"><a href="<?php echo base_url() ?>update_cos/logout"><span>Logout</span></a></li>
                    <?php }
                    ?>
                 </ul>
            </div>
        </div>	
    </div>
    <div class="gtcos-container">
        <div class="row">
            <div style=" margin-bottom: 10px; margin-left: 10px" class="col-sm-7 col-xs-12 menu-1">
                   <strong style="color:#0098EF ; vertical-align: text-top; margin-bottom: 10px;">CUSTOMER CARE : +6221-2902-3688</strong>
            </div>
            <div class="col-sm-5 text-right menu-1">               
            </div>
        </div>  
    </div>
</nav>
    <header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(<?php echo base_url() ?>images/jessica-ruscello-196422.jpg)">
        <div class="overlay"></div>
        <div class="gtco-container">
            <div class="row">
                <div class="col-md-12 col-md-offset-0 text-left">
                    <div class="row row-mt-15em">
                        <div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
                            <span class="intro-text-small">Toko Pandai Club</span>
                            <h1>Membership Area</h1>	
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
                <div class="gtco-section border-bottom">
                    <div class="gtco-container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-12 animate-box">
                                    <h3>Profile</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-12">
                                                     <form action="<?php echo base_url() ?>update_cos/update_url/<?php echo $this->uri->segment(2) ?>" method="post" runat="server"  onsubmit="ShowLoading()" class="form-horizontal">
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="nopolis">Policy No:</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" disabled value="<?php echo $cos[0]['policy_no'] ?>" name="policy_no" class="form-control" id="policy_no">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="nopolis">KTP No:</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" disabled value="<?php echo $cos[0]['ktp_no'] ?>" name="ktp_no" class="form-control" id="ktp_no">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="cname">Nama:</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" disabled value="<?php echo $cos[0]['cname'] ?>" name="cname" class="form-control" id="cname" placeholder="Nama Polis">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="nopolis">Tgl Lahir:</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" disabled value="<?php echo tanggalindo($cos[0]['dob']); ?>" name="dob" class="form-control" id="dob">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="age">Usia:</label>
                                                            <div class="col-sm-10">
                                                                <input type="text"  minlength="2" disabled onkeypress="return isNumberKey(event)" min="18" max="65" maxlength="2" name="age" value="<?php echo $cos[0]['age']; ?>" class="form-control" id="age" placeholder="Usia">
                                                            </div>
                                                        </div>									    
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2"  for="phone_no">Phone No:</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" disabled onkeypress="return isNumberKey(event)" name="phone_no" value="<?php echo $cos[0]['phone_no']; ?>" class="form-control" id="phone_no" placeholder="Phone No">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="email">Email:</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" disabled name="email" value="<?php echo $cos[0]['email']; ?>" class="form-control" id="email" placeholder="Email">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="adrress">Alamat:</label>
                                                            <div class="col-sm-10">
                                                                <textarea name="address" disabled class="form-control" rows="5" id="address"><?php echo $cos[0]['address']; ?></textarea>
                                                            </div>
                                                        </div>
                                                        <?php if ($cos[0]['cname'] == "" OR $cos[0]['phone_no'] == ""  OR $cos[0]['email'] == "" OR $cos[0]['address'] == "") { ?><?php } ?>
<!--                                                            <div class="form-group">
                                                            <div class="col-sm-offset-2 col-sm-10">
                                                            <button type="submit" id="disable" class="btn btn-default">Submit</button>
                                                            </div>	
                                                        </div> -->
                                                    </form>
                                                </div>
                                                 <?php if ($cos[0]['cname'] != "" && $cos[0]['phone_no'] != "" && $cos[0]['email'] != "") { ?>
                                                 <a download href="<?php echo base_url() ?>update_cos/create_pdf_url/<?php echo $cos[0]['policy_no'] ?>">Download E-polis <img width="20" src="<?php echo base_url() ?>/images/pdf.png"></a>
                                                 <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo $template['footer'] ?>
            </div>
        </div>
        <div class="gototop js-top">
            <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
        </div>
        <!-- jQuery -->
        <?php echo $combine['js']; ?>
        <script type="text/javascript">
            $(function () {
                $.fn.datepicker.defaults.format = "yyyy-mm-dd";
                $(".datepicker").datepicker({
                    startDate: '-3d'
                });
                //alert("hdhdhd");
            });
            $("#usia").click(function ()
            {
                var stdob = $("#tgl_lahir").val();
                //alert("d -->"+stdob);
                var st_url = 'data/index.php/json/age';
                //alert(st_url);	       	
                //var dataString = $.toJSON(stdob);	
                //alert(st_url);
                $.post(st_url, {tgl_lahir: stdob},
                        function (res, status) {
                            $("#usia").val(res);
                            //alert(res);
                        });
            });
        </script>
       
       
        <script type="text/javascript">
         

            
            
        $(document).ready(function () {
            //Disable full page
            $('body').bind('cut copy paste', function (e) {
                e.preventDefault();
            });

            //Disable part of page
            $('#id').bind('cut copy paste', function (e) {
                e.preventDefault();
            });
        });
        $(":submit").closest("form").submit(function(){
    $(':submit').attr('disabled', 'disabled');
});
        </script>
        <script type="text/javascript">
            
    function ShowLoading(e) {
        var div = document.createElement('div');
        var img = document.createElement('img');
        img.src = '<?php echo base_url() ?>/images/loading.gif';
        div.innerHTML = "Please Wait...<br />";
        div.style.cssText = 'position: fixed;  top: 50%; left: 400px; z-index: 5000; width: 422px; text-align: center;';
        div.appendChild(img);
        document.body.appendChild(div);
        window.onscroll = function () { window.scrollTo(0, 900); };
        return true;
        // These 2 lines cancel form submission, so only use if needed.
        //window.event.cancelBubble = true;
        //e.stopPropagation();
    }
    </script>
      
      <script>var timoutWarning = 840000;var timoutNow = 160000;var logoutUrl = '<?php echo base_url() ?>home';var warningTimer;var timeoutTimer;function StartTimers() {warningTimer = setTimeout("IdleWarning()", timoutWarning);timeoutTimer = setTimeout("IdleTimeout()", timoutNow);}function ResetTimers() {clearTimeout(warningTimer);clearTimeout(timeoutTimer);StartTimers();$("#timeout").dialog('close');}function IdleWarning() {$("#timeout").dialog({modal: true});}function IdleTimeout() {window.location = logoutUrl;}</script>
        <script type="application/javascript">function isNumberKey(e){var h=e.which?e.which:event.keyCode;return h>31&&(48>h||h>57)?!1:!0}</script>
    </body>
</html>

