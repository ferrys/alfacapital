<!DOCTYPE HTML>
<html>
    <head>
        <title><?php echo $html['title']; ?></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <?php echo $html['metaname']; ?>
        <link href="<?php echo base_url() ?>images/favi.png" rel="shortcut icon" type="image/x-icon" />
        <?php echo $combine['css']; ?>
        <style>
            #popup {
                display:none;
                position:fixed;
                z-index: 3;
                margin:100px auto;
                top: 44%;
                left: 50%;
                transform: translate(-50%, -50%);
                box-shadow: 0px 0px 50px 2px #000;
            }
            .checkboxFive {
                width: auto;
                position: relative;
            }
            .checkboxFive label {
                cursor: pointer;
                position: absolute;
                width: 25px;
                height: 25px;
                top: 0;
                left: 0;
                background: #eee;
                border:1px solid #ddd;
            }
            .checkboxFive label:after {
                opacity: 0.0;
                content: '';
                position: absolute;
                width: 9px;
                height: 5px;
                background: transparent;
                top: 6px;
                left: 7px;
                border: 3px solid #333;
                border-top: none;
                border-right: none;

                transform: rotate(-45deg);
            }
            .checkboxFive label:hover::after {
                opacity: 0.5;
            }
            .checkboxFive input[type=checkbox]:checked + label:after {
                opacity: 1;
                width: auto;
                position: relative;
            }
        </style>

    </head>
    <body >
        <div class="gtco-loader"></div>
        <div id="page">
            <div class="page-inner">
                <?php echo $template['navigation'] ?>
                <header id="gtco-header" class="gtco-cover" role="banner" style="background-image: url(<?php echo base_url() ?>images/jessica-ruscello-196422.jpg)">
                    <div class="overlay"></div>
                    <div class="gtco-container">
                        <div class="row">
                            <div class="col-md-12 col-md-offset-0 text-left">
                                <div class="row row-mt-15em">
                                    <div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
                                        <span class="intro-text-small">Selamat Datang</span>
                                        <h1>Toko Pandai Club</h1>	
                                    </div>
                                    <div class="col-md-4 col-md-push-1 animate-box" data-animate-effect="fadeInRight">
                                        <?php
                                        $is_logged_in = $this->session->userdata('policy_no');
                                        if (!isset($is_logged_in) || $is_logged_in != true) {
                                            ?>
                                            <div class="form-wrap">
                                                <div class="tab">
                                                    <ul class="tab-menu">
                                                        <li class="active gtco-first"><a href="#" data-tab="login">Login</a></li>
                                                        <li class="gtco-second"><a href="#" data-tab="signup">Informasi</a></li>
                                                    </ul>
                                                    <div class="tab-content"> <?php
                                                        if (validation_errors()) {
                                                            echo validation_errors('<p class="alert alert-danger" align="center">', '</p>');
                                                        }
                                                        ?>
                                                        <div class="tab-content-inner active" data-content="login">
                                                            <form action="<?php echo base_url() ?>" method="post" onsubmit="return enter()">

                                                                <div class="row form-group">
                                                                    <div class="col-md-12">

                                                                        <label for="partid">Nomor Peserta</label>

                                                                        <input type="text" onkeypress="return isNumberKey(event)" maxlength="12" class="form-control creditCardText"  name="policy_no"  />
                                                                    </div>
                                                                    <div class="col-md-12 description">Dapat dilihat pada struk Alfamart</div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="col-md-12">
                                                                        <label for="dob">Tanggal Lahir</label>
                                                                        <input type="text" onkeypress="return isNumberKey(event)" class="form-control" id="dob" name="dob" size="6" maxlength="6" />
                                                                    </div>
                                                                    <div class="col-md-12 description">Isikan dengan format DDMMYY</div>
                                                                </div>

                                                        <!--   </div>-->
                                                        <!--     <div class="row form-group">
                                                                    <div class="col-md-12">
                                                                         captcha 
                                                                        <img id="captcha" src="<?php echo base_url() ?>library/vender/securimage/securimage_show.php" alt="CAPTCHA Image" />
                                                                        <a href="#" onclick="document.getElementById('captcha').src = '<?php echo base_url() ?>library/vender/securimage/securimage_show.php?' + Math.random(); return false" class="btn btn-info btn-sm">Show a Different Image</a><br/>
                                                                        <div class="form-group" style="margin-top: 10px;">
                                                                            <input type="text" minlength="3" required maxlength="3" class="form-control" name="captcha_code" id="captcha_code" placeholder="Security Code" />
                                                                            <span class="help-block" style="display: none;">Please enter the code displayed within the image.</span>
                                                                        </div>
                                                                        <span class="help-block" style="display: none;">Please enter a the security code.</span>
                                                                         /captcha 
                                                                    </div>
                                                                </div>-->
                                                                <div class="checkboxFive form-group">
                                                                    <input style="margin-left: 2px" required type="checkbox" value="1" id="checkboxFiveInput" name=""/>
                                                                    &nbsp;&nbsp;&nbsp;<b>Saya Setuju</b> 
                                                                </div>
                                                                <br><br>
                                                                <div class="row form-group">
                                                                    <div class="col-md-12">
                                                                        <input type="hidden" name="res" id="res" />
                                                                        <input type="submit"  id="button1" class="btn btn-primary" value="Login">
                                                                    </div><br><br><br>
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Tidak Memiliki Akun? <a href="<?php echo base_url() ?>member-register">Register</a>
                                                                </div>
                                                            </form>	
                                                        </div>
                                                        <div class="tab-content-inner" data-content="signup">
                                                            Dapatkan manfaat berupa uang santunan dan belanja gratis senilai Rp.15.000.000 jika terjadi kematian, hanya dengan <strong>Rp. 20.000</strong>! Segera Kunjungi <strong>Alfamart</strong> terdekat<br><br>
                                                            <a href="<?php echo base_url() ?>product-information"><button class="btn btn-primary">Info Lebih Lanjut</button></a>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <?php
                                        } else {
                                            
                                        }
                                        ?>  
                                    </div>
                                     <div id="popup" class="popup panel panel-primary" style="max-width:500px;">
                                        <div class="pull-right" >
                                            <a href="#" id="close" class="pull-right"  data-widget="remove"><img src="<?php echo base_url() ?>images/close1.png" style="width:25px"></a>
                                        </div>
                                         <a target="_blank" href="https://www.halodoc.com"><img class="mySlides" src="<?php echo base_url() ?>images/banner/11222017_halodoc.png" style="width:100%"></a>
                                        <img class="mySlides" src="<?php echo base_url() ?>images/banner/promojanuari.png" style="width:100%">
                                       </div>

                                      <script>
                                      var myIndex = 0;
                                      carousel();

                                      function carousel() {
                                          var i;
                                          var x = document.getElementsByClassName("mySlides");
                                          for (i = 0; i < x.length; i++) {
                                             x[i].style.display = "none";  
                                          }
                                          myIndex++;
                                          if (myIndex > x.length) {myIndex = 1}    
                                          x[myIndex-1].style.display = "block";  
                                          setTimeout(carousel, 4000); // Change image every 2 seconds
                                      }
                                      </script>
<!--                                    <div id="popup" class="popup panel panel-primary">                                        
                                        <div class="pull-right">
                                            <a href="#" id="close" class="pull-right" style="margin-right: 10px;" data-widget="remove"><i class="icon-circle-cross"></i></a>
                                        </div>

                                        <img  class="img-responsive" src="<?php echo base_url() ?>images/promo_nov.jpg" data-lightbox="example-set" alt="popup">
                                        <img  class="fancybox img-responsive  example-image-link" src="<?php echo base_url() ?>images/promo_ok.jpg" data-lightbox="example-set" alt="popup">
                                      <img  class="img-responsive  example-image-link" src="<?php echo base_url() ?>images/promo_ok.jpg" data-lightbox="example-set" alt="popup">
                                        
                                    </div>-->
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
<?php echo $template['footer'] ?>
            </div>
        </div>
        <div class="gototop js-top">
            <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
        </div>
        <!-- jQuery -->
<?php echo $combine['js']; ?>
        <script>
            $(document).ready(function () {
                $("#popup").hide().fadeIn(1000);
                $("#close").on("click", function (e) {
                    e.preventDefault();
                    $("#popup").fadeOut(1000);
                    
                });
            });
            (function () {
                // Create mobile element
                var mobile = document.createElement('div');
                mobile.className = 'nav-mobile';
                document.querySelector('.nav').appendChild(mobile);
                // hasClass
                function hasClass(elem, className) {
                    return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
                }
                // toggleClass
                function toggleClass(elem, className) {
                    var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, ' ') + ' ';
                    if (hasClass(elem, className)) {
                        while (newClass.indexOf(' ' + className + ' ') >= 0) {
                            newClass = newClass.replace(' ' + className + ' ', ' ');
                        }
                        elem.className = newClass.replace(/^\s+|\s+$/g, '');
                    } else {
                        elem.className += ' ' + className;
                    }
                }

                // Mobile nav function
                var mobileNav = document.querySelector('.nav-mobile');
                var toggle = document.querySelector('.nav-list');
                mobileNav.onclick = function () {
                    toggleClass(this, 'nav-mobile-open');
                    toggleClass(toggle, 'nav-active');
                };
            })();
        </script>
        <script type="text/javascript">
            $('.creditCardText').keyup(function () {
                var foo = $(this).val().split(" ").join(""); // remove hyphens
                if (foo.length > 0) {
                    foo = foo.match(new RegExp('.{1,4}', 'g')).join(" ");
                }
                $(this).val(foo);
            })
// masked_input_1.4-min.js
// angelwatt.com/coding/masked_input.php
                    (function (a) {
                        a.MaskedInput = function (f) {
                            if (!f || !f.elm || !f.format) {
                                return null
                            }
                            if (!(this instanceof a.MaskedInput)) {
                                return new a.MaskedInput(f)
                            }
                            var o = this, d = f.elm, s = f.format, i = f.allowed || "0123456789", h = f.allowedfx || function () {
                                return true
                            }, p = f.separator || "/:-", n = f.typeon || "_YMDhms", c = f.onbadkey || function () {}, q = f.onfilled || function () {}, w = f.badkeywait || 0, A = f.hasOwnProperty("preserve") ? !!f.preserve : true, l = true, y = false, t = s, j = (function () {
                                if (window.addEventListener) {
                                    return function (E, C, D, B) {
                                        E.addEventListener(C, D, (B === undefined) ? false : B)
                                    }
                                }
                                if (window.attachEvent) {
                                    return function (D, B, C) {
                                        D.attachEvent("on" + B, C)
                                    }
                                }
                                return function (D, B, C) {
                                    D["on" + B] = C
                                }
                            }()), u = function () {
                                for (var B = d.value.length - 1; B >= 0; B--) {
                                    for (var D = 0, C = n.length; D < C; D++) {
                                        if (d.value[B] === n[D]) {
                                            return false
                                        }
                                    }
                                }
                                return true
                            }, x = function (C) {
                                try {
                                    C.focus();
                                    if (C.selectionStart >= 0) {
                                        return C.selectionStart
                                    }
                                    if (document.selection) {
                                        var B = document.selection.createRange();
                                        return -B.moveStart("character", -C.value.length)
                                    }
                                    return -1
                                } catch (D) {
                                    return -1
                                }
                            }, b = function (C, E) {
                                try {
                                    if (C.selectionStart) {
                                        C.focus();
                                        C.setSelectionRange(E, E)
                                    } else {
                                        if (C.createTextRange) {
                                            var B = C.createTextRange();
                                            B.move("character", E);
                                            B.select()
                                        }
                                    }
                                } catch (D) {
                                    return false
                                }
                                return true
                            }, m = function (D) {
                                D = D || window.event;
                                var C = "", E = D.which, B = D.type;
                                if (E === undefined || E === null) {
                                    E = D.keyCode
                                }
                                if (E === undefined || E === null) {
                                    return""
                                }
                                switch (E) {
                                    case 8:
                                        C = "bksp";
                                        break;
                                    case 46:
                                        C = (B === "keydown") ? "del" : ".";
                                        break;
                                    case 16:
                                        C = "shift";
                                        break;
                                    case 0:
                                    case 9:
                                    case 13:
                                        C = "etc";
                                        break;
                                    case 37:
                                    case 38:
                                    case 39:
                                    case 40:
                                        C = (!D.shiftKey && (D.charCode !== 39 && D.charCode !== undefined)) ? "etc" : String.fromCharCode(E);
                                        break;
                                    default:
                                        C = String.fromCharCode(E);
                                        break
                                }
                                return C
                            }, v = function (B, C) {
                                if (B.preventDefault) {
                                    B.preventDefault()
                                }
                                B.returnValue = C || false
                            }, k = function (B) {
                                var D = x(d), F = d.value, E = "", C = true;
                                switch (C) {
                                    case (i.indexOf(B) !== -1):
                                        D = D + 1;
                                        if (D > s.length) {
                                            return false
                                        }
                                        while (p.indexOf(F.charAt(D - 1)) !== -1 && D <= s.length) {
                                            D = D + 1
                                        }
                                        if (!h(B, D)) {
                                            c(B);
                                            return false
                                        }
                                        E = F.substr(0, D - 1) + B + F.substr(D);
                                        if (i.indexOf(F.charAt(D)) === -1 && n.indexOf(F.charAt(D)) === -1) {
                                            D = D + 1
                                        }
                                        break;
                                    case (B === "bksp"):
                                        D = D - 1;
                                        if (D < 0) {
                                            return false
                                        }
                                        while (i.indexOf(F.charAt(D)) === -1 && n.indexOf(F.charAt(D)) === -1 && D > 1) {
                                            D = D - 1
                                        }
                                        E = F.substr(0, D) + s.substr(D, 1) + F.substr(D + 1);
                                        break;
                                    case (B === "del"):
                                        if (D >= F.length) {
                                            return false
                                        }
                                        while (p.indexOf(F.charAt(D)) !== -1 && F.charAt(D) !== "") {
                                            D = D + 1
                                        }
                                        E = F.substr(0, D) + s.substr(D, 1) + F.substr(D + 1);
                                        D = D + 1;
                                        break;
                                    case (B === "etc"):
                                        return true;
                                    default:
                                        return false
                                }
                                d.value = "";
                                d.value = E;
                                b(d, D);
                                return false
                            }, g = function (B) {
                                if (i.indexOf(B) === -1 && B !== "bksp" && B !== "del" && B !== "etc") {
                                    var C = x(d);
                                    y = true;
                                    c(B);
                                    setTimeout(function () {
                                        y = false;
                                        b(d, C)
                                    }, w);
                                    return false
                                }
                                return true
                            }, z = function (C) {
                                if (!l) {
                                    return true
                                }
                                C = C || event;
                                if (y) {
                                    v(C);
                                    return false
                                }
                                var B = m(C);
                                if ((C.metaKey || C.ctrlKey) && (B === "X" || B === "V")) {
                                    v(C);
                                    return false
                                }
                                if (C.metaKey || C.ctrlKey) {
                                    return true
                                }
                                if (d.value === "") {
                                    d.value = s;
                                    b(d, 0)
                                }
                                if (B === "bksp" || B === "del") {
                                    k(B);
                                    v(C);
                                    return false
                                }
                                return true
                            }, e = function (C) {
                                if (!l) {
                                    return true
                                }
                                C = C || event;
                                if (y) {
                                    v(C);
                                    return false
                                }
                                var B = m(C);
                                if (B === "etc" || C.metaKey || C.ctrlKey || C.altKey) {
                                    return true
                                }
                                if (B !== "bksp" && B !== "del" && B !== "shift") {
                                    if (!g(B)) {
                                        v(C);
                                        return false
                                    }
                                    if (k(B)) {
                                        if (u()) {
                                            q()
                                        }
                                        v(C, true);
                                        return true
                                    }
                                    if (u()) {
                                        q()
                                    }
                                    v(C);
                                    return false
                                }
                                return false
                            }, r = function () {
                                if (!d.tagName || (d.tagName.toUpperCase() !== "INPUT" && d.tagName.toUpperCase() !== "TEXTAREA")) {
                                    return null
                                }
                                if (!A || d.value === "") {
                                    d.value = s
                                }
                                j(d, "keydown", function (B) {
                                    z(B)
                                });
                                j(d, "keypress", function (B) {
                                    e(B)
                                });
                                j(d, "focus", function () {
                                    t = d.value
                                });
                                j(d, "blur", function () {
                                    if (d.value !== t && d.onchange) {
                                        d.onchange()
                                    }
                                });
                                return o
                            };
                            o.resetField = function () {
                                d.value = s
                            };
                            o.setAllowed = function (B) {
                                i = B;
                                o.resetField()
                            };
                            o.setFormat = function (B) {
                                s = B;
                                o.resetField()
                            };
                            o.setSeparator = function (B) {
                                p = B;
                                o.resetField()
                            };
                            o.setTypeon = function (B) {
                                n = B;
                                o.resetField()
                            };
                            o.setEnabled = function (B) {
                                l = B
                            };
                            return r()
                        }
                    }(window));</script>
         
        <script type="application/javascript">function isNumberKey(e){var h=e.which?e.which:event.keyCode;return h>31&&(48>h||h>57)?!1:!0}</script>
    </body>
</html>

