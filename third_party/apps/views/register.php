<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Toko Pandai Club :: Register</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="PT Capital Life Indonesia adalah bagian dari Capital Financial Group dan merupakan sister company PT Bank Capital Indonesia Tbk. Grup perusahaan nasional ini berfokus pada industri jasa keuangan yang terintegrasi. PT Capital Life Indonesia terdaftar dan diawasi oleh Otoritas Jasa Keuangan (OJK)
                                            Capital Life berkomitmen memberikan pelayanan cepat dan terbaik, mendahulukan kepentingan pemegang polis serta pihak yang berhak memperoleh manfaat. Hal ini sesuai dengan visi dan misi perusahaan. 
                                            Kinerja Capital Life tahun 2015, memberikan efek positif bagi perusahaan yang pada akhirnya Capital Life mendapatkan beberapa penghargaan. Penghargaan yang didapat antara lain “The Best Insurance 2016” dari Majalah Media Asuransi and predikat “Sangat Bagus” atas kinerja keuangan perusahaan asuransi selama tahun 2015 dari Majalah Infobank." />
        <meta name="keywords" content="" />
        <meta name="author" content="Valdo Inc." />
        <link href="<?php echo base_url() ?>images/favi.png" rel="shortcut icon" type="image/x-icon" />
        <!-- 
       //////////////////////////////////////////////////////
       Capital Life-AlfaMart ND
       � Valdo Inc. 2017
       //////////////////////////////////////////////////////
        -->
        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content=""/>
        <meta property="og:image" content=""/>
        <meta property="og:url" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="og:description" content=""/>
        <meta name="twitter:title" content="" />
        <meta name="twitter:image" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:card" content="" />
        <?php echo $combine['css']; ?>
         
        <!-- Animate.css<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet"> -->
       
    </head>
    <body>
        <div class="gtco-loader"></div>
        <div id="page">
            <div class="page-inner">
                <?php echo $template['navigation'] ?>
                <header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(images/bg-register.jpg)">
                    <div class="overlay"></div>
                    <div class="gtco-container">
                        <div class="row">
                            <div class="col-md-12 col-md-offset-0 text-left">
                                <div class="row row-mt-15em">

                                    <div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
                                        <span class="intro-text-small">Toko Pandai Club</span>
                                        <h1>Membership Area</h1>	
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </header>
                <div class="gtco-section border-bottom">
                    <div class="gtco-container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-12 animate-box">
                                    <h3>Profile</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form action="<?php echo base_url() ?>update_cos/register_c" method="post" class="form-horizontal">

                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="nama">Nama:</label>
                                                            <div class="field col-sm-10">
                                                                <input type="text"  required class="form-control" name="cname" placeholder="Nama Polis">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="handphone">Phone No:</label>
                                                            <div class="field col-sm-4">
                                                                <input type="text" required minlength="10" maxlength="15" onBlur="checkUmur()" onkeypress="return isNumberKey(event)"  name="phone_no" class="form-control" id="phone_no" placeholder="Phone No">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="ktp_no">KTP No:</label>
                                                            <div class="field col-sm-4">
                                                                <input type="text" required minlength="16" maxlength="16" onBlur="checkKtpAvailability()" id="ktp_no" name="ktp_no" onkeypress="return isNumberKey(event)" class="form-control" id="phone_no" placeholder="KTP No">
                                                            </div> <span id="user-ktp-status"></span>
                                                        </div>
                                                        <script type="application/javascript">
                                                            function checkKtpAvailability() {
                                                                jQuery.ajax({
                                                                    url: "<?php echo site_url('update_cos/check_ktp_exists') ?>",
                                                                    data: 'ktp_no=' + $("#ktp_no").val(),
                                                                    type: "POST",
                                                                    success: function (data) {
                                                                        $("#user-ktp-status").html(data);
                                                                    },
                                                                    error: function () {}
                                                                });
                                                            }
                                                        </script>
                                                        <style>
                                                            .status-available{color:#2FC332;}
                                                            .status-not-available{color:#D60202;}
                                                        </style>
                                                      
                                                        
                                                        
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="email">Email:</label>
                                                            <div class="field col-sm-10">
                                                                <input type="email" required name="email" class="form-control" id="email" placeholder="Email">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="alamat">Alamat:</label>
                                                            <div class="field col-sm-10">
                                                                <textarea class="form-control" required name="address" rows="5" id="address"></textarea>
                                                            </div>
                                                        </div>
<!--                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="Plan">Produk Plan:</label>
                                                            <div class="field col-sm-10">
                                                                <label class="radio-inline"><input type="radio" name="product_plan">Paket A - Rp.20.000,-</label>
                                                                <label class="radio-inline"><input type="radio" name="product_plan">Paket B - Rp.20.000,-</label>
                                                                <label class="radio-inline"><input type="radio" name="product_plan">Paket C - Rp.20.000,-</label> 		
                                                            </div>
                                                        </div>-->
                                                        
                                                        <div class="form-group">
                                                            <div  class="actions col-sm-offset-2 col-sm-10">
                                                                <input type="submit" value="Submit" id="myBtn" class="btn btn-default">
                                                        </div>
                                                </div>
                                                </div>
                                                    </form>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <?php echo $template['footer'] ?>
            </div>

        </div>

        <div class="gototop js-top">
            <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
        </div>

       <?php echo $combine['js']; ?>
        <script type="text/javascript">
        $(document).ready(function () {
            //Disable full page
            $('body').bind('cut copy paste', function (e) {
                e.preventDefault();
            });

            //Disable part of page
            $('#id').bind('cut copy paste', function (e) {
                e.preventDefault();
            });
        });
        </script>
       <script type="application/javascript">function isNumberKey(e){var h=e.which?e.which:event.keyCode;return h>31&&(48>h||h>57)?!1:!0}</script>
       </body>
</html>

