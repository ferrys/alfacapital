<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $html['title']; ?></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <?php echo $html['metaname']; ?>
        <link href="<?php echo base_url() ?>images/favi.png" rel="shortcut icon" type="image/x-icon" />
        <?php echo $combine['css']; ?>
        <style> #popup {
                display:none;
                position:fixed;
                z-index: 3;
                margin:100px auto;
                top: 44%;
                left: 50%;
                transform: translate(-50%, -50%);
                box-shadow: 0px 0px 50px 2px #000;
            }</style>
    </head>
    <body onload="StartTimers();" onmousemove="ResetTimers();">

        <div class="gtco-loader"></div>
        <div id="page">
            <div class="page-inner">
                <?php echo $template['navigation'] ?>
                
                <!--<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(images/drew-coffman-98466.jpg)">-->
                <header class="gtco-cover gtco-cover-sm" role="banner" >
                    <!--style="background-image: url(images/product-information.jpg);background-size: 100%; margin-top: 50px; background-position: center; background-repeat: no-repeat; "-->
                   
                     <?php 
                        $is_logged_in = $this->session->userdata('policy_no');
                         if (!isset($is_logged_in) || $is_logged_in != true) {
                         $link= base_url()."member-register";
                         }
                         else {$link= base_url()."product-information";}
                         
                         ?>
                    <a href="<?php echo $link ?>">   <img class="gambar" src="<?php echo base_url()?>images/product-information.jpg" style="height: 87%; margin-top: 100px; background-position: center;background-repeat: no-repeat;background-size: cover;"></a>
<!--                    <div id="popup" class="popup panel panel-primary ">
                                        
                                        <div class="pull-right">
                                    <a href="#" id="close" class="pull-right"  data-widget="remove"><i class="icon-circle-cross"></i></a>
                                        </div>
                                    <img  class="img-responsive"  src="<?php base_url() ?>images/promo_nov.jpg" alt="popup">
                                    </div>                        -->
                                <div id="popup" class="popup panel panel-primary" style="max-width:500px">
                                        <div class="pull-right">
                                            <a href="#" id="close" class="pull-right" data-widget="remove"><img src="<?php echo base_url() ?>images/close1.png" style="width:25px"></a>
                                        </div>
                                    <a target="_blank" href="https://www.halodoc.com"><img class="mySlides" src="<?php echo base_url() ?>images/banner/11222017_halodoc.png" style="width:100%"></a>
                                        <img class="mySlides" src="<?php echo base_url() ?>images/banner/promojanuari.png" style="width:100%">
                                       </div>

                                      <script>
                                      var myIndex = 0;
                                      carousel();

                                      function carousel() {
                                          var i;
                                          var x = document.getElementsByClassName("mySlides");
                                          for (i = 0; i < x.length; i++) {
                                             x[i].style.display = "none";  
                                          }
                                          myIndex++;
                                          if (myIndex > x.length) {myIndex = 1}    
                                          x[myIndex-1].style.display = "block";  
                                          setTimeout(carousel, 4000); // Change image every 2 seconds
                                      }
                                      </script>
                    <!--<div style="background-image: url(images/product-information.jpg); background-size: 100%; margin-top: 50px; background-position: center; background-repeat: no-repeat;"></div>-->
                    <div class="gtco-container">
                        <div class="row">
                            <div class="col-md-12 col-md-offset-0 text-left">
                                <div class="row row-mt-15em">
                                    <div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
<!--                                        <span class="intro-text-small">Toko Pandai Club</span>
                                        <h1>Informasi Produk</h1>	-->
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <div class="gtco-section border-bottom">
                    <div class="gtco-container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-12 animate-box">
                                    <h3>FITUR PRODUK PROGRAM ASURANSI JIWA KUMPULAN ASURANSI CAPITAL EKA PROTEKSI KHUSUS MEMBER TOKO PANDAI CLUB</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-responsive" style="text-align: justify">
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Nama Produk</td>
                                                        <td>Asuransi Capital Eka Proteksi Group (CAKAP)</td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>Mata Uang</td>
                                                        <td>Rupiah (IDR)</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Penanggung</td>
                                                        <td>PT Capital Life Indonesia</td>
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td>Pemegang Polis</td>
                                                        <td>PT Valdo International</td>
                                                    </tr>
                                                    <tr>
                                                        <td>5</td>
                                                        <td>Tertanggung</td>
                                                        <td>Konsumen Toko Pandai Club</td>
                                                    </tr>
                                                    <tr>
                                                        <td>6</td>
                                                        <td>Masa Pertanggungan</td>
                                                        <td><p>Masa pertanggungan 6 bulan dengan ketentuan Usia Tertanggung saat mengajukan
                                                                permohonan pertanggungan ditambah masa pertanggungan ≤ 60 tahun.</p></td>
                                                    </tr>
                                                    <tr>
                                                        <td>7</td>
                                                        <td>Deskripsi Manfaat Produk</td>
                                                        <td>Produk ini memberikan Manfaat Asuransi yang dapat dijelaskan sebagai berikut :
                                                            <ol><li>Apabila Tertanggung meninggal dunia dalam masa pertanggungan yang diakibatkan karena Penyakit (bukan karena Kecelakaan) dan pertanggungan masih berlaku, maka Penanggung akan membayarkan Manfaat Asuransi sebesar 100% (seratus persen) Uang Pertanggungan dan selanjutnya pertanggungan berakhir.
                                                                </li><li>	Meninggal dunianya Tertanggung sebagaimana dimaksud pada poin 1 diatas bukan sebagai akibat hal-hal yang tidak dijamin atau dikecualikan.
                                                                </li></ol></td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td>8</td>
                                                        <td>Syarat Menjadi Tertanggung</td>
                                                        <td><p>Syarat untuk menjadi Tertanggung pada saat pengajuan permohonan pertanggungan :</p>
                                                            <ol>
                                                                <li> Syarat umum
                                                                    Tertanggung harus dalam keadaan sehat jasmani dan rohani, tidak sedang menjalani Rawat
                                                                    Inap di Rumah Sakit atau Rawat Jalan dan tidak sedang menderita Penyakit akut atau Penyakit
                                                                    menahun.</li>
                                                                <li> Usia masuk
                                                                    Usia masuk Tertanggung yang diperkenankan :<br>
                                                                    <ul> <li>Minimum : 18 tahun</li>
                                                                        <li>Maksimum : 59 tahun</li>
                                                                    </ul>
                                                                </li>
                                                                <li>Usia masuk ditambah masa pertanggungan ≤ 60 tahun.</li>
                                                                <li>Memenuhi ketentuan seleksi risiko (underwriting) yang ditetapkan Penanggung, termasuk
                                                                    namun tidak terbatas pada pemeriksaan kesehatan apabila diperlukan.</li></ol>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>9</td>
                                                        <td>Uang Pertanggungan</td>
                                                        <td>Besarnya Uang Pertanggungan ditetapkan sebesar Rp. 15.000.000,- per tertanggung</td>
                                                    </tr>

                                                    <tr>
                                                        <td>10</td>
                                                        <td>Hal-hal yang tidak Dijamin (Pengecualian)</td>
                                                        <td>
                                                            Manfaat Asuransi tidak dapat dibayarkan apabila Tertanggung meninggal dunia sebagai akibat
                                                            dari hal-hal tersebut dibawah ini :
                                                            <ol><li> Kematian yang terjadi sebelum Tanggal Mulai Pertanggungan; atau</li>
                                                                <li> Kondisi yang telah ada Sebelumnya (Pre-Existing Condition) ), menurut jangka waktu 
                                                                    sebelum Tanggal Mulai Pertanggungan sebagaimana tercantum dalam Ringkasan Polis; atau</li>
                                                                <li> Tindakan melukai dan mencederai diri sendiri, usaha atau tindakan pembunuhan dan
                                                                    percobaan bunuh diri atau tindakan lainnya yang membahayakan diri yang dilakukan dengan
                                                                    maksud jahat atau tidak, dalam keadaan sadar atau tidak sadar, dalam keadaan waras atau
                                                                    tidak waras yang dilakukan oleh Tertanggung atau pihak lain atas permintaan Tertanggung
                                                                    atau Pemegang Polis; atau
                                                                </li>
                                                                <li> Keikutsertaan dalam suatu kegiatan atau olahraga berbahaya, seperti semua olahraga
                                                                    beladiri (tinju, karate, judo, silat, gulat, kempo, taekwondo, kungfu atau sejenisnya), semua
                                                                    olahraga dirgantara (terjun payung, terbang layang, parasailing atau sejenisnya), hang
                                                                    gliding, ballooning, panjat tebing, mendaki gunung, semua jenis olah raga kontak fisik,
                                                                    semua perlombaan ketangkasan atau kecepatan yang menggunakan kendaraan bermotor,
                                                                    sepeda, kuda, perahu, pesawat udara atau sejenisnya, berlayar seorang diri, menyelam,
                                                                    arum jeram, ski air, ski es, hockey, rugby, bungee jumping, surfing atau olahraga air
                                                                    sejenisnya, memasuki gua-gua atau lubang-lubang yang dalam, berburu binatang, segala
                                                                    jenis perlombaan yang menyangkut daya tahan dan olahraga berbahaya lainnya dan berisiko
                                                                    tinggi baik resmi maupun tidak resmi; atau
                                                                </li>
                                                                <li> Pengaruh penggunaan alkohol, obat bius, narkotik dan sejenisnya, termasuk obat-obatan
                                                                    dalam arti yang seluas-luasnya terkecuali zat-zat dan/atau obat-obatan dimaksud
                                                                    dipergunakan atas petunjuk Dokter dan tidak terkait dengan upaya perawatan kecanduan
                                                                    obat (upaya rehabilitasi) atau mengalami gangguan lemah mental/sakit jiwa; atau
                                                                </li>
                                                                <li> Dengan sengaja ikut serta mengambil bagian dalam suatu tindakan melanggar hukum,
                                                                    tindak pidana kejahatan, perkelahian (kecuali jika sebagai orang yang bertindak
                                                                    mempertahankan diri) dan sejenisnya (termasuk mengendarai kendaraan bermotor tanpa
                                                                    Surat Ijin Mengemudi yang sah dan berlaku); atau
                                                                </li>
                                                                <li> Tindak kejahatan (pembunuhan) yang dilakukan dengan sengaja, atau kekhilafan besar oleh
                                                                    pihak yang berkepentingan dalam Polis ini dan ahli warisnya; atau
                                                                </li>
                                                                <li> Keterlibatan sebagai pelaku aktif dalam tindakan terorisme, sabotase, bom, dan/atau huruhara
                                                                    (SRCC); atau
                                                                </li>
                                                                <li> Segala Penyakit yang berkembang akibat dari terinfeksi HIV, atau Penyakit yang timbul baik
                                                                    langsung maupun tidak langsung oleh AIDS (Acquired Immune Deficiency Syndrome)
                                                                    dan/atau komplikasinya (AIDS Related Complex/ARC); atau jenis Penyakit lain yang
                                                                    menyebabkan hilangnya kekebalan tubuh, serta Penyakit kelamin lainnya; atau
                                                                </li>
                                                                <li> Perang (baik yang dinyatakan atau tidak oleh Pemerintah), invasi, perang saudara, tugas
                                                                    militer, pembajakan, pemogokan, huru-hara, kerusuhan atau pemberontakan, revolusi,
                                                                    kekuatan militer, makar, terorisme, sabotase, perlawanan terhadap Pemerintah, pengambilalihan
                                                                    kekuasaan dengan kekerasan; atau
                                                                </li>
                                                                <li>Tertanggung dikenakan hukuman mati berdasarkan keputusan Pengadilan yang telah
                                                                    memiliki kekuatan hukum yang tetap; atau
                                                                </li>
                                                                <li> Menggunakan alat transportasi yang membawa bahan peledak atau bahan berbahaya
                                                                    lainnya; atau
                                                                </li>
                                                                <li> Apapun baik langsung maupun tidak langsung karena atau terjadi pada reaksi-reaksi inti
                                                                    atom dan atau nuklir, termasuk namun tidak terbatas kepada radiasi nuklir, ionisasi, fusi, fisi
                                                                    atau pencemaran radioaktif dari setiap bahan nuklir, limbah nuklir, bahan kimia, reaksi
                                                                    biologi, gas beracun; atau
                                                                </li>
                                                                <li> Tindakan aborsi, komplikasi kehamilan dan/atau kelahiran (bagi wanita).</li>
                                                            </ol>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>11</td>
                                                        <td>Prosedur dan dokumen
                                                            pengajuan klaim
                                                        </td>
                                                        <td>
                                                            <ol>
                                                                <li>Pengajuan klaim atas manfaat meninggal dunia harus dilaporkan/diajukan ke Penanggung
                                                                    selambat-lambatnya 90 (sembilan puluh) Hari Kalender terhitung sejak tanggal
                                                                    meninggalnya Tertanggung.
                                                                </li>
                                                                <li> Klaim yang dilaporkan/diajukan setelah atau melebihi jangka waktu sebagaimana dimaksud
                                                                    pada poin 1 di atas, maka Klaim dianggap kadaluarsa dan Penanggung tidak bertanggung
                                                                    jawab dan berhak menolak pengajuan Klaim tersebut.
                                                                </li>
                                                                <li> Pengajuan Klaim meninggal dunia karena Penyakit harus disertai dengan dokumen
                                                                    pendukung sebagai berikut :
                                                                    <ol type="a">

                                                                        <li> Form pengajuan Klaim dari Pemegang Polis atau Penerima Manfaat; dan
                                                                        </li>
                                                                        <li> Copy Polis atau bukti kepesertaan asuransi lainnya dari Tertanggung; dan
                                                                        </li>
                                                                        <li> Copy bukti pembayaran Premi; dan
                                                                        </li>
                                                                        <li> Copy kartu identitas Tertanggung yang masih berlaku (KTP/SIM/Paspor); dan
                                                                        </li>
                                                                        <li> Copy kartu identitas diri Pemegang Polis atau Penerima Manfaat yang masih berlaku 

                                                                            (KTP/Passpor/SIM); dan
                                                                        </li>
                                                                        <li> Copy kartu keluarga atau alat bukti sah bahwa Penerima Manfaat adalah keluarga atau
                                                                            yang telah ditunjuk Pemegang Polis atau Tertanggung; dan
                                                                        </li>
                                                                        <li> Surat keterangan meninggal asli/legalisir dari Instansi yang berwenang/Pamong Praja
                                                                            setempat (jika Tertanggung meninggal dunia bukan di Rumah Sakit) atau dari
                                                                            Dokter/Rumah Sakit (jika Tertanggung meninggal di Rumah Sakit); dan
                                                                        </li>
                                                                        <li> Surat keterangan kronologis kematian dari Penerima Manfaat (jika Tertanggung
                                                                            meninggal dunia bukan di Rumah Sakit); dan
                                                                        </li>
                                                                        <li> Copy catatan/resume medis, seluruh hasil pemeriksaan laboratorium dan radiologi (jika
                                                                            ada); dan
                                                                        </li>
                                                                        <li> Surat keterangan pemakaman/kremasi yang dilegalisir dari Instansi yang berwenang;
                                                                            dan
                                                                        </li>
                                                                        <li> Surat keterangan dari Kedutaan Besar Republik Indonesia (KBRI) jika Tertanggung
                                                                            meninggal di luar negeri; dan
                                                                        </li>
                                                                        <li> Surat penetapan pengadilan dalam hal Tertanggung dinyatakan hilang sesuai dengan
                                                                            ketentuan perundangan yang berlaku.</li>
                                                                    </ol>
                                                                </li>
                                                                <li> Apabila diperlukan, Penanggung berhak mengadakan penyelidikan (investigasi) dan
                                                                    memperoleh informasi lebih detail atas Klaim yang diajukan baik itu meminta keterangan
                                                                    medis, hasil otopsi atau visum et repertum serta meminta dokumen tambahan lainnya
                                                                    kepada Pemegang Polis atau Penerima Manfaat atau dari Dokter yang merawat Tertanggung
                                                                    dan Penanggung berhak untuk menunjuk dan menyewa seorang praktisi medis untuk
                                                                    melakukan pemeriksaan terhadap Klaim yang diajukan. Penanggung tidak akan melakukan
                                                                    pembayaran apapun untuk memperoleh keterangan atau laporan medis apapun.
                                                                </li>
                                                                <li> Dokumen pengajuan Klaim sebagaimana dimaksud di atas, jika dibuat dalam bahasa asing
                                                                    maka harus diterjemahkan ke dalam bahasa Indonesia dan dilakukan oleh penterjemah
                                                                    tersumpah. Biaya-biaya yang timbul sehubungan dengan hal tersebut akan menjadi
                                                                    tanggung jawab pihak pengaju.
                                                                </li>
                                                                <li> Pengajuan Klaim adalah sah apabila syarat dan ketentuan sebagaimana disebutkan di atas
                                                                    telah dipenuhi dan Penanggung mempunyai hak untuk menolak Klaim yang diajukan apabila
                                                                    syarat dan ketentuan tersebut tidak dipenuhi.
                                                                </li>
                                                                <li> Penanggung akan membayarkan Manfaat Asuransi setelah syarat dan ketentuan
                                                                    sebagaimana disebutkan di atas telah dipenuhi dan telah disetujui oleh Penanggung.
                                                                </li>
                                                                <li> Pembayaran Manfaat Asuransi dapat dilakukan melalui pemindahbukuan antar bank
                                                                    (transfer) ke rekening Penerima Manfaat atau dengan cara lain yang ditetapkan oleh
                                                                    Penanggung.
                                                                </li>
                                                                <li> Penanggung akan membayarkan Manfaat Asuransi selambat-lambatnya 14 (empat belas)
                                                                    Hari Kerja setelah pengajuan klaim disetujui oleh Penanggung.
                                                                </li>
                                                                <li>10)	Pembayaran Manfaat Asuransi akan dilakukan dalam bentuk santunan kematian sebesar Rp. 9.000.000 (sembilan juta rupiah) dan voucher belanja senilai Rp. 6.000.000 (enam juta rupiah).</li></ol>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>12</td>
                                                        <td>Ketentuan Tambahan </td>
                                                        <td>
                                                            Masa Tunggu<ol type="a">
                                                                <li>Berlaku Masa Tunggu selama 30 (tiga puluh) Hari Kalender terhitung sejak Tanggal Mulai
                                                                    Pertanggungan untuk meninggal dunia yang disebabkan karena Penyakit apapun.
                                                                </li><li>Dalam hal meninggal dunianya Tertanggung terjadi dalam Masa Tunggu, maka
                                                                    Penanggung tidak berkewajiban membayar Manfaat Asuransi.
                                                                </li></ol>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <a target="_blank" download href="<?php echo base_url(); ?>assets/pdf/INFORMASI_PRODUCT.pdf">DOWNLOAD INFORMASI PRODUCT</a> 
                                           <?php 
                                            $is_logged_in = $this->session->userdata('policy_no');
                                             if (!isset($is_logged_in) || $is_logged_in != true) {
                                             ?> <div class="pull-right"><a align="right" href="<?php echo base_url() ?>member-register"> <input type="submit"  id="button1" class="btn btn-primary" value="REGISTRASI SEKARANG"></a></div><?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo $template['footer'] ?>
            </div>
        </div>
        <div class="gototop js-top">
            <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
        </div>
        <?php echo $combine['js']; ?>
        <script>
            $(document).ready(function () {
                $("#popup").hide().fadeIn(1000);
                $("#close").on("click", function (e) {
                    e.preventDefault();
                    $("#popup").fadeOut(1000);
                });
            });
             
            var timoutWarning = 840000; // Display warning in 14 Mins.
            var timoutNow = 300000; // Timeout in 15 mins.
            var logoutUrl = '<?php echo base_url() ?>home'; // URL to logout page.

            var warningTimer;
            var timeoutTimer;

            // Start timers.
            function StartTimers() {
                warningTimer = setTimeout("IdleWarning()", timoutWarning);
                timeoutTimer = setTimeout("IdleTimeout()", timoutNow);
            }
            // Reset timers.
            function ResetTimers() {
                clearTimeout(warningTimer);
                clearTimeout(timeoutTimer);
                StartTimers();
                $("#timeout").dialog('close');
            }

            // Show idle timeout warning dialog.
            function IdleWarning() {
                $("#timeout").dialog({
                    modal: true
                });
            }
            // Logout the user.
            function IdleTimeout() {
                window.location = logoutUrl;
            }
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                //Disable full page
                $('body').bind('cut copy paste', function (e) {
                    e.preventDefault();
                });

                //Disable part of page
                $('#id').bind('cut copy paste', function (e) {
                    e.preventDefault();
                });
            });
        </script>
        <script type="text/javascript">function disableselect(n) {
                return!1
            }
            function reEnable() {
                return!0
            }
            document.onselectstart = new Function("return false"), document.oncontextmenu = new Function("return false"), window.sidebar && (document.onmousedown = disableselect, document.onclick = reEnable), window.history.forward(), window.onload = function () {
                window.history.forward()
            }, window.onunload = function () {};</script>
    </body>
</html>

