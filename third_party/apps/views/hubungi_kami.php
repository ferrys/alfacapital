<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $html['title']; ?></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <?php echo $html['metaname']; ?>
        <link href="<?php echo base_url() ?>images/favi.png" rel="shortcut icon" type="image/x-icon" />
        <?php echo $combine['css']; ?>
    </head>
    <body onload="StartTimers();" onmousemove="ResetTimers();">
        <div class="gtco-loader"></div>
        <div id="page">
            <div class="page-inner">
                <?php echo $template['navigation'] ?>
                <header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" width="202" style="background-image: url(images/robyn-budlender-112521.jpg)">
                    <div class="overlay"></div>
                    <div class="gtco-container">
                        <div class="row">
                            <div class="col-md-12 col-md-offset-0 text-left">
                                <div class="row row-mt-15em">

                                    <div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
                                        <span class="intro-text-small">Toko Pandai Club</span>
                                        <h1>Hubungi Kami</h1>	
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <div class="gtco-section border-bottom">
                    <div class="gtco-container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6 animate-box">
                                    <h3>Hubungi Kami</h3>
                                    <form action="<?php echo base_url() ?>hubungi_kami/hub_kami" method="POST">
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <label class="sr-only" for="name">Name</label>
                                                <input type="text" required id="name" class="form-control" placeholder="Your Name">
                                            </div>							
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <label class="sr-only" for="email">Email</label>
                                                <input type="text" required name="email" id="email" class="form-control" placeholder="Your email address">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <label class="sr-only" for="subject">Subject</label>
                                                <input type="text" required name="subject" id="subject" class="form-control" placeholder="Your subject of this message">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <label class="sr-only" for="message">Message</label>
                                                <textarea name="message" required id="message" cols="30" rows="10" class="form-control" placeholder="Write us something"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" value="Send Message" class="btn btn-primary">
                                        </div>
                                    </form>		
                                </div>
                                <div class="col-md-5 col-md-push-1 animate-box">
                                    <div class="gtco-contact-info">
                                        <h3>Contact Information</h3>
                                        <ul>
                                            <li class="address">Sona Topas Tower, <br> Lantai 9 Jl. Jend. Sudirman Kav 26 Jakarta 12920, Indonesia 
<!--                                        <br>Fax : +6221-2903 9148 <br>RT.4/RW.2, Karet, Setia Budi, Kota Jakarta Selatan,</li>-->
                                            <li class="icon-print"><a href="tel://+622129039147">+6221-2903 9148 (Fax)</a></li>
                                            <li class="phone"><a href="tel://+622129039147">+6221-2902-3688 / +6289-9191-9898 (WA)</a></li>
                                            <li class="email"><a href="mailto:care.support@capitallife.co.id">care@tokopandai.club</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<?php echo $template['footer'] ?>
            </div>
        </div>
        <div class="gototop js-top">
            <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
        </div>
        <!-- jQuery -->
<?php echo $combine['js']; ?>
        <script>
            // Set timeout variables.
            var timoutWarning = 840000; // Display warning in 14 Mins.
            var timoutNow = 300000; // Timeout in 15 mins.
            var logoutUrl = '<?php echo base_url() ?>/home'; // URL to logout page.
            var warningTimer;
            var timeoutTimer;
            // Start timers.
            function StartTimers() {
                warningTimer = setTimeout("IdleWarning()", timoutWarning);
                timeoutTimer = setTimeout("IdleTimeout()", timoutNow);
            }
            // Reset timers.
            function ResetTimers() {
                clearTimeout(warningTimer);
                clearTimeout(timeoutTimer);
                StartTimers();
                $("#timeout").dialog('close');
            }
            // Show idle timeout warning dialog.
            function IdleWarning() {
                $("#timeout").dialog({
                    modal: true
                });
            }
            // Logout the user.
            function IdleTimeout() {
                window.location = logoutUrl;
            }
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                //Disable full page
                $('body').bind('cut copy paste', function (e) {
                    e.preventDefault();
                });
                //Disable part of page
                $('#id').bind('cut copy paste', function (e) {
                    e.preventDefault();
                });
            });
        </script>
        <script type="text/javascript">function disableselect(n) {
                return!1
            }
            function reEnable() {
                return!0
            }
            document.onselectstart = new Function("return false"), document.oncontextmenu = new Function("return false"), window.sidebar && (document.onmousedown = disableselect, document.onclick = reEnable), window.history.forward(), window.onload = function () {
                window.history.forward()
            }, window.onunload = function () {};</script>
    </body>
</html>