<?php
$about = "";
$claim = "";
$produk = "";
$keuangan = "";
$hub_kami = "";
$benefit ="";
if ($this->uri->segment(1) == "about-us") {
    $about = 'class="active"';
} else if ($this->uri->segment(1) == "claim-page") {
    $claim = 'class="active"';
} else if ($this->uri->segment(1) == "product-information") {
    $produk = 'class="active"';
} else if ($this->uri->segment(1) == "contact-us") {
    $hub_kami = 'class="active"';
} else if ($this->uri->segment(1) == "info-keuangan") {
    $keuangan = 'class="active"';
}
else if ($this->uri->segment(1) == "benefit") {
    $benefit = 'class="active"';
}

?>
<style>
    [data-tooltip] {
        position: relative;
        z-index: 2;
        cursor: pointer;
    }
    /* Hide the tooltip content by default */
    [data-tooltip]:before,
    [data-tooltip]:after {
        visibility: hidden;
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
        filter: "progid: DXImageTransform.Microsoft.Alpha(Opacity=0)";
        opacity: 0;
        pointer-events: none;
    }

    /* Position tooltip above the element */
    [data-tooltip]:before {
        position: absolute;
        bottom: 50%;
        left: 50%;
        margin-top: 50px;
        margin-left: -80px;
        padding: 7px;
        width: 100px;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        background-color: #000;
        background-color: hsla(0, 0%, 20%, 0.9);
        color: #fff;
        content: attr(data-tooltip);
        text-align: center;
        font-size: 14px;
        line-height: 1.2;
    }

    /* Triangle hack to make tooltip look like a speech bubble */
    [data-tooltip]:after {
        position: absolute;
        bottom: 150%;
        left: 50%;
        margin-left: -5px;
        width: 0;
        border-top: 5px solid #000;
        border-top: 5px solid hsla(0, 0%, 20%, 0.9);
        border-right: 5px solid transparent;
        border-left: 5px solid transparent;
        content: " ";
        font-size: 0;
        line-height: 0;
    }

    /* Show tooltip content on hover */
    [data-tooltip]:hover:before,
    [data-tooltip]:hover:after {
        visibility: visible;
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
        filter: "progid: DXImageTransform.Microsoft.Alpha(Opacity=100)";
        opacity: 1;
    }
</style>
<!--<nav   class="gtco-nav" role="navigation">
    <div style=" margin-top: 10px;" class="gtcos-container">
        <div class="row">
            <div class="col-sm-6 col-xs-12">
<?php
$is_logged_in = $this->session->userdata('policy_no');
if (!isset($is_logged_in) || $is_logged_in != true) {
    ?>
                    <a href="<?php echo base_url(); ?>"><img  width="202" style="color: #ffffff;" src="<?php echo base_url() ?>/images/new-cli-logo.png"></a>
                    <a class="menu-1" href="http://capitallife.co.id"><img width="130" style="color: #ffffff;" src="<?php echo base_url() ?>/images/new-cli-logo-old.png"></a>
                    <a class="menu-1" href="http://www.valdoinc.com"><img width="130" style="color: #ffffff;" src="<?php echo base_url() ?>/images/logo500kbedit.jpg"></a>
    <?php
} else {
    ?>
                    <a href="<?php echo base_url(); ?>home-login"><img  width="202" style="color: #ffffff;" src="<?php echo base_url() ?>/images/new-cli-logo.png"></a>
                    <a class="menu-1" href="http://capitallife.co.id"><img width="130" style="color: #ffffff;" src="<?php echo base_url() ?>/images/new-cli-logo-old.png"></a>
                    <a class="menu-1" href="http://www.valdoinc.com"><img width="130" style="color: #ffffff;" src="<?php echo base_url() ?>/images/logo500kbedit.jpg"></a>
<?php } ?>
            </div>
            <div class="text-left menu-1">
                <ul nav-list>
                <li <?php echo $about ?>><a style="font-size: 15px;" href="<?php echo base_url() ?>about-us">Tentang Kami</a></li>
                <?php if ($is_logged_in == true) {?><li <?php echo $keuangan ?>><a style="font-size: 15px;" href="<?php echo base_url() ?>info-keuangan">Info Keuangan</a></li><?php } ?>
                <li <?php echo $produk ?>><a style="font-size: 15px;" href="<?php echo base_url() ?>product-information">Produk</a></li>
                <li <?php echo $claim ?>><a style="font-size: 15px;" href="<?php echo base_url() ?>claim-page">Klaim</a></li>
                <li <?php echo $hub_kami ?>><a style="font-size: 15px;" href="<?php echo base_url() ?>contact-us">Hubungi Kami</a></li>
<?php
$is_logged_in = $this->session->userdata('policy_no');
if (!isset($is_logged_in) || $is_logged_in != true) {
    ?>
                                                            <li class="has-dropdown" >
                                                                        <a href="#">
                                                                        <img title="" width="30" src="<?php echo base_url() ?>/images/logout-128.png" ></a>
                                                                        <ul class="dropdown">
                        <li class="btn-cta"><a style="font-size: 12px;" href="<?php echo base_url() ?>">Login</a>|<a style="font-size: 12px;"  href="<?php echo base_url() ?>member-register">Register</a></li>
                        <li><a class="menu-1" href="<?php base_url() ?>faq"><img width="20" style="color: #ffffff;" src="<?php echo base_url() ?>/images/icon_help.png"></a></li>
                                                          <li class="btn-cta"><a style="font-size: 17px;"  href="<?php echo base_url() ?>member-register">Register</a></li>
                                                                        </ul>
                                                                </li>
                                                                    <li class="btn-cta"><a href="<?php echo base_url() ?>member-register"><span>Aktif</span></a></li>
<?php } else { ?>
                        <li data-tooltip="Logout" >|<a style="font-size: 12px;" href="<?php echo base_url() ?>update_cos/logout">logout</a></li>

<?php } ?> 
                </ul>
            </div>
        </div>	
    </div>
    <div class="gtcos-container">
        <div class="row">
            <div style=" margin-bottom: 10px; margin-left: 10px" class="col-sm-7 col-xs-12 menu-1">
                <strong style="color:#0098EF ; vertical-align: text-top; margin-bottom: 10px;">CUSTOMER CARE : +6221-2902-3688</strong>
            </div>
            <div class="col-sm-5 text-right menu-1">               
            </div>
        </div>  
    </div>
</nav>-->

                <nav   class="gtco-nav" role="navigation">
    <div style=" margin-top: 10px;" class="gtcos-container">
        <div class="row">
            <div class="col-sm-6 col-xs-12">
<?php
$is_logged_in = $this->session->userdata('policy_no');
if (!isset($is_logged_in) || $is_logged_in != true) {
    ?>
                    <a href="<?php echo base_url(); ?>"><img  width="202" style="color: #ffffff;" src="<?php echo base_url() ?>/images/new-cli-logo.png"></a>
                    <a class="menu-1" href="http://capitallife.co.id"><img width="130" style="color: #ffffff;" src="<?php echo base_url() ?>/images/new-cli-logo-old.png"></a>
                    <a class="menu-1" href="http://www.valdoinc.com"><img width="130" style="color: #ffffff;" src="<?php echo base_url() ?>/images/logo500kbedit.jpg"></a>
    <?php
} else {
    ?>
                    <a href="<?php echo base_url(); ?>home-login"><img  width="202" style="color: #ffffff;" src="<?php echo base_url() ?>/images/new-cli-logo.png"></a>
                    <a class="menu-1" href="http://capitallife.co.id"><img width="130" style="color: #ffffff;" src="<?php echo base_url() ?>/images/new-cli-logo-old.png"></a>
                    <a class="menu-1" href="http://www.valdoinc.com"><img width="130" style="color: #ffffff;" src="<?php echo base_url() ?>/images/logo500kbedit.jpg"></a>
<?php } ?>
            </div>
            <div class="text-left menu-1">
                <ul nav-list>
                <li <?php echo $about ?>><a style="font-size: 14px;" href="<?php echo base_url() ?>about-us">Tentang Kami</a></li>
                <?php if ($is_logged_in == true) {?><li <?php echo $keuangan ?>><a style="font-size: 15px;" href="<?php echo base_url() ?>info-keuangan">Info Keuangan</a></li><?php } ?>
                <li <?php echo $produk ?>><a style="font-size: 14px;" href="<?php echo base_url() ?>product-information">Produk</a></li>
                <li <?php echo $benefit ?>><a style="font-size: 15px;" href="<?php echo base_url() ?>benefit">Benefit</a></li>

                <li <?php echo $claim ?>><a style="font-size: 14px;" href="<?php echo base_url() ?>claim-page">Klaim</a></li>
                <li <?php echo $hub_kami ?>><a style="font-size: 14px;" href="<?php echo base_url() ?>contact-us">Hubungi Kami</a></li>
<?php
$is_logged_in = $this->session->userdata('policy_no');
if (!isset($is_logged_in) || $is_logged_in != true) {
    ?>
                        <!--                                    <li class="has-dropdown" >
                                                                        <a href="#">
                                                                        <img title="" width="30" src="<?php echo base_url() ?>/images/logout-128.png" ></a>
                                                                        <ul class="dropdown">-->
                        <li class="btn-cta"><a style="font-size: 11px;" href="<?php echo base_url() ?>">Login</a>|<a style="font-size: 11px;"  href="<?php echo base_url() ?>member-register">Register</a></li>
                        <li><a class="menu-1" href="<?php base_url() ?>faq"><img width="20" style="color: #ffffff;" src="<?php echo base_url() ?>/images/icon_help.png"></a></li>
  <!--                                                        <li class="btn-cta"><a style="font-size: 17px;"  href="<?php echo base_url() ?>member-register">Register</a></li>-->
                        <!--                                                </ul>
                                                                </li>-->
                                                                                    <!--    <li class="btn-cta"><a href="<?php echo base_url() ?>member-register"><span>Aktif</span></a></li>-->
                    <?php } else { ?>
                                            <li data-tooltip="Logout" >|<a style="font-size: 12px;" href="<?php echo base_url() ?>update_cos/logout">logout</a></li>

                    <?php } ?> 
                </ul>
            </div>
        </div>	
    </div>
    <div class="gtcos-container">
        <div class="row">
            <div style=" margin-bottom: 10px; margin-left: 10px" class="col-sm-7 col-xs-12 menu-1">
                <strong style="color:#0098EF ; vertical-align: text-top; margin-bottom: 10px;">CUSTOMER CARE : +6221-2902-3688</strong>
            </div>
            <div class="col-sm-5 text-right menu-1">               
            </div>
        </div>  
    </div>
</nav>