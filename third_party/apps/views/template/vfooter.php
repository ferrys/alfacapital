<style>
    [data-tooltip] {
        position: relative;
        z-index: 2;
        
    }
    /* Hide the tooltip content by default */
    [data-tooltip]:before,
    [data-tooltip]:after {
        visibility: hidden;
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
        filter: "progid: DXImageTransform.Microsoft.Alpha(Opacity=0)";
        opacity: 0;
        pointer-events: none;
    }

    /* Position tooltip above the element */
    [data-tooltip]:before {
        position: absolute;
        bottom: 50%;
        left: 50%;
        margin-top: 50px;
        margin-left: -80px;
        padding: 7px;
        width: 100px;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        background-color: #000;
        background-color: hsla(0, 0%, 20%, 0.9);
        color: #fff;
        content: attr(data-tooltip);
        text-align: center;
        font-size: 14px;
        line-height: 1.2;
    }

    /* Triangle hack to make tooltip look like a speech bubble */
    [data-tooltip]:after {
        position: absolute;
        bottom: 150%;
        left: 50%;
        margin-left: -5px;
        width: 0;
        border-top: 5px solid #000;
        border-top: 5px solid hsla(0, 0%, 20%, 0.9);
        border-right: 5px solid transparent;
        border-left: 5px solid transparent;
        content: " ";
        font-size: 0;
        line-height: 0;
    }

    /* Show tooltip content on hover */
    [data-tooltip]:hover:before,
    [data-tooltip]:hover:after {
        visibility: visible;
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
        filter: "progid: DXImageTransform.Microsoft.Alpha(Opacity=100)";
        opacity: 1;
    }
</style>
<!-- <script>
            setInterval(function ()
            {
                $.ajax({
                    type: "post",
                    url: "<?php echo base_url() ?>home",
                    datatype: "html",
                    success: function (data)
                    {
                        $("#load").load(" #load");
                        
                    }
                });
            }, 1000);    
        </script>-->
<footer id="gtco-footer" role="contentinfo">
    <div class="gtco-container">
        <div class="row row-p b-md">
            <div class="col-md-4">
                <div class="gtco-widget">
                    <h3>PT <span class="footer-logo">Capital Life Indonesia</span><span></span></h3>
                    <p>Sona Topas Tower, Lantai 9 Jl. Jend. Sudirman Kav 26 Jakarta 12920, Indonesia <br> Telepon : +6221-2902-3688<br>Fax : +6221-2903 9148</p>
                </div>
            </div>
            <div class="col-md-4 col-md-push-1">
                <div class="gtco-widget">
                    <h3>PENJUALAN & PEMASARAN</h3>
                    <ul class="gtco-quick-contact">
                        <li><img title="" width="25" src="<?php echo base_url() ?>/images/telpon.png" >Telepon : <a href="#"> +6221-2902-3688</a></li>
                        <li><img title="" width="17" src="<?php echo base_url() ?>/images/fax.png" > &nbsp;&nbsp;Fax : <a href="#">&nbsp;&nbsp;  +6221-2903 9148</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-md-push-1">
                <div class="gtco-widget">
                    <h3>LAYANAN PELANGGAN</h3>
                    <ul class="gtco-quick-contact">
                        <li><img title="" width="25" src="<?php echo base_url() ?>/images/telpon.png" ><a href="#"> +6221-2902-3688</a></li>
                       <li><a target="_black" href="https://api.whatsapp.com/send?phone=628991919898&text=Hello%20Capital"><img title="" width="17" src="<?php echo base_url() ?>/images/wa.png" >&nbsp;&nbsp;  +6289-9191-9898 (WA)</a></li>
                       <li><a href="mailto:care@tokopandai.club?Subject=Hello%20Capital"><img title="" width="25" src="<?php echo base_url() ?>/images/pesan.png" > &nbsp;care@tokopandai.club</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row copyright">
            <div class="col-md-12">
                <p class="pull-left">
                    <small class="block">Copyright &copy; <?php echo date("Y");?> <a href="http://www.tokopandai.club">Protected by Capital Life</a>. All Rights Reserved. E-Policy System Provided by <a href="http://www.valdoinc.com">Valdo inc</a> <a href="#" style="color:#ffffff;">Rendering {elapsed_time}</a></small> 
                </p>
                <p class="pull-right">
                    <script>
                    function insertReply(content) {
                        document.getElementById('output').innerHTML = script.src;
                    }
                    var script = document.createElement('script');
                    script.src = '<?php echo base_url() ?>API-DATA?key=63c7379d421368a48243107b1f96f077ff57841bc0f2e4900914451b0ea78a24';
                    document.body.appendChild(script);
                    </script>
         
<!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
   <script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>-->
                    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111085166-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-111085166-1');
</script>

                <div id="load">  <ul class="gtco-social-icons pull-right">
                    <li data-tooltip="day(<?php echo $visitor ?>) Month(<?php echo $visitorm ?>)"><a target="_blank" href="#"><img  width="25" src="<?php echo base_url() ?>/images/visitor.png" ></a></li>
                    <li><a target="_blank"  href="https://www.facebook.com/Capital-Life-Indonesia-707048319454368/"><img  width="25" src="<?php echo base_url() ?>/images/facebook.png" ></a></li>
                    <li><a target="_blank"  href="http://www.capitallife.co.id/"><img width="25" src="<?php echo base_url() ?>/images/world.png" ></a></li><span id="output"></span>
                </ul></div>
                </p>
            </div>
        </div>
    </div>
</footer>
