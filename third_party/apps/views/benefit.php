<!DOCTYPE HTML>
<html>
    <head>
        <title><?php echo $html['title']; ?></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <?php echo $html['metaname']; ?>
        <link href="<?php echo base_url() ?>images/favi.png" rel="shortcut icon" type="image/x-icon" />
        <?php echo $combine['css']; ?>
        <style>
            #popup {
                display:none;
                position:absolute;
                z-index: 3;
                margin:100px auto;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                box-shadow: 0px 0px 50px 2px #000;
            }
            .checkboxFive {
                width: auto;
                position: relative;
            }
            .checkboxFive label {
                cursor: pointer;
                position: absolute;
                width: 25px;
                height: 25px;
                top: 0;
                left: 0;
                background: #eee;
                border:1px solid #ddd;
            }
            .checkboxFive label:after {
                opacity: 0.0;
                content: '';
                position: absolute;
                width: 9px;
                height: 5px;
                background: transparent;
                top: 6px;
                left: 7px;
                border: 3px solid #333;
                border-top: none;
                border-right: none;

                transform: rotate(-45deg);
            }
            .checkboxFive label:hover::after {
                opacity: 0.5;
            }
            .checkboxFive input[type=checkbox]:checked + label:after {
                opacity: 1;
                width: auto;
                position: relative;
            }
        </style>

    </head>
    <body >
        <div id="page">
            <div class="page-inner">
                <?php
$about = "";
$claim = "";
$produk = "";
$keuangan = "";
$hub_kami = "";
if ($this->uri->segment(1) == "about-us") {
    $about = 'class="active"';
} else if ($this->uri->segment(1) == "claim-page") {
    $claim = 'class="active"';
} else if ($this->uri->segment(1) == "product-information") {
    $produk = 'class="active"';
} else if ($this->uri->segment(1) == "contact-us") {
    $hub_kami = 'class="active"';
} else if ($this->uri->segment(1) == "info-keuangan") {
    $keuangan = 'class="active"';
}
?>
<style>
    [data-tooltip] {
        position: relative;
        z-index: 2;
        cursor: pointer;
    }
    /* Hide the tooltip content by default */
    [data-tooltip]:before,
    [data-tooltip]:after {
        visibility: hidden;
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
        filter: "progid: DXImageTransform.Microsoft.Alpha(Opacity=0)";
        opacity: 0;
        pointer-events: none;
    }

    /* Position tooltip above the element */
    [data-tooltip]:before {
        position: absolute;
        bottom: 50%;
        left: 50%;
        margin-top: 50px;
        margin-left: -80px;
        padding: 7px;
        width: 100px;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        background-color: #000;
        background-color: hsla(0, 0%, 20%, 0.9);
        color: #fff;
        content: attr(data-tooltip);
        text-align: center;
        font-size: 14px;
        line-height: 1.2;
    }

    /* Triangle hack to make tooltip look like a speech bubble */
    [data-tooltip]:after {
        position: absolute;
        bottom: 150%;
        left: 50%;
        margin-left: -5px;
        width: 0;
        border-top: 5px solid #000;
        border-top: 5px solid hsla(0, 0%, 20%, 0.9);
        border-right: 5px solid transparent;
        border-left: 5px solid transparent;
        content: " ";
        font-size: 0;
        line-height: 0;
    }

    /* Show tooltip content on hover */
    [data-tooltip]:hover:before,
    [data-tooltip]:hover:after {
        visibility: visible;
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
        filter: "progid: DXImageTransform.Microsoft.Alpha(Opacity=100)";
        opacity: 1;
    }
</style>
<div class="gtco-loader"></div>
<?php echo $template['navigation'] ?>

<header id="gtco-header" class="gtco-cover gtco-cover-sms" role="banner">
<img class="gambar" src="<?php echo base_url()?>images/banner.png" style=" margin-top: 100px; background-position: center;background-repeat: no-repeat;background-size: cover;">
		<div class="gtco-container">
			
		</div>
	</header>

<div class="gtco-sections">
    <div class="gtco-container">
        <div class="col-sm-2"><img width="200px" src="<?php echo base_url() ?>images/hanyahalo.png"></div>
        <div class="pull-right"><img  width="200px" src="<?php echo base_url() ?>images/new-cli-logo-old.png"></div>
<!--        <div class="col-sm-2"><img  width="200px" src="<?php echo base_url() ?>images/new-cli-logo-old.png">
        <div class="pull-right" ><img width="200px" src="<?php echo base_url() ?>images/logo-halodoc.png"></div>-->
        </div>
        <br> <div class="text-center col-md-8 col-md-offset-2 "style="margin-top: 50px;">
					<h2 style="color:#e00854;">APA ITU HALODOC</h2>
                  Halodoc merupakan aplikasi kesehatan yang memberikan solusi kesehatan lengkap dan terpercaya untuk memnuhi kebutuhan kesehatan kamu dan keluarga dengan menawarkan berbagai layanan yaitu<br>
                  Hubungi dokter, Apotik Antar & Lab Service
                        </div> 
                          
<div class="text-center col-md-8 col-md-offset-2 ">
                 
    
    
    <div class="gtco-sections">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
					<h2 style="color:#e00854;">KENAPA HALODOC</h2></div>
			</div>
			<div class="row">
					<div class="col-md-4">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<span class="icon">
                                                    <img width="100" src="<?php echo base_url()?>images/cepat.png">
						</span>
						<h3 style="color:#444243!important;">CEPAT, AMAN & NYAMAN</h3>
						<p>Nikmati mudahnya membeli obat ataupun suplemen dengan cepat, aman dan nyaman yang langsung diantarkan ketempat tujuanmu</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<span class="icon">
							<img width="100" src="<?php echo base_url()?>images/terpercaya.png">
						</span>
						<h3 style="color:#444243!important;">TERPERCAYA</h3>
						<p>Kebutuhan medis kamu akan terpenuhi dengan dokter dan apotik yang sudah terdaftar & tersebar diseluruh indonesia</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<span class="icon">
							<img width="55" src="<?php echo base_url()?>images/dimanasaja.png">
						</span>
						<h3 style="color:#444243!important;">DIMANA SAJA & KAPAN SAJA</h3>
						<p>Dimanapun dan kapanpun kamu bepergian, Halodoc siap memberikan kemudahan bagi kesehatan kamu & keluarga </p>
					</div>
				</div>
			</div>
		</div>
	</div>
    
                        </div> 
   
    
    
    <div class="gtco-section text-center col-md-8 col-md-offset-2 ">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 text-center gtco-heading">
                                    <h2 style="color:#e00854;">CARA MENDAPATKAN KEUNTUNGAN<br> BAGI USER Capital Life MELALUI HALODOC</h2></div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<span class="icon">
							<img width="100" src="<?php echo base_url()?>images/registrasi.png">
						</span>
						<h3 style="color:#444243!important;">REGISTRASI</h3>
						<p>Registrasi asuransi kamu di tokopandai.club</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<span class="icon">
                                                    <a target="_blank" href="https://ch5xj.app.goo.gl/JaiN" ><img width="55" src="<?php echo base_url()?>images/download.png"></a>
						</span>
						<h3 style="color:#444243!important;">DOWNLOAD HALODOC</h3>
						<p>Download aplikasi Halodoc <a target="_blank" href="https://ch5xj.app.goo.gl/JaiN" >disini</a></p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<span class="icon">
							<img width="55" src="<?php echo base_url()?>images/voucher.png">
						</span>
						<h3 style="color:#444243!important;">DAPATKAN VOUCHER</h3>
						<p>Dapatkan voucher untuk gunakan "Hubungi Dokter" melalui sms dari Halodoc</p>
					</div>
				</div>
			</div>
		</div>
            
            
    
                        </div> 
        <div class="gtco-sections text-center col-md-8 col-md-offset-2 ">
            <div class="row">
				<div class="col-md-12 text-center gtco-heading">
                                    <h2 style="color:#e00854;">FITUR APLIKASI HALODOC</h2></div>
			</div>
            <div class="gtco-container" style="background-color: #d8d2d5; z-index: 0">
                <ul style="list-style-type: none;">
                    <li class="gtco-section  ">
                        <p style="float: right; z-index: 2;">
                            <img src="/images/hubungi.png" width="400px" ></p>                            
                            <p style="width:300px" class="text-left"><b style="color:#444243!important;">HUBUNGI DOKTER</b><br>Temukan & hubungi dokter umum maupun berbagai spesialis favoritmu. Nikmati berbagai pilihan komunikasi pada dokter seperti chat dan voice/video call dari smartphone, dimana saja!</p>
                    </li>
                            <li class="gtco-sections  ">
                                <p style="float: left; "><a href="#"><img src="/images/antar.png" width="400px" style="margin-right: 10px;"></a></p>
                                
                            <p class="text-left ;float: right;"><b style="color:#444243!important;" class="text-left">APOTIK ANTAR</b><br>Belanja kebutuhan medis dengan lebih cepat, aman & nyaman. Bebas ribet tanpa perlu antre di apotik ataupun terkena macet dan dapat langsung diantar ke lokasi tujuan kamu</p>
                    </li>
                             
                              <li class="gtco-section">
                                  <p style="float: right;"><img src="/images/service.png"  width="400px" ></p>
                           
                                  <p style="width:300px;" class="text-left"><b style="color:#444243!important;">LAB SERVICE</b><br>Kini periksa cek laboratorium kesehatanmu dapat dilakukan dirumah. Pilih paket pemeriksaan kamu, tentukan tanggal dan staf kami akan langsung datang kelokasi kamu</p>
                             </li>
                        <!-- <td class="gtco-section text-center col-md-2">
                             <p style="float: right;"><img src="/images/icon_help.png" height="300px" width="200px" border="1px"></p>
                            <h3 style="color:#444243!important;">HUBUNGI DOKTER</h3>
                             <p>Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text</p>
                         </td>-->
                </ul><div class="clearfix"></div>
		</div>
	</div>
         </div>
   
<!--<div id="gtco-subscribe">
		<div class="gtco-container">
			
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2">
					<form class="form-inline">
						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label for="email" class="sr-only">Email</label>
								<input type="email" class="form-control" id="email" placeholder="Your Email">
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<button type="submit" class="btn btn-default btn-block">Subscribe</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>-->
   
             
<?php echo $template['footer'] ?>

            </div>
        </div>
        <div class="gototop js-top">
            <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
        </div>
        <!-- jQuery -->
<?php echo $combine['js']; ?>
        </body>
</html>

