<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Toko Pandai Club :: Informasi Keuangan</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="PT Capital Life Indonesia adalah bagian dari Capital Financial Group dan merupakan sister company PT Bank Capital Indonesia Tbk. Grup perusahaan nasional ini berfokus pada industri jasa keuangan yang terintegrasi. PT Capital Life Indonesia terdaftar dan diawasi oleh Otoritas Jasa Keuangan (OJK)
                                            Capital Life berkomitmen memberikan pelayanan cepat dan terbaik, mendahulukan kepentingan pemegang polis serta pihak yang berhak memperoleh manfaat. Hal ini sesuai dengan visi dan misi perusahaan. 
                                            Kinerja Capital Life tahun 2015, memberikan efek positif bagi perusahaan yang pada akhirnya Capital Life mendapatkan beberapa penghargaan. Penghargaan yang didapat antara lain “The Best Insurance 2016” dari Majalah Media Asuransi and predikat “Sangat Bagus” atas kinerja keuangan perusahaan asuransi selama tahun 2015 dari Majalah Infobank." />
        <meta name="keywords" content="" />
        <meta name="author" content="Valdo Inc." />
        <meta property="og:title" content=""/>
        <meta property="og:image" content=""/>
        <meta property="og:url" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="og:description" content=""/>
        <meta name="twitter:title" content="" />
        <meta name="twitter:image" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:card" content="" />
        <link href="<?php echo base_url() ?>images/favi.png" rel="shortcut icon" type="image/x-icon" />
        <?php echo $combine['css']; ?>
        <style>
            .image-link {
                cursor: -webkit-zoom-in;
                cursor: -moz-zoom-in;
                cursor: zoom-in;
            }
            /* This block of CSS adds opacity transition to background */
            .mfp-with-zoom .mfp-container,
            .mfp-with-zoom.mfp-bg {
                opacity: 0;
                -webkit-backface-visibility: hidden;
                -webkit-transition: all 0.3s ease-out; 
                -moz-transition: all 0.3s ease-out; 
                -o-transition: all 0.3s ease-out; 
                transition: all 0.3s ease-out;
            }

            .mfp-with-zoom.mfp-ready .mfp-container {
                opacity: 1;
            }
            .mfp-with-zoom.mfp-ready.mfp-bg {
                opacity: 0.8;
            }

            .mfp-with-zoom.mfp-removing .mfp-container, 
            .mfp-with-zoom.mfp-removing.mfp-bg {
                opacity: 0;
            }



            /* padding-bottom and top for image */
            .mfp-no-margins img.mfp-img {
                padding: 0;
            }
            /* position of shadow behind the image */
            .mfp-no-margins .mfp-figure:after {
                top: 0;
                bottom: 0;
            }
            /* padding for main container */
            .mfp-no-margins .mfp-container {
                padding: 0;
            }



            /* aligns caption to center */
            .mfp-title {
                text-align: center;
                padding: 6px 0;
            }
            .image-source-link {
                color: #DDD;
            }
            #pagewrap {
                padding: 5px;
                width: 960px;
                margin: 20px auto;
            }
            header {
                height: 100px;
                padding: 0 15px;
            }
            #content {
                width: auto;
                float: left;
                padding: 5px 15px;
            }

            #middle {
                width: auto; /* Account for margins + border values */
                float: left;
                padding: 5px 15px;
                margin: 0px 5px 5px 5px;
            }

            #sidebar {
                width: auto;
                padding: 5px 15px;
                float: left;
            }
            footer {
                clear: both;
                padding: 0 15px;
            }
            @media screen and (max-width: 980px) {

                #pagewrap {
                    width: 94%;
                }
                #content {
                    width: 41%;
                    padding: 1% 4%;
                }
                #middle {
                    width: 41%;
                    padding: 1% 4%;
                    margin: 0px 0px 5px 5px;
                    float: right;
                }

                #sidebar {
                    clear: both;
                    padding: 1% 4%;
                    width: auto;
                    float: none;
                }

                header, footer {
                    padding: 1% 4%;
                }
            }
            @media screen and (max-width: 600px) {

                #content {
                    width: auto;
                    float: none;
                }

                #middle {
                    width: auto;
                    float: none;
                    margin-left: 0px;
                }

                #sidebar {
                    width: auto;
                    float: none;
                }
            }
            /* for 480px or less */
            @media screen and (max-width: 480px) {

                header {
                    height: auto;
                }
                h1 {
                    font-size: 2em;
                }
                #sidebar {
                    display: none;
                }
            }
            #content {
                background: #f8f8f8;
            }
            #sidebar {
                background: #f0efef;
            }
            header, #content, #middle, #sidebar {
                margin-bottom: 5px;
            }





        </style>
    </head>
    <body onload="StartTimers();" onmousemove="ResetTimers();">
        <div class="gtco-loader"></div>
        <div id="page">
            <div class="page-inner">
                <?php echo $template['navigation'] ?>
                <header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(images/drew-coffman-98466.jpg)">
                    <div class="overlay"></div>
                    <div class="gtco-container">
                        <div class="row">
                            <div class="col-md-12 col-md-offset-0 text-left">
                                <div class="row row-mt-15em">
                                    <div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
                                        <span class="intro-text-small">Toko Pandai Club</span>
                                        <h1>Informasi Keuangan</h1>	
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>

                <div class="gtco-section border-bottom">
                    <div class="gtco-container">
                        <div class="row">
                            <div class="col-md-12 text-justify">
                                <h1 align="center">Piramida Keuangan</h1>
                                <h2 align="center"><i>(Financial Pyramid)</i></h2>
                                <!--  <section id="content">
                                    <a href="http://mediaindonesia.com/thumbs/600x400c/news/2016/03/pital2.jpg" class="without-caption image-link">
                                        <img src="http://mediaindonesia.com/thumbs/600x400c/news/2016/03/pital2.jpg" width="300"/>  
                                    </a>
                                    <div class="clearfix"><br></div>
                                    <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/Water_Dolphin.jpg/800px-Water_Dolphin.jpg" data-source="http://commons.wikimedia.org/wiki/Commons:Picture_of_the_day" class="with-caption image-link" title="The caption">
                                        <img src="http://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/Water_Dolphin.jpg/800px-Water_Dolphin.jpg" width="300"/>  
                                    </a>
                                </section>
                                <section id="middle">
                                    <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Anthochaera_chrysoptera.jpg/800px-Anthochaera_chrysoptera.jpg" class="without-caption image-link">
                                        <img src="http://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Anthochaera_chrysoptera.jpg/800px-Anthochaera_chrysoptera.jpg" width="300"/>  
                                    </a>
                                    <div class="clearfix"><br></div>
                                    <a href="http://www.capitallife.co.id/wp-content/uploads/2015/07/CPP-1.png" data-source="http://commons.wikimedia.org/wiki/Commons:Picture_of_the_day" class="with-caption image-link" title="The caption">
                                        <img src="http://www.capitallife.co.id/wp-content/uploads/2015/07/CPP-1.png" width="300"/>  
                                    </a>
                                </section>
                                <aside id="sidebar">
                                    <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Anthochaera_chrysoptera.jpg/800px-Anthochaera_chrysoptera.jpg" class="without-caption image-link">
                                        <img src="http://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Anthochaera_chrysoptera.jpg/800px-Anthochaera_chrysoptera.jpg" width="300"/>  
                                    </a>
                                    <div class="clearfix"><br></div>
                                    <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/Water_Dolphin.jpg/800px-Water_Dolphin.jpg" data-source="http://commons.wikimedia.org/wiki/Commons:Picture_of_the_day" class="with-caption image-link" title="The caption">
                                        <img src="http://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/Water_Dolphin.jpg/800px-Water_Dolphin.jpg" width="300"/>  
                                    </a>
                                </aside>-->
                                <p style="text-indent: 30px;">Ada stereotype di masyarakat,  membuat perencanaan keuangan pribadi itu susah atau rumit. Sebenarnya membuat perencanaan keuangan pribadi itu cukup mudah asal kita mengetahui kiat atau ilmunya. Namun sayangnya, ilmu perencanaan keuangan pribadi <i>(personal finance)</i> ini tidak atau jarang diajarkan di sekolah formal. Manajemen keuangan yang kita pelajari di sekolah atau di kampus umumnya adalah manajemen keuangan untuk perusahaan <i>(corporate finance)</i>.</p>
                                <p style="text-indent: 30px;">Jadi, pertama-tama dalam melakukan perencanaan keuangan ada beberapa hal yang seharusnya dilakukan yaitu:</p>
                                <ol type="0">
                                    <li>Tentukan tujuan keuangan keluarga, baik jangka pendek, menengah dan panjang </li>
                                    <li>Ketahui kemampuan keuangan saat ini dan potensi keuangan di masa depan </li>
                                    <li>Buatlah laporan arus kas, laporan neraca dan anggaran keuangan keluarga </li>
                                    <li>Dalam melakukan investasi, ketahui profil risiko pribadi, apakah konservatif, moderat atau agresif. </li>
                                    <li>Ketahui cara atau model perencanaan keuangan yang efektif dan mudah dimengerti. </li>
                                </ol>
                                <p style="text-indent: 30px;">Untuk poin kelima, kami menyarankan satu model perencanaan keuangan yang  mudah diterapkan. Kami menyebutnya “Piramida Keuangan”. Model ini berasal dari konsep,   membuat perencanaan keuangan sama seperti membangun rumah. Semuanya berawal dari pondasi yang kuat sehingga bangunan diatasnya bisa kuat menghadapi topan badai. Jadi perencanaan keuangan itu seperti membangun “rumah keuangan” yang seharusnya kuat pondasinya sehingga apabila terjadi “badai” atau krisis keuangan maka keluarga kita tetap aman secara finansial. 
                                    Berikut ini merupakan gambar dari “Piramida Keuangan” yang bisa diikuti sebagai model dalam membuat perencanaan keuangan pribadi. <p style="text-indent: 30px;">

                                <div align="center"><img src="<?php echo base_url() ?>images/infou.jpg" ><br></div>
                                <p style="text-indent: 30px;">Pada gambar Piramida Keuangan,  ada enam kotak dibawah piramida. Itulah  enam buah batu yang merupakan pondasi bagi “bangunan” diatasnya. Nah, seperti membangun rumah maka pondasilah yang paling penting agar rumah itu kuat dan tidak akan ambruk apabila diterpa hujan bahkan badai sekalipun.</p>
                                <p style="text-indent: 30px;">Membangun keuangan pribadi ibarat membangun rumah, dengan enam batu pondasi sebagai proteksi kekayaan pribadi atau keluarga kita. Keenam   batu inilah yang memberikan proteksi apabila terjadi “badai keuangan” menimpa diri kita dan keluarga.</p>
                                <p style="text-indent: 30px;">Marilah  kita menelaah satu persatu batu pondasi ini. Batu pertama adalah kebutuhan dasar <i>(basic needs)</i>. Artinya, apabila kita sudah memiliki penghasilan maka tentu saja yang akan dipenuhi terlebih dahulu adalah kebutuhan dasar seperti sandang, pangan dan papan. Setelah kebutuhan dasar untuk diri kita dan keluarga terpenuhi maka kita bisa beralih ke batu pondasi yang berikutnya. </p>
                                <p style="text-indent: 30px;">Batu pondasi kedua adalah proteksi keluarga <i>(family protection)</i>. Apabila uang kita sudah “berlebih” setelah kebutuhan dasar terpenuhi, maka keamanan keluarga adalah prioritas yang perlu kita perhatikan. Proteksi keluarga bertujuan untuk memproteksi keamanan keluarga kita dari berbagai risiko. Proteksi keluarga terdiri atas dua  jenis, yaitu proteksi aset dan proteksi pendapatan. Proteksi aset artinya, kita melakukan proteksi atas aset yang kita miliki dari risiko kehilangan, kebakaran ataupun kerusakan. Umumnya, proteksi aset ini berupa asuransi umum seperti asuransi kendaraan bermotor dan asuransi rumah tinggal, kantor atau pabrik. Sedangkan proteksi pendapatan artinya, kita melakukan proteksi atas pendapatan kita sebagai kepala keluarga apabila tertimpa sakit, cacat atau meninggal. </p>
                                <p style="text-indent: 30px;">Dengan kata lain, apabila seorang kepala keluarga terkena risiko sakit, cacat atau meninggal maka pendapatan bulanan yang seharusnya hilang karena terjadinya risiko, tetap tersedia buat keluarga yang ditinggalkan, sama  seperti apabila kepala keluarga tersebut sehat walafiat dan bekerja seperti biasa. Inilah yang dimaksud dengan proteksi pendapatan.</p>
                                <p style="text-indent: 30px;">Batu pondasi yang ketiga adalah dana darurat <i>(emergency fund)</i>. Dana ini berguna  apabila terjadi sesuatu yang sifatnya darurat, misalnya kendaraan milik keluarga ngadat dan membutuhkan perbaikan dengan biaya besar. Atau yang lebih tragis, Anda terkena PHK. Sampai Anda mendapatkan pekerjaan  kembali, tersedia dana yang memang disediakan khusus untuk situasi tersebut. Umumnya, besarnya dana darurat  tiga  sampai enam kali pendapatan bulanan.</p>
                                <p style="text-indent: 30px;">Batu pondasi berikutnya adalah dana pendidikan anak. Ini merupakan tujuan keuangan yang sangat penting bagi keluarga yang sudah mempunyai anak. Sebagai orang tua, tentu saja kita menginginkan pendidikan yang terbaik untuk anak. Perlu  perencanaan yang matang untuk mempersiapkan dana pendidikan ini. Sebab, apabila tidak direncanakan, pendidikan anak kita kemungkinan besar tidak tercapai seperti yang kita harapkan. </p>
                                <p style="text-indent: 30px;">Perencanaan dana pendidikan anak <i>(child education planning)</i> meliputi dua kegiatan utama, yaitu menabung atau berinvestasi dan proteksi. Setelah menghitung berapa besar dana yang dibutuhkan untuk biaya pendidikan anak, maka menabung atau investasi merupakan cara untuk mencapainya.</p>
                                <p style="text-indent: 30px;">Namun, perlu diperhitungkan pula risiko yang mungkin terjadi, yaitu  pada saat kita sebagai orang tua sedang menabung atau berinvestasi untuk tujuan tersebut. Disinilah peran proteksi (asuransi jiwa, asuransi kesehatan ataupun asuransi cacat) sangat dibutuhkan. Sehingga apabila terjadi risiko  seperti sakit, cacat bahkan meninggal, yang mengakibatkan perencanaan dana pendidikan anak kita terganggu,  proteksi bisa mengambil alih tanggungjawab ini. Melalui proteksi,  perencanaan tersebut bisa tetap berjalan sesuai  yang kita harapkan. </p>
                                <p style="text-indent: 30px;">Produk keuangan yang bisa digunakan sebagai tools atau alat dalam perencanaan dana pendidikan  antara lain tabungan pendidikan, asuransi beasiswa, unit linked, reksa dana plus asuransi dan sejenisnya. Yang penting, produk keuangan tersebut mempunyai unsur tabungan atau investasi dan proteksi. </p>
                                <p style="text-indent: 30px;">Batu pondasi yang kelima adalah dana pensiun <i>(retirement fund)</i>. Sebagai orang yang bekerja sekian lama, tentu saja kita menginginkan di satu saat kita bisa menikmati pensiun dengan tenang, baik moril maupun materiil. Pensiun disini bukan berarti berhenti kerja total, melainkan,  apapun yang kita inginkan,  apakah tetap bekerja penuh waktu, bekerja paruh waktu atau sama sekali tidak bekerja, kita sudah memiliki dana yang cukup untuk membiayai hidup kita dan <i>life style</i> atau gaya hidup kita, tanpa tergantung pada penghasilan dari pekerjaan kita tersebut.</p>
                                <p style="text-indent: 30px;">Dalam melakukan perencanaan dana pensiun <i>(retirement planning)</i> ini, unsur tabungan atau investasi sangatlah dominan. Proteksi yang perlu diperhitungkan disini adalah asuransi kesehatan. Sebab tidak ada gunanya apabila kita pensiun namun sakit-sakitan sehingga menghabiskan dana pensiun yang sudah bertahun-tahun dikumpulkan. Yang penting dalam merencanakan dana pensiun ini, kita perlu tahu secara pasti apa <i>life style</i> yang diinginkan pada waktu pensiun dan berapa besar dana yang perlu dikumpulkan untuk membiayai <i>life style</i> tersebut.</p>
                                <p style="text-indent: 30px;">Adapun produk keuangan yang bisa digunakan sebagai alat dalam perencanaan dana pensiun antara lain Dana Pensiun Lembaga Keuangan (DPLK), anuitas, unit linked, reksa dana, obligasi, properti, saham dan sejenisnya. Perlu pula diketahui, jangka waktu investasi dan profil risiko investor dalam perencanaan dana pensiun ini. Sebab hal tersebut akan sangat mempengaruhi pemilihan produk keuangan yang akan dibeli sebagai alat atau solusi dalam mencapai tujuan penyediaan dana pensiun tersebut. </p>
                                <p style="text-indent: 30px;">Batu pondasi yang keenam adalah warisan. Warisan  disini bisa mempunyai dua tujuan. Yang pertama, sebagai distribusi kekayaan atau memberikan aset yang dimiliki kepada pihak yang diinginkan walaupun sang pewaris masih hidup, atau mewariskan kepada ahli waris setelah meninggal. Yang kedua, apabila kita tidak mempunyai aset atau kekayaan, atau justru memiliki utang, baik  kepada bank atau pihak lain, agar jangan sampai utang tersebut diwariskan kepada ahli waris kita. Itulah sebabnya perencanaan warisan (estate planning) sangat diperlukan untuk kedua tujuan tersebut, baik untuk mendistribusikan kekayaan atau aset, ataupun menghindarkan diwariskannya utang bagi anak cucu kita. </p>
                                <p style="text-indent: 30px;">Memang, sebagai orang Timur, kita umumnya enggan atau bahkan tabu bila membicarakan warisan selagi masih hidup. Namun, banyak fakta yang memperlihatkan, tanpa adanya perencanaan warisan yang baik, ahli waris dalam satu keluarga bisa menjadi ribut dan akhirnya tujuan sang ahli waris untuk mendistribusikan kekayaan berupa aset yang dikumpulkan seumur hidupnya tidak tercapai. Lebih menyedihkan,  jika keluarga yang ditinggalkan terbebani utang yang belum lunas. Solusi dalam melakukan perencanaan warisan ini adalah menghitung besarnya kekayaan dan utang yang dimiliki dan menuliskan apa yang harus dilakukan atas kekayan atau utang tersebut dalam surat wasiat <i>(wills)</i>. Pembuatan surat wasiat ini bisa menggunakan jasa pengacara atau notaris yang dipercaya.</p>
                                <p style="text-indent: 30px;">Selanjutnya, kita tiba pada batu-batu di bagian Akumulasi Kekayaan pada Konsep Piramida Keuangan. Batu-batu di bagian ini mengambil asumsi,  apabila batu-batu pondasi piramida keuangan ini sudah terpenuhi (walaupun belum 100% namun sudah dilakukan) maka berarti sudah ada “uang lebih” yang bisa diinvestasikan. </p>
                                <p style="text-indent: 30px;">Dengan kata lain, pendapatan seseorang atau individu tersebut sudah makin meningkat. Di sisi lain, dengan pendapatan yang makin meningkat ini, maka individu tersebut juga menginginkan tingkat imbal hasil yang makin tinggi untuk mengakumulasikan pendapatan atau aset (kekayaan) yang dimiliki. Kalau di batu pondasi, seseorang atau individu  “bekerja untuk mendapatkan uang” maka di bagian Akumulasi Kekayaan, “uang bekerja untuk uang”. </p>
                                <p style="text-indent: 30px;">Masalahnya, sesuai dengan teori investasi, bila tingkat imbal hasil atau return yang diharapkan makin tinggi maka konsekuensinya adalah risiko atau risk akan makin tinggi pula.  Makin tinggi expected return dari suatu instrumen investasi, makin tinggi pula risiko terjadinya penurunan, bahkan hilangnya uang atau modal yang diinvestasikan dalam instrumen investasi tersebut.</p>
                                <p style="text-indent: 30px;">Intrumen investasi yang paling rendah risikonya dan berarti paling rendah pula tingkat imbal hasilnya adalah tabungan dan deposito. Seperti kita ketahui, tabungan atau deposito dijamin tingkat bunganya dalam periode waktu tertentu oleh bank yang mengeluarkan produk tabungan atau deposito tersebut. Bahkan di Indonesia, untuk dana simpanan masyarakat yang ada di bank sampai jumlah tertentu dijamin oleh Lembaga Penjamin Simpanan (LPS), suatu badan yang didirikan pemerintah untuk melindungi dana masyarakat yang ada di bank dari risiko seperti penutupan bank <i>(likuidasi)</i>.</p>
                                <p style="text-indent: 30px;">Tingkat suku bunga simpanan atau deposito perbankan semakin lama menunjukkan tren penurunan yang disebabkan terus turunnya  suku bunga patokan yang ditetapkan Bank Indonesia, dan makin rendahnya tingkat inflasi. Semakin baik perekonomian suatu negara, umumnya semakin rendah bunga perbankan.</p>
                                <p style="text-indent: 30px;">Di batu selanjutnya adalah instrumen investasi unit linked. Unit linked merupakan instrumen investasi yang cukup aman dengan tingkat imbal hasil investasi yang cukup menarik. Cukup aman, karena pada unit linked terdapat unsur proteksi dalam bentuk asuransi jiwa, sehingga apabila terjadi risiko hidup seperti sakit, cacat atau meninggal, unit linked bisa memberikan manfaat bagi pemegang polisnya.  </p>
                                <p style="text-indent: 30px;">Disamping memberikan manfaat asuransi jiwa, tingkat imbal hasil investasi produk ini juga menarik, walaupun akan sangat bergantung pada kemana   dana tersebut diinvestasikan. Dana atau fund  dibedakan atas portofolio investasinya yaitu di money market, fixed income, balanced/mixed atau equity. Dan  perbedaaan dalam portofolio investasi ini menyebabkan tingkat imbal hasil dan risiko juga berbeda antara fund yang satu dengan yang lain.</p>
                                <p style="text-indent: 30px;">Sebenarnya, produk unit linked ini masih relatif baru di Indonesia yaitu mulai berkembang sekitar tahun 2000-an. Namun dengan berkembangnya pengetahuan masyarakat mengenai investasi, produk unit linked ini makin diminati. Karena, selain tetap memberikan proteksi, pemegang polis  bisa menikmati tingkat imbal hasil yang sesuai dengan kondisi pasar, apakah di pasar uang atau di pasar modal. Produk unit linked ini sendiri merupakan pengembangan dari produk reksa dana. Bedanya dengan reksa dana,  unit link ada unsur proteksinya. Ini disebabkan karena unit linked merupakan pengembangan dari produk asuransi.</p>
                                <p style="text-indent: 30px;">Batu selanjutnya adalah reksa dana. Reksa dana adalah produk investasi yang makin dikenal di Indonesia, sejak pertama kali diterbitkan tahun 1996. Reksa dana ini cukup aman dari sisi risiko dan  tingkat pengembalian hasil investasinya bisa sangat menarik. Mengapa demikian? Karena  reksa dana dikelola secara profesional oleh orang-orang yang ahli  dibidangnya, yaitu yang kita kenal sebagai wakil manager investasi. Reksa dana diterbitkan  perusahaan yang memiliki izin sebagai perusahaan manajemen investasi atau umumnya disebut fund manager atau fund management company.   </p>
                                <p style="text-indent: 30px;">Seperti  unit linked, reksa dana dibedakan atas portofolio investasinya. Di Indonesia ada pembagian empat jenis reksa dana, yaitu reksa dana pasar uang <i>(money market fund)</i>, reksa dana pendapatan tetap <i>(fixed income fund)</i>, reksa dana saham <i>(equity fund)</i> dan reksa dana campuran <i>(balanced atau mixed fund)</i>. Tingkat risiko dan pengembalian investasinya berbeda diantara fund-fund tersebut. Yang paling rendah risiko dan tingkat pengembaliannya adalah reksa dana pasar uang dan yang paling tinggi tingkat risikonya namun dengan tingkat pengembalian tertinggi adalah reksa dana saham. Selain reksa dana yang sudah disebutkan, saat ini sudah ada reksa dana khusus seperti Reksa Dana Terstruktur (Structured Mutual Fund), Reksa Dana Indeks, Reksa Dana Syariah, Reksa Dana Valas, Reksa Dana Sektoral, Reksa Dana Lokal, dan Reksa Dana ETF (Exchange Traded Fund).</p>
                                <p style="text-indent: 30px;">Setelah reksa dana, selanjutnya ada obligasi. Obligasi bisa dibagi berdasarkan jenis dan tingkat risikonya. Dimulai dari obligasi pemerintah yang dikenal juga sebagai ORI (Obligasi Ritel Indonesia) dan obligasi korporasi. Obligasi korporas memberikan yield atau kupon bunga yang lebih tinggi namun tingkat keamanannya lebih rendah. Obligasi biasanya diperingkat oleh lembaga pemeringkat independen agar diketahui tingkat risiko gagal bayarnya <i>(default risk)</i> sampai sejauh mana. Makin tinggi peringkatnya, makin kecil risikonya. </p>
                                <p style="text-indent: 30px;">Kemudian di batu berikutnya, properti (dan emas) merupakan instumen investasi yang tingkat risikonya lebih tinggi daripada reksa dana, namun memiliki  potensi tingkat pengembalian yang  sangat menarik. Syaratnya, investor yang melakukan investasi dalam instrumen ini harus mempunyai tujuan investasi jangka panjang.  Sebab, properti (dan emas) dari sisi likuiditas tidaklah begitu tinggi dibandingkan instrumen-intrumen investasi dibawahnya. Namun, properti (dan emas) merupakan instumen investasi yang baik untuk mengatasi inflasi, khususnya dalam jangka panjang. Untuk berinvestasi di properti (dan emas) diperlukan pengetahuan yang cukup sebab apabila salah lokasi untuk memilih properti atau salah membaca keadaan ekonomi ketika membeli emas, maka uang yang ditanamkan bisa berkurang sangat signifikan. </p>
                                <p style="text-indent: 30px;">Setelah properti, di tingkat yang selanjutnya ada intrumen saham. Saham, seperti yang diketahui, memiliki tingkat likuiditas yang tinggi namun fluktuasinya juga sangat tinggi. Inilah yang menyebabkan tingkat risikonya juga cukup tinggi, tetapi  berpotensi menghasilkan  return yang juga sangat tinggi. Untuk melakukan investasi di intrumen saham ini diperlukan pengetahuan pasar modal yang mendalam dan waktu yang fleksibel. Yang terakhir adalah intrumen investasi yang sifatnya spekulasi. Intrumen yang sifatnya spekulasi ini bisa berbentuk investasi di valuta asing atau bursa komoditi. Bisa juga berbentuk investasi di barang-barang antik atau barang-barang koleksi. Yang jelas, disebut spekulasi sebab tingkat fluktuasinya bisa sangat tinggi, misalnya valas. Saat ini, nilai  tukar rupiah terhadap dolar  bisa Rp 10.000/US$, namun besok bisa saja menjadi Rp 11.000/US$. Ada pula investasi berupa barang yang   mengharapkan untung atau gain yang sangat tinggi pada saat dijual, contohnya barang antik atau benda seni.</p>
                                <p style="text-indent: 30px;">Kesimpulannya, investasi di intrumen manapun sah-sah saja sejauh mempertimbangkan tujuan keuangan yang ingin dicapai dan jangka waktu investasi. Selain itu, kita perlu mengetahui sejauh mana tingkat risiko yang mampu kita hadapi <i>(risk profile)</i>. Juga sangat diperlukan, pengetahuan yang cukup apabila ingin melakukan investasi di instumen investasi tertentu. Apabila produk investasi yang kita pilih terlalu rumit atau kita tidak punya cukup waktu, mintalah bantuan profesional.</p>


                            </div>
                        </div>
                    </div>
                </div>

                <?php echo $template['footer'] ?>
            </div>
        </div>
        <div class="gototop js-top">
            <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
        </div>
        <?php echo $combine['js']; ?>
        <script>
            var timoutWarning = 840000; 
            var timoutNow = 300000; // Timeout in 15 mins.
            var logoutUrl = '<?php echo base_url() ?>home'; // URL to logout page.
    
            var warningTimer;
            var timeoutTimer;
    
            // Start timers.
            function StartTimers() {
                warningTimer = setTimeout("IdleWarning()", timoutWarning);
                timeoutTimer = setTimeout("IdleTimeout()", timoutNow);
            }
            // Reset timers.
            function ResetTimers() {
                clearTimeout(warningTimer);
                clearTimeout(timeoutTimer);
                StartTimers();
                $("#timeout").dialog('close');
            }
    
            // Show idle timeout warning dialog.
            function IdleWarning() {
                $("#timeout").dialog({
                    modal: true
                });
            }
            // Logout the user.
            function IdleTimeout() {
                window.location = logoutUrl;
            }
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                //Disable full page
                $('body').bind('cut copy paste', function (e) {
                    e.preventDefault();
                });

                //Disable part of page
                $('#id').bind('cut copy paste', function (e) {
                    e.preventDefault();
                });
            });
        </script>
        <script type="text/javascript">function disableselect(n) {
                return!1
            }
            function reEnable() {
                return!0
            }
            document.onselectstart = new Function("return false"), document.oncontextmenu = new Function("return false"), window.sidebar && (document.onmousedown = disableselect, document.onclick = reEnable), window.history.forward(), window.onload = function () {
                window.history.forward()
            }, window.onunload = function () {};</script>
        <script>
            $('.without-caption').magnificPopup({
                type: 'image',
                closeOnContentClick: true,
                closeBtnInside: false,
                mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
                image: {
                    verticalFit: true
                },
                zoom: {
                    enabled: true,
                    duration: 300 // don't foget to change the duration also in CSS
                }
            });

            $('.with-caption').magnificPopup({
                type: 'image',
                closeOnContentClick: true,
                closeBtnInside: false,
                mainClass: 'mfp-with-zoom mfp-img-mobile',
                image: {
                    verticalFit: true,
                    titleSrc: function (item) {
                        return item.el.attr('title') + ' &middot; <a class="image-source-link" href="' + item.el.attr('data-source') + '" target="_blank">image source</a>';
                    }
                },
                zoom: {
                    enabled: true
                }
            });


        </script>
    </body>
</html>

