<?php

function get_dob_date($ktp_no)
{
	$st_date = substr($ktp_no,6,6);
	//die($st_date);
	
	$dd = get_dob_day(substr($st_date,0,2));
	$mm = substr($st_date,2,2);
	$yy = get_dob_year(substr($st_date,4,2));
	
	$st_dob = "$yy-$mm-$dd";
	
	//die($st_date . '---' . $yy);
	//die($st_dob);
	
	return $st_dob;
}

/**
 * Tahun Lahir, jika lebih kecil dari tahun current, maka tahun lahir di atas 2000
 */
function get_dob_year($yy)
{
	$ret = "";
	$year_now = date("y");
	if($yy <=  $year_now)
		$ret = "20$yy";
	else
		$ret = "19$yy";
	
	return $ret;
}

/**
 * Tgl Lahir, Jika P tgl lahir dikurangi 40
 */
function get_dob_day($dd)
{
	$ret = "";
	if($dd < 31)
		$ret = $dd;
	else
	{
		$ret = $dd - 40;
	}
				
	return $ret;
}

/**
 * Penentuan Jenis Kelamin
 */
function get_gender($ktp_no)
{
	$icont = 40;
	$dd = substr($ktp_no,6,2);
	//die($dd);
	if($dd <= 31)
		$stval = "L";
	else
		$stval = "P";
		
	return $stval;	
}
