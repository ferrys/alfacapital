<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function google_analytics($uacct = '') {
    $CI = & get_instance();
    if ($uacct != '' || $CI->config->item('google_uacct') != '') {
        if ($CI->config->item('google_uacct') != '') {
            $google_account_id = $CI->config->item('google_uacct');
        }
        if ($uacct != '') {
            $google_account_id = $uacct;
        }
        
        $google_analytics_code = "
            <script type=\"text/javascript\"> 
                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', '" . $google_account_id . "']);
                _gaq.push(['_setDomainName', 'sindonews.com']);
                _gaq.push(['_trackPageview']);

                (function() {
                  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                })();
            </script>
            <script type=\"text/javascript\">
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                ga('create', '" . $google_account_id . "', {'cookieDomain': 'sindonews.com'});
                ga('send', 'pageview');
            </script>";
        
        return $google_analytics_code;
    }
}

function google_analytics_extend($uacct = '

         ', $subdomain_urcin = '') {
    $CI = & get_instance();
    if ($uacct != '' || $CI->config->item('google_uacct') != '') {
        if ($CI->config->item('google_uacct') != '') {
            $google_account_id = $CI->config->item('google_uacct');
        }
        if ($uacct != '') {
            $google_account_id = $uacct;
        }

        $google_analytics_code = '
                <script type = "text/javascript">
                var _gaq = _gaq || [];
                _gaq.push([\'_setAccount\', \'' . $subdomain_urcin . '\']);
                    _gaq.push([\'_trackPageview\']);
                    
                    var _gaq_domain = _gaq || [];
                    _gaq_domain.push([\'_setAccount\', \'' . $google_account_id . '\']);
                    _gaq_domain.push([\'_setDomainName\', \'.sindonews.com\']);
                    _gaq_domain.push([\'_trackPageview\']);
                  (function() {
                    var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true;
                    ga.src = (\'https:\' == document.location.protocol ? \'https://ssl\' : \'http://www\') + \'.google-analytics.com/ga.js\';
                    var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ga, s);
                  })();
                 </script> 
            ';
        return $google_analytics_code;
    }
}

?>