<?php

function metaname($meta = array(),$newline = "\n"){
    $str = '';
    foreach($meta as $key => $val){
        $str .= '<meta name="' . $key . '" content="' . $val . '" />' . $newline;
    }
    return $str;
}

function metafb($meta = array(),$newline = "\n"){
    $str = '';
    foreach($meta as $key => $val){
        $str .= '<meta property="og:' . $key . '" content="' . $val . '" />' . $newline;
    }
    return $str;
}

function add_js($js = ''){
    $CI = & get_instance();
    if(trim($js) == "") return false;
    $string = "<script type=\"text/javascript\" src=\"" . $CI->config->item('template_uri') . $js . "\"></script>\n";
    return $string;
}

function add_external_js($js = ''){
    $CI = & get_instance();
    if(trim($js) == "") return false;
    $string = "<script type=\"text/javascript\" src=\"" . $js . "\"></script>\n";
    return $string;
}

function add_css($css = ''){
    $CI = & get_instance();
    if(trim($css) == "") return false;
    $string = "<link rel=\"stylesheet\" type=\"text/css\" href=\"" . $CI->config->item('template_uri') . $css . "\" />\n";
    return $string;
}

function add_external_css($css = ''){
    $CI = & get_instance();
    if(trim($css) == "") return false;
    $string = "<link rel=\"stylesheet\" type=\"text/css\" href=\"" . $css . "\" />\n";
    return $string;
}

function addMobileCss($csspath,$type = null){
    $media = (($type != null)) ? "media=\"" . $type . "\"" : "";

    $string = "<link rel=\"stylesheet\" " . $media . " href=\"" . $csspath . "\" type=\"text/css\" />\n\t";
    return $string;
}

function addTitle($title){
    $string = "<title>" . $title . "</title>\n";
    return $string;
}

function no_cache(){
    $CI = & get_instance();
    $CI->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
    $CI->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
    $CI->output->set_header('Pragma: no-cache');
    $CI->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
}

