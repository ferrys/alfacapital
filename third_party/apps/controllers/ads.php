<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ads extends CI_Controller {

    function __construct() {
        parent:: __construct();
         $this->load->model('m_cstmr');
        $this->load->library(array('form_validation','session','recaptcha'));
        $this->load->library();
    }
    function index() {
        
      $a =array();
        $ket = "Mengunjungi Halaman http://tokopandai.club";
        $this->m_cstmr->user_visitor($ket);
        $a['combine']['css'] = $this->load->view('template/combinecss', NULL, true);
        $a['combine']['js'] = $this->load->view('template/combinejs', NULL, true);
        $a['title'] = 'Selamat Datang di Toko Pandai Club';
        $s= $this->m_cstmr->get_today_visitor();
        $d['visitor'] =$s[0]['visitor'];
        $r= $this->m_cstmr->get_month_visitor();
        $d['visitorm'] =$r[0]['visitor'];
        $a['template']['footer'] = $this->load->view('template/vfooter', $d, true);
        $a['template']['navigation'] = $this->load->view('template/vmenu', NULL, true);
        $this->form_validation->set_rules('policy_no', 'Nomor Peserta', 'trim|required');
        $this->form_validation->set_rules('dob', 'Tanggal Lahir', 'trim|required|callback_login');
        $a['html']['title'] = "Selamat Datang Toko Pandai Club";
        $metaname = array(
            'description' => "PT Capital Life Indonesia adalah bagian dari Capital Financial Group dan merupakan sister company PT Bank Capital Indonesia Tbk. Grup perusahaan nasional ini berfokus pada industri jasa keuangan yang terintegrasi. PT Capital Life Indonesia terdaftar dan diawasi oleh Otoritas Jasa Keuangan (OJK)
              Capital Life berkomitmen memberikan pelayanan cepat dan terbaik, mendahulukan kepentingan pemegang polis serta pihak yang berhak memperoleh manfaat. Hal ini sesuai dengan visi dan misi perusahaan. 
              Kinerja Capital Life tahun 2015, memberikan efek positif bagi perusahaan yang pada akhirnya Capital Life mendapatkan beberapa penghargaan. Penghargaan yang didapat antara lain “The Best Insurance 2016” dari Majalah Media Asuransi and predikat “Sangat Bagus” atas kinerja keuangan perusahaan asuransi selama tahun 2015 dari Majalah Infobank.",
            'title' => "Selamat Datang Toko Pandai Club",
            'image_src' => base_url()."images/promo_nov.jpg",
            'googlebot-news' => 'index,follow',
        );
        /* create facebook meta tags */
        $metafb = array(
            'title' => $a['html']['title'],
            'type' => 'article',
            'url' => current_url(),
            'image' => base_url()."images/promo_nov.jpg",
            'description' => html_entity_decode($metaname['description']),
            'site_name' => 'tokopandai.club'
        );


        /* create twitter card tags */
        $metatwit = array(
            'twitter:card' => 'summary_large_image',
            'twitter:site' => '@Capitallife',
            'twitter:url' => current_url(),
            'twitter:domain' => 'tokopandai.club',
            'twitter:title' => $metaname['title'],
            'twitter:description' => $metaname['description'],
            'twitter:image:src' => base_url()."images/promo_nov.jpg",
            
        );

        $a['html']['metaname'] = metaname($metaname);
        $a['html']['metaname'] .= metafb($metafb);
        $a['html']['metaname'] .= metaname($metatwit);
        
        if ($this->form_validation->run() === TRUE) 
        {
            if($this->session->userdata('policy_no') == TRUE)
            {
                $berhasil= "SUCCESS";
                $policy= $this->session->userdata('policy_no');
                $this->m_cstmr->log_login($policy,$berhasil);
            }
            else
            {
                $gagal= "GAGAL";
                $policy= $this->input->post('policy_no');
                $this->m_cstmr->log_login($policy,$gagal);
            }
            redirect('home-login','refresh');
        }
        
        $this->session->sess_destroy();
        $this->m_cstmr->hit_home();
        $this->load->view('ads', $a, FALSE);
    }
    function faq() {
        $this->m_cstmr->hit_faq();
        $a['combine']['css'] = $this->load->view('template/combinecss', NULL, true);
        $a['combine']['js'] = $this->load->view('template/combinejs', NULL, true);

        $a['title'] = 'Selamat Datang di Capital Life ND';
        $a['template']['navigation'] = $this->load->view('template/vmenu', NULL, true);
        $s= $this->m_cstmr->get_today_visitor();
        $d['visitor'] =$s[0]['visitor'];
        $a['template']['footer'] = $this->load->view('template/vfooter', $d, true);
        $this->load->view('faq', $a);
    }
}