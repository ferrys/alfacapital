<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Informasi_keuangan extends CI_Controller {
	function __construct() {
        parent:: __construct();
        $this->load->model('m_cstmr');
	}
	function index()
	{
            $this->is_logged_in();
//            $a['html']['css'] = add_css('css/animate.css');
//            $a['html']['css'] .= add_css('css/icomoon.css');
//            $a['html']['css'] .= add_css('css/themify-icons.css');
//            $a['html']['css'] .= add_css('css/bootstrap.css');
//            $a['html']['css'] .= add_css('css/magnific-popup.css');
//            $a['html']['css'] .= add_css('css/owl.carousel.min.css');
//            $a['html']['css'] .= add_css('css/owl.theme.default.min.css');
//            $a['html']['css'] .= add_css('css/style.css');
//            $a['combine']['css'] = $this->load->view('template/combinecss', NULL, true);
//
//            $a['html']['js'] = add_js('js/modernizr-2.6.2.min.js');
//            $a['html']['js'] .= add_js('js/jquery.min.js');
//            $a['html']['js'] .= add_js('js/jquery.easing.1.3.js');
//            $a['html']['js'] .= add_js('js/bootstrap.min.js');
//            $a['html']['js'] .= add_js('js/jquery.waypoints.min.js');
//            $a['html']['js'] .= add_js('js/owl.carousel.min.js');
//            $a['html']['js'] .= add_js('js/jquery.countTo.js');
//            $a['html']['js'] .= add_js('js/jquery.magnific-popup.min.js');
//            $a['html']['js'] .= add_js('js/magnific-popup-options.js');
//            $a['html']['js'] .= add_js('js/main.js');
            
             $a['combine']['css'] = $this->load->view('template/combinecss', NULL, true);
             $a['combine']['js'] = $this->load->view('template/combinejs', NULL, true);
//             if($this->is_logged_in()){
//             $id = $this->session->userdata('policy_no');
//             $a['cos'] = $this->m_cstmr->get_customers($id);}
             if ($this->session->userdata('policy_no') !=""){
             $id = $this->session->userdata('policy_no');
             $a['cos'] = $this->m_cstmr->get_customers($id);}
             $a['title'] = 'Selamat Datang di Capital Life ND';
             $a['template']['navigation'] = $this->load->view('template/vmenu', NULL, true);
             $s= $this->m_cstmr->get_today_visitor();
            $d['visitor'] =$s[0]['visitor'];
             $r= $this->m_cstmr->get_month_visitor();
        $d['visitorm'] =$r[0]['visitor'];
            $a['template']['footer'] = $this->load->view('template/vfooter', $d, true);
             $this->m_cstmr->hit_inf_keu();
             $this->load->view('informasi_keuangan',$a,FALSE);
	}
        
         function is_logged_in() {
            $is_logged_in = $this->session->userdata('policy_no');
            if (!isset($is_logged_in) || $is_logged_in != true) {
                redirect(base_url());
         }
         
         
    }
}
	