<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function __construct() {
        parent:: __construct();
        $this->load->model('m_cstmr');   
    }
    function index() {
        $this->is_logged_in();
        $a = array();
//        $a['html']['css'] = add_css('css/animate.css');
//        $a['html']['css'] .= add_css('css/icomoon.css');
//        $a['html']['css'] .= add_css('css/themify-icons.css');
//        $a['html']['css'] .= add_css('css/bootstrap.css');
//        $a['html']['css'] .= add_css('css/bootstrap-datepicker.css');
//        $a['html']['css'] .= add_css('css/magnific-popup.css');
//        $a['html']['css'] .= add_css('css/owl.carousel.min.css');
//        $a['html']['css'] .= add_css('css/owl.theme.default.min.css');
//        $a['html']['css'] .= add_css('css/style.css');
        
        //$a['html']['js'] = add_js('js/modernizr-2.6.2.min.js');
        
//        $a['html']['js'] = add_js('js/jquery.min.js');
//        $a['html']['js'] .= add_js('js/jquery.easing.1.3.js');
//        $a['html']['js'] .= add_js('js/bootstrap.min.js');
//        $a['html']['js'] .= add_js('js/bootstrap-datepicker.js');
//        $a['html']['js'] .= add_js('js/jquery.waypoints.min.js');
//        $a['html']['js'] .= add_js('js/owl.carousel.min.js');
//        $a['html']['js'] .= add_js('js/jquery.countTo.js');
//        $a['html']['js'] .= add_js('js/jquery.magnific-popup.min.js');
//        $a['html']['js'] .= add_js('js/magnific-popup-options.js');
//        $a['html']['js'] .= add_js('js/main.js');
        
        $a['combine']['css'] = $this->load->view('template/combinecss', NULL, true);
        $a['combine']['js'] = $this->load->view('template/combinejs', NULL, true);
        $a['title'] = 'Selamat Datang di Capital Life ND';
        $id = $this->session->userdata('policy_no');
        
        $a['cos'] = $this->m_cstmr->get_customers($id);
        
        $this->m_cstmr->hit_home_login();
        $a['template']['navigation'] = $this->load->view('template/vmenu', $a, true);
        $s= $this->m_cstmr->get_today_visitor();
        $d['visitor'] =$s[0]['visitor'];
         $r= $this->m_cstmr->get_month_visitor();
        $d['visitorm'] =$r[0]['visitor'];
        $a['template']['footer'] = $this->load->view('template/vfooter', $d, true);
        
        $this->load->view('dashboard', $a, FALSE);
    }
     function is_logged_in() {
        $is_logged_in = $this->session->userdata('policy_no');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            redirect(base_url());
        }
    }
}