<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct() {
        parent:: __construct();
        $this->load->model('m_cstmr');
        $this->load->library(array('form_validation','session','recaptcha'));
        $this->load->library();
    }

    function index() {
        
        $a =array();
         
        $a['combine']['css'] = $this->load->view('template/combinecss', NULL, true);
        $a['combine']['js'] = $this->load->view('template/combinejs', NULL, true);
        $a['title'] = 'Selamat Datang di Toko Pandai Club';
        $s= $this->m_cstmr->get_today_visitor();
        $d['visitor'] =$s[0]['visitor'];
         $r= $this->m_cstmr->get_month_visitor();
        $d['visitorm'] =$r[0]['visitor'];
        $a['template']['footer'] = $this->load->view('template/vfooter', $d, true);
        $a['template']['navigation'] = $this->load->view('template/vmenu', NULL, true);
        
        $this->form_validation->set_rules('policy_no', 'Nomor Peserta', 'trim|required');
        $this->form_validation->set_rules('dob', 'Tanggal Lahir', 'trim|required|callback_login');
        
        $this->session->sess_destroy();
        $this->load->view('home', $a, FALSE);
    }
}