<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Update_cos extends CI_Controller {

    function __construct() {
        parent:: __construct();
        $this->load->model('m_cstmr');
        $this->load->helper(array('date', 'url', 'text', 'general'));
        $this->load->library(array('form_validation', 'curl', 'email'));
    }

    function index() {
        $cname = $this->input->post("cname");
        $phone_no = $this->input->post("phone_no");
        $email = $this->input->post("email");
        $address = $this->input->post("address");
        $id = $this->session->userdata('policy_no');
        $this->form_validation->set_rules('cname', 'cname', 'trim|required');

        $string = $phone_no;
        if ($string[0] == 6) {
            $phone = preg_replace('/62/', '0', $phone_no, 1);
        } else {
            $phone = $phone_no;
        }

        if ($this->form_validation->run() === TRUE) {
            $this->m_cstmr->update_c($cname, $phone, $email, $address, $id);
            $this->m_cstmr->log_update_data();
            $config['protocol'] = "smtp";
            $config['smtp_host'] = 'mail.valdoinfo.com';
            $config['smtp_port'] = '587';
            $config['smtp_user'] = 'tokopandai.club@valdoinfo.com';
            $config['smtp_pass'] = 'Password01';
            $config['mailtype'] = 'text';
            $config['validate'] = TRUE;
            $config['smtp_timeout'] = 5;
            $config['charset'] = 'utf-8';
            $config['newline'] = "\r\n";
            $config['wordwrap'] = TRUE;

            $this->load->library('email');

            $this->email->initialize($config);
            $this->email->from('tokopandai.club@valdoinfo.com', 'care@tokopandai.club');
            $this->email->to($email);
            $this->email->subject('Selamat, Anda sudah terlindungi oleh Capital Life Indonesia');
            $this->email->message('
            <div align="center"><b>SELAMAT DATANG DI TOKO PANDAI CLUB</b></div><br>
            Terima kasih atas kepercayaan Anda memilih PT Capital Life Indonesia <br>sebagai mitra dalam memberikan perlindungan finansial bagi Anda dan Keluarga.
            <br>www.tokopandai.club/p/' . $id . '?t=' . substr(md5(sha1($id)), 0, 5) . '
            <br>Apabila Anda memiliki pertanyaan atau memerlukan tambahan informasi, dapat menghubungi :
            <div align="center"><br><b>Customer Care</b>
            <br>Telp      : (62) (21) 290 23 688
            <br>WA        : (62) (89) 919 19 898
            <br>Fax       : (62) (21) 290 23 338
            <br>E-mail    : care@tokopandai.club</div>
            <br><br>Kami akan selalu mendengar dan memberikan pelayanan terbaik untuk Anda dan Keluarga.
            <br>Hormat kami,
            <br><br><br><br>Protected by PT CAPITAL LIFE INDONESIA');
            $this->email->set_mailtype("html");
            $this->email->set_crlf("\r\n");
            $this->email->send();
            
            $message = "Selamat, anda telah terlindungi Asuransi dari PT Capital Life Indonesia Untuk informasi lebih lanjut silakan klik website www.tokopandai.club/p/".$id."?t=".substr(md5(sha1($id)), 0, 5);
            $c = 'http://192.168.252.1/sendsms?username=sms123&password=sms123&phonenumber='.$phone.'&message='.$message.'&port=gsm-1.1&report=JSON&timeout=20';
            $data = json_decode(file_get_contents(str_replace(" ", "%20", $c)),true);
            $report=$data['report'];
            $e = array(
                'ktp_no' => $this->session->userdata('ktp_no'),
                'sms_status' => $report[0][1][0]['result'],
                'no_hp' =>  $report[0][1][0]['phonenumber'],
                'message' => $message
            );
            $this->db->insert('customers_sms_report', $e);
//            echo '<script language="javascript">';
//            echo 'alert("Data Sudah Terupdate")';
//            echo '</script>';
            redirect("home-login", 'refresh');
        }
        redirect("home-login", 'refresh');
    }

    function update_url() {
        $cname = $this->input->post("cname");
        // $age = $this->input->post("age");
        $phone_no = $this->input->post("phone_no");
        $email = $this->input->post("email");
        $address = $this->input->post("address");
        $id = $this->uri->segment(3);
        $this->form_validation->set_rules('cname', 'cname', 'trim|required');

        $string = $phone_no;
        if ($string[0] == 6) {
            $phone = preg_replace('/62/', '0', $phone_no, 1);
        } else {
            $phone = $phone_no;
        }

        if ($this->form_validation->run() === TRUE) {
            $this->m_cstmr->update_c($cname, $phone, $email, $address, $id);
            $this->m_cstmr->log_update_data();
            $this->smsemail();
        //          $config['smtp_host'] = 'mail.capitallife.co.id'; 
        //          $config['smtp_port'] = '587';
        //          $config['smtp_user'] = 'care.support@capitallife.co.id';
        //          $config['smtp_pass'] = 'Care4support';
        //          $config['smtp_host'] = 'mail.valdo-intl.com'; 
        //          $config['smtp_port'] = '587';
        //          $config['smtp_user'] = 'ferry.sukarto@valdo-intl.com';
        //          $config['smtp_pass'] = 'Pa$$word01';
        //            $config['protocol'] = "smtp";
        //            $config['smtp_host'] = 'mail.valdoinfo.com';
        //            $config['smtp_port'] = '587';
        //            $config['smtp_user'] = 'capitallife@valdoinfo.com';
        //            $config['smtp_pass'] = 'Password01';
        //            // capitallife@valdoinfo.com
        //            //Password01
        //            $config['mailtype'] = 'text';
        //            $config['validate'] = TRUE;
        //            $config['smtp_timeout'] = 5;
        //            $config['charset'] = 'utf-8';
        //            $config['newline'] = "\r\n";
        //            $config['wordwrap'] = TRUE;
        //
        //            $this->load->library('email');
        //
        //            $this->email->initialize($config);
        //            $this->email->from('capitallife@valdoinfo.com', 'Capital Life');
        //            $this->email->to($email);
        //            $this->email->subject('Selamat, Anda sudah terlindungi oleh Capital Life Indonesia');
        //            $this->email->message('
        //            <div align="center"><b>SELAMAT DATANG DI PT CAPITAL LIFE INDONESIA</b></div><br>
        //            Terima kasih atas kepercayaan Anda memilih PT Capital Life Indonesia <br>sebagai mitra dalam memberikan perlindungan finansial bagi Anda dan Keluarga.
        //            <br>Segera lengkapi data anda di https://www.tokopandai.club
        //            <br>Apabila Anda memiliki pertanyaan atau memerlukan tambahan informasi, dapat menghubungi :
        //            <div align="center"><br><b>Customer Care</b>
        //            <br>Telp      : (62) (21) 290 23 688
        //            <br>Fax       : (62) (21) 290 23 338
        //            <br>E-mail    : care@tokopandai.club</div>
        //            <br><br>Kami akan selalu mendengar dan memberikan pelayanan terbaik untuk Anda dan Keluarga.
        //            <br>Hormat kami,
        //            <br><br><br><br>PT CAPITAL LIFE INDONESIA');
        //            $this->email->set_mailtype("html");
        //            $this->email->set_crlf("\r\n");
        //            $this->email->send();
        //
        //            if (substr($phone, 0, 4) == "0811" OR substr($phone, 0, 4) == "0812" OR substr($phone, 0, 4) == "0813" OR substr($phone, 0, 4) == "0821" OR substr($phone, 0, 4) == "0822" OR substr($phone, 0, 4) == "0823" OR substr($phone, 0, 4) == "0852" OR substr($phone, 0, 4) == "0853") {
        //                $message = "'Selamat, anda telah terlindungi Asuransi dari PT Capital Life Indonesia Untuk informasi lebih lanjut silakan klik website https://www.tokopandai.club'";
        //                $url = 'http://192.168.252.1/sendsms?username=sms123&password=sms123&phonenumber=' . $phone_no . '&message=' . $message . '&port=gsm-1.3&report=String&timeout=20';
        //                $url = str_replace(" ", "%20", $url);
        //                file_get_contents($url);
        //            } else if (substr($phone, 0, 4) == "0818" OR substr($phone, 0, 4) == "0819" OR substr($phone, 0, 4) == "0817" OR substr($phone, 0, 4) == "0877" OR substr($phone, 0, 4) == "0878") {
        //                $message = "Selamat, anda telah terlindungi Asuransi dari PT Capital Life Indonesia Untuk informasi lebih lanjut silakan klik website https://www.tokopandai.club";
        //                $url = 'http://192.168.252.1/sendsms?username=sms123&password=sms123&phonenumber=' . $phone_no . '&message=' . $message . '&port=gsm-1.1&report=String&timeout=20';
        //                $url = str_replace(" ", "%20", $url);
        //                file_get_contents($url);
        //            } else if (substr($phone, 0, 4) == "0814" OR substr($phone, 0, 4) == "0815" OR substr($phone, 0, 4) == "0816" OR substr($phone, 0, 4) == "0855" OR substr($phone, 0, 4) == "0856" OR substr($phone, 0, 4) == "0857" OR substr($phone, 0, 4) == "0858") {
        //                $message = "'Selamat, anda telah terlindungi Asuransi dari PT Capital Life Indonesia Untuk informasi lebih lanjut silakan klik website https://www.tokopandai.club'";
        //                $url = 'http://192.168.252.1/sendsms?username=sms123&password=sms123&phonenumber=' . $phone_no . '&message=' . $message . '&port=gsm-1.2&report=String&timeout=20';
        //                $url = str_replace(" ", "%20", $url);
        //                file_get_contents($url);
        //            } else {
        //                $message = "'Selamat, anda telah terlindungi Asuransi dari PT Capital Life Indonesia Untuk informasi lebih lanjut silakan klik website https://www.tokopandai.club'";
        //                $url = 'http://192.168.252.1/sendsms?username=sms123&password=sms123&phonenumber=' . $phone_no . '&message=' . $message . '&port=gsm-1.3&report=String&timeout=20';
        //                $url = str_replace(" ", "%20", $url);
        //                file_get_contents($url);
        //            }
            redirect(base_url() . "p/" . $this->uri->segment(3), 'refresh');
        }
        redirect(base_url() . "p/" . $this->uri->segment(3), 'refresh');
    }

    function smsemail() {
        //0 = before update , 1 = wait to send , 2 = update and send
        $t['list'] = $this->m_cstmr->get_cron_mail_sms_update();
        //var_dump($t['list']);
        $id = $this->session->userdata('policy_no');
        $total = count($t['list']);
        for ($i = 0; $i < $total; $i++) {

            $config['protocol'] = "smtp";
            $config['smtp_host'] = 'mail.valdoinfo.com';
            $config['smtp_port'] = '587';
            $config['smtp_user'] = 'tokopandai.club@valdoinfo.com';
            $config['smtp_pass'] = 'Password01';

            $config['mailtype'] = 'text';
            $config['validate'] = TRUE;
            $config['smtp_timeout'] = 5;
            $config['charset'] = 'utf-8';
            $config['newline'] = "\r\n";
            $config['wordwrap'] = TRUE;

            $this->load->library('email');
            // $email = $this->input->post("email");
            $this->email->initialize($config);
            $this->email->from('tokopandai.club@valdoinfo.com', 'care@tokopandai.club');
            $this->email->to($t['list'][$i]['email']);
            $phone_no = $this->input->post("phone_no");
             $string = $t['list'][$i]['phone_no'];
              if ($string[0] == 6) {
                 $phone = preg_replace('/62/', '0', $t['list'][$i]['phone_no'], 1);
             } else {
            $phone = $t['list'][$i]['phone_no'];
            }
            $this->email->subject('Selamat, Anda sudah terlindungi oleh Capital Life Indonesia');
            $this->email->message('
            <div align="center"><b>SELAMAT DATANG DI PT CAPITAL LIFE INDONESIA</b></div><br>
            Terima kasih atas kepercayaan Anda memilih PT Capital Life Indonesia <br>sebagai mitra dalam memberikan perlindungan finansial bagi Anda dan Keluarga.
            <br>Segera lengkapi data anda di https://www.tokopandai.club
            <br>Apabila Anda memiliki pertanyaan atau memerlukan tambahan informasi, dapat menghubungi :
            <div align="center"><br><b>Customer Care</b>
            <br>Telp      : (62) (21) 290 23 688
            <br>Fax       : (62) (21) 290 23 338
            <br>E-mail    : care@tokopandai.club</div>
            <br><br>Kami akan selalu mendengar dan memberikan pelayanan terbaik untuk Anda dan Keluarga.
            <br>Hormat kami,
            <br><br><br><br>PT CAPITAL LIFE INDONESIA');
            $this->email->set_mailtype("html");
            $this->email->set_crlf("\r\n");
            $this->email->send();

//            if (substr($phone, 0, 4) == "0811" OR substr($phone, 0, 4) == "0812" OR substr($phone, 0, 4) == "0813" OR substr($phone, 0, 4) == "0821" OR substr($phone, 0, 4) == "0822" OR substr($phone, 0, 4) == "0823" OR substr($phone, 0, 4) == "0852" OR substr($phone, 0, 4) == "0853") {
//                $message = "'Selamat, anda telah terlindungi Asuransi dari PT Capital Life Indonesia Untuk informasi lebih lanjut silakan klik website www.tokopandai.club/profile/" . $id . "?token=" . substr(md5(sha1($id)), 0, 5) . "'";
//                $url = 'http://192.168.252.1/sendsms?username=sms123&password=sms123&phonenumber=' . $phone_no . '&message=' . $message . '&port=gsm-1.3&report=String&timeout=20';
//                $url = str_replace(" ", "%20", $url);
//                file_get_contents($url);
//            } else if (substr($phone, 0, 4) == "0818" OR substr($phone, 0, 4) == "0819" OR substr($phone, 0, 4) == "0817" OR substr($phone, 0, 4) == "0877" OR substr($phone, 0, 4) == "0878") {
                $message = "'Selamat, anda telah terlindungi Asuransi dari PT Capital Life Indonesia Untuk informasi lebih lanjut silakan klik website www.tokopandai.club/profile/" . $id . "?token=" . substr(md5(sha1($id)), 0, 5) . "'";
                $url = 'http://192.168.252.1/sendsms?username=sms123&password=sms123&phonenumber=' . $phone . '&message=' . $message . '&port=gsm-1.1&report=String&timeout=20';
                $url = str_replace(" ", "%20", $url);
                file_get_contents($url);
//            } else if (substr($phone, 0, 4) == "0814" OR substr($phone, 0, 4) == "0815" OR substr($phone, 0, 4) == "0816" OR substr($phone, 0, 4) == "0855" OR substr($phone, 0, 4) == "0856" OR substr($phone, 0, 4) == "0857" OR substr($phone, 0, 4) == "0858") {
//                $message = "'Selamat, anda telah terlindungi Asuransi dari PT Capital Life Indonesia Untuk informasi lebih lanjut silakan klik website www.tokopandai.club/profile/" . $id . "?token=" . substr(md5(sha1($id)), 0, 5) . "'";
//                $url = 'http://192.168.252.1/sendsms?username=sms123&password=sms123&phonenumber=' . $phone_no . '&message=' . $message . '&port=gsm-1.2&report=String&timeout=20';
//                $url = str_replace(" ", "%20", $url);
//                file_get_contents($url);
//            } else {
//                $message = "'Selamat, anda telah terlindungi Asuransi dari PT Capital Life Indonesia Untuk informasi lebih lanjut silakan klik website www.tokopandai.club/profile/" . $id . "?token=" . substr(md5(sha1($id)), 0, 5) . "'";
//                $url = 'http://192.168.252.1/sendsms?username=sms123&password=sms123&phonenumber=' . $phone_no . '&message=' . $message . '&port=gsm-1.3&report=String&timeout=20';
//                $url = str_replace(" ", "%20", $url);
//                file_get_contents($url);
//            }
        }
    }

    function register_c() {
        $this->load->model('m_cstmr');
        $this->load->library("form_validation");
        $cname = $this->input->post("cname");
        //$age = $this->input->post("age");
        $phone_no = $this->input->post("phone_no");
        $email = $this->input->post("email");
        $address = $this->input->post("address");
        //$gender = $this->input->post("gender");
        $ktp_no = $this->input->post("ktp_no");
        $expired = date("Y-m-d", mktime(0, 0, 0, date("m") + 6, date("d") - 1, date("Y")));
                            //$expires = date($expired, mktime(0, 0, 0, date("m"), date("d") - 1, date("Y")));
        $expired_res = date("Ymd", mktime(0, 0, 0, date("m") + 6, date("d"), date("Y")));
        //$dob = date('Y-m-d', strtotime(strtr($this->input->post("dob"), '/', '-')));

        $start = date("Y-m-d");
        $deadline = $expired;
        /* KTP */
        $dt_dob = get_dob_date($ktp_no);
        $st_gender = get_gender($ktp_no);
        //curdate - 6 bulan -> 18 s/d 64
        $start_req = date("Ymd", mktime(0, 0, 0, date("m") - 6, date("d"), date("Y")));
        $int_age = 0;
        $sql = "SELECT YEAR((FROM_DAYS(TO_DAYS('$start_req') - TO_DAYS('$dt_dob')))) AS umur_req, YEAR((FROM_DAYS(TO_DAYS(curdate()) - TO_DAYS('$dt_dob')))) AS umur";

        //die($sql);
        $query = $this->db->query($sql);

        $row = $query->row_array();
        $int_age_req = $row['umur_req'];
        $int_age = $row['umur'];
        /* KTP END */
        if ($row['umur'] <= 18) {
            echo '<script language="javascript">';
            echo 'alert("Umur kurang dari 18 tahun.\nMinimal Umur 18 Tahun\nRegistrasi Gagal")';
            echo '</script>';
            redirect("member-register", 'refresh');
        } else if ($row['umur'] >= 60) {
            echo '<script language="javascript">';
            echo 'alert("Umur lebih dari 60 tahun.\nMaksimal Umur 59 Tahun\nRegistrasi Gagal")';
            echo '</script>';
            redirect("member-register", 'refresh');
        }

        if ($st_gender == "L") {
            $gelar = "Bapak";
        } else {
            $gelar = "Ibu";
        }
        
        $this->form_validation->set_rules('cname', 'cname', 'trim|required');
        $this->form_validation->set_rules('ktp_no', 'KTP', 'trim|required|callback_ktp_handle');
        if ($this->form_validation->run() === TRUE) {
        $this->m_cstmr->input_c($cname, $dt_dob, $int_age, $phone_no, $email, $address, $st_gender, $ktp_no,$start,$deadline);
            //$this->sendpayremainder();
        $config['protocol'] = "smtp";
        $config['smtp_host'] = 'mail.valdoinfo.com';
        $config['smtp_port'] = '587';
        $config['smtp_user'] = 'tokopandai.club@valdoinfo.com';
        $config['smtp_pass'] = 'Password01';

        $config['mailtype'] = 'text';
        $config['validate'] = TRUE;
        $config['smtp_timeout'] = 5;
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;

        $this->load->library('email');
        // $email = $this->input->post("email");
        $this->email->initialize($config);
        $this->email->from('tokopandai.club@valdoinfo.com', 'care@tokopandai.club');
        $this->email->to($email);
        //$phone_no = $this->input->post("phone_no");
        // $string = $t['list'][$i]['phone_no'];
        //  if ($string[0] == 6) {
        //     $phone = preg_replace('/62/', '0', $t['list'][$i]['phone_no'], 1);
        // } else {
        //}
        $this->email->subject('Selamat datang di Asuransi Capitallife Indonesia');
        $this->email->message('Selamat ' . $gelar . ' ' . $cname . ',<br><br>
                
                <div >Anda sudah terdaftar dengan data sebagai calon Member Asuransi Capital Life :
                <br><br>
                No KTP : ' . $ktp_no . '<br>
                Nama   : ' . $cname . '<br>
                Iuran  : Rp 20.000,00<br>
                <br>
                Silahkan membayar di Alfamart terdekat
                dengan menunjukan KTP sebagai bukti untuk melakukan pembayaran
                untuk mengaktifkan Member Asuransi Capitallife
                <br><br>
                <br>Apabila Anda memiliki pertanyaan atau memerlukan tambahan informasi, dapat menghubungi :
                <div align="center"><br><b>Customer Care</b>
                <br>Telp      : (62) (21) 290 23 688
                <br>Fax       : (62) (21) 290 23 338
                <br>E-mail    : care@tokopandai.club</div>
                <br><br>Kami akan selalu mendengar dan memberikan pelayanan terbaik untuk Anda dan Keluarga.
                <br>Hormat kami,
                <br><br><br><br>PT CAPITAL LIFE INDONESIA
                </div>
                ');
        $this->email->set_mailtype("html");
        $this->email->set_crlf("\r\n");
        $this->email->send();
        $this->sendnotifdaftar();
            redirect("thankyou", 'refresh');
        }

        redirect("member-register", 'refresh');
    }

    function create_pdf_url() {
        $idx = $this->uri->segment(3);
        $this->load->library('m_pdf');
        //if ($idx != "") {
        $t['list'] = $this->m_cstmr->get_customers_url($idx);
        $html = $this->load->view('print', $t, true);
        $pdfFilePath = str_replace(' ', '_', $t['list'][0]['cname']) . '_' . $t['list'][0]['policy_no'] . ".pdf";

        $pdf = $this->m_pdf->load();
        $pdf->showImageErrors = true;
        $pdf->AddPage('L', // L - landscape, P - portrait
                '', '', '', '', 0, // margin_left
                0, // margin right
                0, // margin top
                0, // margin bottom
                0, // margin header
                0); // margin footer
        //
            //$pdf->SetProtection(array(),$t['list'][0]['policy_no']);
        $pdf->WriteHTML($html);
        $pdf->Output($pdfFilePath, "D");
//        } else {
//            redirect('');
//        }
    }

    function create_pdf() {

        $this->load->library('m_pdf');
        if ($this->session->userdata('policy_no') != "") {
            $t['list'] = $this->m_cstmr->get_customers();
            $html = $this->load->view('print', $t, true);
            $pdfFilePath = str_replace(' ', '_', $t['list'][0]['cname']) . '_' . $t['list'][0]['policy_no'] . ".pdf";

            $pdf = $this->m_pdf->load();
            $pdf->showImageErrors = true;
            $pdf->AddPage('L', // L - landscape, P - portrait
                    '', '', '', '', 0, // margin_left
                    0, // margin right
                    0, // margin top
                    0, // margin bottom
                    0, // margin header
                    0); // margin footer
            //
            //$pdf->SetProtection(array(),$t['list'][0]['policy_no']);
            $pdf->WriteHTML($html);
            $pdf->Output($pdfFilePath, "D");
        } else {
            redirect('');
        }
    }

    function check_ktp_exists() {
        $ktp = $this->input->post('ktp_no');
        //$dob = date('Y-m-d', strtotime(strtr($this->input->post("dob"), '/', '-')));
        /* KTP */
        $dt_dob = get_dob_date($ktp);
        $st_gender = get_gender($ktp);
        //curdate - 6 bulan -> 18 s/d 64
        $start_req = date("Ymd", mktime(0, 0, 0, date("m") - 6, date("d"), date("Y")));
        $int_age = 0;
        $sql = "SELECT YEAR((FROM_DAYS(TO_DAYS('$start_req') - TO_DAYS('$dt_dob')))) AS umur_req, YEAR((FROM_DAYS(TO_DAYS(curdate()) - TO_DAYS('$dt_dob')))) AS umur";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        $int_age_req = $row['umur_req'];
        $int_age = $row['umur'];


        $exists = $this->m_cstmr->check_ktp_availablity($ktp);
        $count = count($exists);
        if ($this->input->post('ktp_no') == '') {
            echo "<span class='status-not-available'>Isi Kolom KTP!</span>";
        } else {
            if (strlen($this->input->post('ktp_no')) == 16) {
                if ($row['umur'] < 18) {
                    echo "<span class='status-not-available'>Umur anda kurang dari 18</span>";
                } else if ($row['umur'] > 59) {
                    echo "<span class='status-not-available'>Umur anda lebih dari 60</span>";
                } else if ($count > 0) {
                    echo "<span class='status-not-available'>KTP Sudah Terdaftar</span>";
                } else {
                    echo "<span class='status-available'>KTP Available</span>";
                }
            } else {
//            echo '<script language="javascript">';
//            echo 'alert("message successfully sent")';
//            echo '</script>';
                echo "<span class='status-not-available'>KTP TIDAK VALID!</span>";
            }
        }
    }

    function logout() {
        $this->m_cstmr->log_out();
        $this->session->sess_destroy();
        header("Expires: " . gmdate("D, d M Y H:i:s", time()) . " GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        header("Pragma: no-cache");
        redirect('');
    }

    public function ktp_handle() {
        $ktp_no = addslashes($this->input->post('ktp_no'));
        $nama_sama = $this->m_cstmr->check_ktp_exist($ktp_no);
        if ($nama_sama) {
            $this->form_validation->set_message('ktp_handle', 'KTP Sudah Ada Didatabase');
            return false;
        } else {
            return true;
        }
    }

    public function hub_kami() {
//        $config['protocol'] = "smtp";
//        $config['smtp_host'] = 'mail.valdoinfo.com'; 
//        $config['smtp_port'] = '587';
//        $config['smtp_user'] = 'capitallife@valdoinfo.com';
//        $config['smtp_pass'] = 'Password01';

        $config['mailtype'] = 'text';
        $config['validate'] = TRUE;
        $config['smtp_timeout'] = 5;
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;

        $this->load->library('email');
        //$email = $this->input->post("email");
        $subject = $this->input->post("subject");
        $message = $this->input->post("message");

        $this->email->initialize($config);
        $this->email->from('capitallife@valdoinfo.com', 'care@tokopandai.club');
        $this->email->to("capitallife@valdoinfo.com");
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->set_mailtype("html");
        $this->email->set_crlf("\r\n");
        $this->email->send();
        redirect(base_url() . "contact-us");
    }
    
    
    
    function alldata()
    {
        $key = hash('sha256',md5(sha1(md5("valdo-sms-bls-key"))));
       //$key = md5(sha1(md5("valdo-sms-bls-key")));
       /*url--http://tokopandai.club/blast?key=63c7379d421368a48243107b1f96f077ff57841bc0f2e4900914451b0ea78a24--*/
       if($_GET['key'] != $key){redirect(base_url());}
        $vi = $this->m_cstmr->get_customers_all();
        $data[] = array(
            "success" => true,
            "errors" => [],
            'Data_capital' => $vi
        );
        
        echo json_encode($data);
        
    }

    function sendnotifdaftar() {
        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
        $json = json_decode($location, true);
        if($json['region_name'] != "")
        {$region =$json['region_name'];}
        else {$region ="Indonesia";}
        $this->load->library('user_agent');
        if ($this->agent->is_browser())
        {
                $agent = $this->agent->browser().' '.$this->agent->version();
        }
        elseif ($this->agent->is_robot())
        {
                $agent = $this->agent->robot();
        }
        elseif ($this->agent->is_mobile())
        {
                $agent = $this->agent->mobile();
        }
        else
        {
                $agent = 'Unidentified User Agent';
        }
        $platform= $this->agent->platform();
        $config['protocol'] = "smtp";
        $config['smtp_host'] = 'mail.valdoinfo.com';
        $config['smtp_port'] = '587';
        $config['smtp_user'] = 'tokopandai.club@valdoinfo.com';
        $config['smtp_pass'] = 'Password01';

        $config['mailtype'] = 'text';
        $config['validate'] = TRUE;
        $config['smtp_timeout'] = 5;
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;

        $this->load->library('email');
        $ktp_no = $this->input->post("ktp_no");
        $dt_dob = get_dob_date($ktp_no);
        $st_gender = get_gender($ktp_no);
        //curdate - 6 bulan -> 18 s/d 64
        $start_req = date("Ymd", mktime(0, 0, 0, date("m") - 6, date("d"), date("Y")));
        $int_age = 0;
        $sql = "SELECT YEAR((FROM_DAYS(TO_DAYS('$start_req') - TO_DAYS('$dt_dob')))) AS umur_req, YEAR((FROM_DAYS(TO_DAYS(curdate()) - TO_DAYS('$dt_dob')))) AS umur";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        $int_age_req = $row['umur_req'];
        $int_age = $row['umur'];
        $this->email->initialize($config);
        $this->email->from('tokopandai.club@valdoinfo.com', 'care@tokopandai.club');
        $this->email->to("sdkdevelopers@gmail.com");

        $this->email->subject('NEW REGISTRATION DATA ' . date('d-m-Y H:s'));
        $this->email->message('<div style=" font-family:Arial, Helvetica, sans-serif;background-color:#EEEEEE;"><b>Hello Team</b>,<br><br>
        <br><br>
        <div align="center" style="text-align:left; margin: auto; width: 300px; border: 3px solid black; padding:10px; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#666666; background-color:#EEEEEE;">
        <justify><b>New user has Registration data :</b><br><br>
        No KTP : ' . $this->input->post("ktp_no") . '<br>
        Nama : '.$this->input->post("cname").'<br>
        Umur : '.$int_age.'<br>
        Gender : '.$st_gender.'<br>
        No HP : '.$this->input->post("phone_no").'<br>
        Alamat : '.$this->input->post("address").'<br>
        Email : ' . $this->input->post("email") . '<br>
        Sumber : Tokopandai.club<br>
        IP Address : '.$this->input->ip_address().'<br>
        Browser :'.$agent.'<br>
        Flatform : '.$platform.'<br>
        City : '.$region.'<br>
        ISP : '.$hostname.'<br></justify></div>
        <br>
        <br><b>Hormat kami,</b>
        <br><br><br><b>VALDO INC</b></div>');
        $this->email->set_mailtype("html");
        $this->email->set_crlf("\r\n");
        $this->email->send();
    }
}