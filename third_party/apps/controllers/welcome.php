<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Welcome extends CI_Controller {

    function __construct() {
        parent:: __construct();
        $this->load->model('m_cstmr');
        $this->load->library(array('form_validation','session','recaptcha'));
        $this->load->library();
    }

    function index() {
       
        $a =array();
        $ket = "Mengunjungi Halaman http://www.tokopandai.club";
        $this->m_cstmr->user_visitor($ket);
        $a['combine']['css'] = $this->load->view('template/combinecss', NULL, true);
        $a['combine']['js'] = $this->load->view('template/combinejs', NULL, true);
        $a['title'] = 'Selamat Datang di Toko Pandai Club';
        $s= $this->m_cstmr->get_today_visitor();
        $d['visitor'] =$s[0]['visitor'];
         $r= $this->m_cstmr->get_month_visitor();
        $d['visitorm'] =$r[0]['visitor'];
        $a['template']['footer'] = $this->load->view('template/vfooter', $d, true);
        $a['template']['navigation'] = $this->load->view('template/vmenu', NULL, true);
        
        $this->form_validation->set_rules('policy_no', 'Nomor Peserta', 'trim|required');
        $this->form_validation->set_rules('dob', 'Tanggal Lahir', 'trim|required|callback_login');
        
        $a['html']['title'] = "Selamat Datang Toko Pandai Club";
        $metaname = array(
            'description' => "PT Capital Life Indonesia adalah bagian dari Capital Financial Group dan merupakan sister company PT Bank Capital Indonesia Tbk. Grup perusahaan nasional ini berfokus pada industri jasa keuangan yang terintegrasi. PT Capital Life Indonesia terdaftar dan diawasi oleh Otoritas Jasa Keuangan (OJK)
              Capital Life berkomitmen memberikan pelayanan cepat dan terbaik, mendahulukan kepentingan pemegang polis serta pihak yang berhak memperoleh manfaat. Hal ini sesuai dengan visi dan misi perusahaan. 
              Kinerja Capital Life tahun 2015, memberikan efek positif bagi perusahaan yang pada akhirnya Capital Life mendapatkan beberapa penghargaan. Penghargaan yang didapat antara lain “The Best Insurance 2016” dari Majalah Media Asuransi and predikat “Sangat Bagus” atas kinerja keuangan perusahaan asuransi selama tahun 2015 dari Majalah Infobank.",
            'title' => "Selamat Datang Toko Pandai Club",
            'image_src' => base_url()."images/promo_nov.jpg",
            'googlebot-news' => 'index,follow',
        );
        
        /* create facebook meta tags */
        $metafb = array(
            'title' => $a['html']['title'],
            'type' => 'article',
            'url' => current_url(),
            'image' => base_url()."images/promo_nov.jpg",
            'description' => html_entity_decode($metaname['description']),
            'site_name' => 'tokopandai.club'
        );


        /* create twitter card tags */
        $metatwit = array(
            'twitter:card' => 'summary_large_image',
            'twitter:site' => '@Capitallife',
            'twitter:url' => current_url(),
            'twitter:domain' => 'tokopandai.club',
            'twitter:title' => $metaname['title'],
            'twitter:description' => $metaname['description'],
            'twitter:image:src' => base_url()."images/promo_nov.jpg",
            
        );

        $a['html']['metaname'] = metaname($metaname);
        $a['html']['metaname'] .= metafb($metafb);
        $a['html']['metaname'] .= metaname($metatwit);
        
        if ($this->form_validation->run() === TRUE) 
        {
            if($this->session->userdata('policy_no') == TRUE)
            {
                $berhasil= "SUCCESS";
                $policy= $this->session->userdata('policy_no');
                $this->m_cstmr->log_login($policy,$berhasil);
            }
            else
            {
                $gagal= "GAGAL";
                $policy= $this->input->post('policy_no');
                $this->m_cstmr->log_login($policy,$gagal);
            }
            redirect('home-login','refresh');
        }
        
        $this->session->sess_destroy();
        $this->m_cstmr->hit_home();
        $this->load->view('welcome', $a, FALSE);
    }
    
//    function home() {
//        $a =array();
//         
//        $a['combine']['css'] = $this->load->view('template/combinecss', NULL, true);
//        $a['combine']['js'] = $this->load->view('template/combinejs', NULL, true);
//        $a['title'] = 'Selamat Datang di Toko Pandai Club';
//        $a['template']['footer'] = $this->load->view('template/vfooter', NULL, true);
//        $a['template']['navigation'] = $this->load->view('template/vmenu', NULL, true);
//        
//        $this->form_validation->set_rules('policy_no', 'Nomor Peserta', 'trim|required');
//        $this->form_validation->set_rules('dob', 'Tanggal Lahir', 'trim|required|callback_login');
//        
//        $this->session->sess_destroy();
//        $this->load->view('home', $a, FALSE);
//    }
    
    public function validate_captcha() 
    { 
        $recaptcha = trim($this->input->post('g-recaptcha-response')); 
        $userIp= $this->input->ip_address(); 
        $secret='6Ldr3xgUAAAAAN1avqHjbs30hJgPxm-z-lSJRMtV'; 
        $data = array( 
            'secret' => "$secret", 
            'response' => "Recaptcha required", 
            'remoteip' =>"$userIp" ); 
        $verify = curl_init(); 
        curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($verify, CURLOPT_POST, true); 
        curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data)); 
        curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false); 
        curl_setopt($verify, CURLOPT_RETURNTRANSFER, true); 
        $response = curl_exec($verify); 
        $status= json_decode($response, true); 
        if(empty($status['success']))
        {
            $this->session->sess_destroy();
            return FALSE;
        }
        else
        { 
             return TRUE;
        } 
    } 
    
    function login() {
       
        $policy_no = addslashes(str_replace(' ', '', $this->input->post('policy_no')));
        $dob = addslashes($this->input->post('dob'));
        $data = $this->m_cstmr->validate($policy_no,$dob);
        $jum = count($data);
        //$image = new Securimage();
        //$is_valid = 0;
        if ($jum==0) 
        {
            $this->form_validation->set_message('login', 'Nomor peserta atau tanggal lahir tidak cocok');
            return false;
        }
//        else if($image->check(['captcha_code']) === TRUE)
//        {
//             $this->form_validation->set_message('login', 'Recaptcha required');
//             return false;
//        }
        else 
        {
            return true;
        }
    }
    
    function is_logged_in() 
    {
        $is_logged_in = $this->session->userdata('policy_no');
        if (!isset($is_logged_in) || $is_logged_in == true) {
            redirect(base_url());
        }
    }

    function logouts() 
    {
       //$this->log_out_log();
        
        $this->session->sess_destroy();
        header("Expires: " . gmdate("D, d M Y H:i:s", time()) . " GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        header("Pragma: no-cache");
        redirect(current_url());
        
    }
     
    function logout() {
       //$this->log_out_log();  
        $this->session->sess_destroy();
        header("Expires: " . gmdate("D, d M Y H:i:s", time()) . " GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        header("Pragma: no-cache");
        redirect('');
    }
    function logoutss() {
       //$this->log_out_log();  
        $this->session->sess_destroy();
        header("Expires: " . gmdate("D, d M Y H:i:s", time()) . " GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        header("Pragma: no-cache");
        redirect('home');
    }
    
    function hit()
    {
        /*url--http://tokopandai.club/API-DATA?key=63c7379d421368a48243107b1f96f077ff57841bc0f2e4900914451b0ea78a24--*/
        $key = hash('sha256',md5(sha1(md5("valdo-sms-bls-key"))));
        if($_GET['key'] != $key){redirect(base_url());}
        $visitor['post'] = $this->m_cstmr->get_page_visitor();
        echo json_encode($visitor);
        
    }
}
