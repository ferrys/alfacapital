<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rekonsiliasi_bulanan extends CI_Controller {

    function __construct() {
        parent:: __construct();
        $this->load->model('m_cstmr');
    }

    function index() {
        $key = md5(sha1(md5("valdo-sms-bls-key")));

        /* url--http://tokopandai.club/rekonsiliasi_bulanan?key=fa1b08166895825c39feec06a92bfca3-- */
        if ($_GET['key'] != $key) {
            redirect(base_url());
        }
        $t['lis'] = $this->m_cstmr->get_rekon_monthly();
        $a['data'] = $this->load->view('print/data_bulanan', $t, true);
        $this->load->view('print/excel_bulanan', $a, FALSE);
        $this->sendmailtovaldo();
    }

    function sms_bulanan() {
        $key = md5(sha1(md5("valdo-sms-bls-key")));
        /* url--http://tokopandai.club/rekonsiliasi_bulanan/sms_bulanan?key=fa1b08166895825c39feec06a92bfca3-- */
        if ($_GET['key'] != $key) {
            redirect(base_url());
        }
        $x = array();
        $t['sms'] = $this->m_cstmr->get_sms_monthly();
        $x['datasms'] = $this->load->view('print/data_sms_bulanan', $t, true);
        $this->load->view('print/excel_sms_bulanan', $x, FALSE);
        $this->sendmailsmstovaldo();
    }

    function sendmailsmstovaldo() {
        $config['protocol'] = "smtp";
        $config['smtp_host'] = 'mail.valdoinfo.com';
        $config['smtp_port'] = '587';
        $config['smtp_user'] = 'tokopandai.club@valdoinfo.com';
        $config['smtp_pass'] = 'Password01';
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;

        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from('tokopandai.club@valdoinfo.com', 'SMS VALDO REPORT');
        $this->email->to(array('ferry.sukarto@valdo-intl.com', 'Danus.Saputra@valdo-intl.com'));
        $this->email->subject('SMS REPORT BULANAN');
        $this->email->message('
            Dear Valdo Team, <br><br><br>
            Berikut hasil rekonsiliasi selama 1 bulan pada <br>' . date('M Y', strtotime('-1 months')) . '
            <br><br><br>
            Regards,<br><br><br>
            Valdo inc.');
        $dir = FCPATH . "recon/xls_to_capital";
        chdir($dir);
        $date = date('M Y', strtotime('-1 months'));
        array_multisort(array_map("2017-06-06", ($files = glob("*_" . $date . ".*"))), SORT_DESC, $files);
        $this->email->attach(FCPATH . "recon/xls_to_capital/sms_" . $date . ".xls");
        $this->email->set_mailtype("html");
        $this->email->send();
    }

    function sendmailtovaldo() {
        $config['protocol'] = "smtp";
        $config['smtp_host'] = 'mail.valdoinfo.com';
        $config['smtp_port'] = '587';
        $config['smtp_user'] = 'tokopandai.club@valdoinfo.com';
        $config['smtp_pass'] = 'Password01';
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;

        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from('tokopandai.club@valdoinfo.com', 'VALDO REPORT');
        $this->email->to(array('ferry.sukarto@valdo-intl.com', 'Danus.Saputra@valdo-intl.com'));
        //'Danus.Saputra@valdo-intl.com'
        $this->email->subject('REPORT PAYMENT BULANAN');
        $this->email->message('
            Dear Valdo Team, <br><br><br>
            Berikut hasil rekonsiliasi selama 1 bulan pada <br>' . date('M Y', strtotime('-1 months')) . '
            <br><br><br>
            Regards,<br><br><br>
            Valdo inc.');
        $dir = FCPATH . "recon/xls_to_capital";
        chdir($dir);
        $date = date('M Y', strtotime('-1 months'));
        array_multisort(array_map("2017-06-06", ($files = glob("*_" . $date . ".*"))), SORT_DESC, $files);
        $this->email->attach(FCPATH . "recon/xls_to_capital/" . $date . ".xls");
        $this->email->set_mailtype("html");
        $this->email->send();
    }

    function sms_blast_karyawan_capital() {
        $key = md5(sha1(md5("valdo-sms-bls-key")));
        /* url--http://tokopandai.club/rekonsiliasi_bulanan/sms_blast_karyawan_capital?key=fa1b08166895825c39feec06a92bfca3-- */
        if ($_GET['key'] != $key) {
            redirect(base_url());
        }
        $x = array();
        $t['sms'] = $this->m_cstmr->get_sms_blast_karyawan_capital();
        $x['datasms'] = $this->load->view('print/data_sms_karyawan_capital', $t, true);
        $this->load->view('print/excel_sms_karyawan_capital', $x, FALSE);
        $this->sendmailsmsblasttovaldo();
    }

    function sendmailsmsblasttovaldo() {
        $config['protocol'] = "smtp";
        $config['smtp_host'] = 'mail.valdoinfo.com';
        $config['smtp_port'] = '587';
        $config['smtp_user'] = 'tokopandai.club@valdoinfo.com';
        $config['smtp_pass'] = 'Password01';
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;

        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from('tokopandai.club@valdoinfo.com', 'SMS VALDO REPORT');
        $this->email->to(array('ferry.sukarto@valdo-intl.com', 'Danus.Saputra@valdo-intl.com'));
        $this->email->subject('SMS REPORT BULANAN');
        $this->email->message('
            Dear Valdo Team, <br><br><br>
            Berikut hasil Blast SMS ke karyawan capital
            <br><br><br>
            Regards,<br><br><br>
            Valdo inc.');
        $dir = FCPATH . "recon/xls_to_capital";
        chdir($dir);
        //array_multisort(array_map("2017-06-06", ($files = glob("*_" . $date . ".*"))), SORT_DESC, $files);
        $this->email->attach(FCPATH . "recon/xls_to_capital/sms_blast_capital.xls");
        $this->email->set_mailtype("html");
        $this->email->send();
    }

    function export() {
        $t['sms'] = $this->m_cstmr->get_karyawan_valdo();
//        var_dump($t['sms']);
//        exit();
        $t['title'] = "KARYAWAN VALDO BLAST REPORT";
        $this->load->view('print/export_data',$t,FALSE);
        //echo json_encode($t['sms']);
    }

    function send_to_halodoc() {
//         $key = md5(sha1(md5("valdo-sms-bls-key")));
//        /* url--http://tokopandai.club/rekonsiliasi_bulanan/send_to_halodoc?key=fa1b08166895825c39feec06a92bfca3-- */
//        if ($_GET['key'] != $key) {
//            redirect(base_url());
//        }
        $t['lis'] = $this->m_cstmr->send_to_halodoc();

        $x['datasms'] = $this->load->view('print/data_halodoc', $t, true);
        $this->load->view('print/excel_halodoc', $x, FALSE);
        if (count($t['lis']) != 0) {
            $this->sendmailsmsblasttohalodoc();
            for ($i = 0; i < count($t['lis']); $i++) {
                $data = array('send_to_halodoc ' => 1);
                $this->db->update('customers', $data, array('ktp_no' => $t['lis'][$i]['ktp_no']));
            }
       }
    }

    function sendmailsmsblasttohalodoc() {
        $config['protocol'] = "smtp";
        $config['smtp_host'] = 'mail.valdoinfo.com';
        $config['smtp_port'] = '587';
        $config['smtp_user'] = 'tokopandai.club@valdoinfo.com';
        $config['smtp_pass'] = 'Password01';
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;


        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from('tokopandai.club@valdoinfo.com', 'Data peserta Capitallife ' . date('d-m-Y', strtotime('-8 days')) . ' sampai ' . date('d-m-Y', strtotime('-1 days')) . '');
        $this->email->to(array('mktgroup.support@capitallife.co.id','capitallife@halodoc.com', 'danus.Saputra@valdo-intl.com','ferrysukarto@gmail.com'));
        //$this->email->to(array('ferrysukarto@gmail.com'));
        $this->email->subject('Data peserta');
        $this->email->message('
        Dear Halodoc, <br><br><br>
        Berikut terlampir data peserta Capitallife di tokopandai.club yang sudah melengkapi data no telepon.
        ' . date('d-m-Y', strtotime('-8 days')) . ' sampai ' . date('d-m-Y', strtotime('-1 days')) . '
        <br><br><br>
        Regards,<br><br><br>
        Valdo inc.');
        $dir = FCPATH . "recon/xls_to_capital";
        chdir($dir);
        $this->email->attach(FCPATH . "recon/xls_to_capital/data_pemegang_polis.xls");
        $this->email->set_mailtype("html");
        $this->email->send();
    }

    function sendmailtes() {
        $config['protocol'] = "smtp";
        $config['smtp_host'] = 'mail.valdoinfo.com';
        $config['smtp_port'] = '587';
        $config['smtp_user'] = 'tokopandai.club@valdoinfo.com';
        $config['smtp_pass'] = 'Password01';
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;
        $css = $this->load->view('template/combinecss', NULL, true);

        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from('tokopandai.club@valdoinfo.com', 'Contoh Email');
        $this->email->to('ferrysukarto@gmail.com');
        $this->email->cc(array('ferry.sukarto@valdo-intl.com'));
        $this->email->subject('Contoh Email');
        $this->email->message('<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
  <meta name="format-detection" content="date=no"> <!-- disable auto date linking in iOS 7-9 -->
  <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS 7-9 -->
  <title>Two Columns to Rows (Simple)</title>
  
  <style type="text/css">
body {
  margin: 0;
  padding: 0;
  -ms-text-size-adjust: 100%;
  -webkit-text-size-adjust: 100%;
}

table {
  border-spacing: 0;
}

table td {
  border-collapse: collapse;
}

.ExternalClass {
  width: 100%;
}

.ExternalClass,
.ExternalClass p,
.ExternalClass span,
.ExternalClass font,
.ExternalClass td,
.ExternalClass div {
  line-height: 100%;
}

.ReadMsgBody {
  width: 100%;
  background-color: #ebebeb;
}

table {
  mso-table-lspace: 0pt;
  mso-table-rspace: 0pt;
}

img {
  -ms-interpolation-mode: bicubic;
}

.yshortcuts a {
  border-bottom: none !important;
}

@media screen and (max-width: 599px) {
  .force-row,
  .container {
    width: 100% !important;
    max-width: 100% !important;
  }
}
@media screen and (max-width: 400px) {
  .container-padding {
    padding-left: 12px !important;
    padding-right: 12px !important;
  }
}
.ios-footer a {
  color: #aaaaaa !important;
  text-decoration: underline;
}
a[href^="x-apple-data-detectors:"],
a[x-apple-data-detectors] {
  color: inherit !important;
  text-decoration: none !important;
  font-size: inherit !important;
  font-family: inherit !important;
  font-weight: inherit !important;
  line-height: inherit !important;
}
 .alnright { text-align: right; }
</style>
</head>

<body style="margin:0; padding:0;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<!-- 100% background wrapper (grey background) -->
<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
  <tr>
    <td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #F0F0F0;">

      <br>

      <!-- 600px container (white background) -->
      <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px">
        <tr>
          
        </tr>
        <tr>
          <td class="container-padding content" align="left" style="padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff">
            <br>
<br>


<br>

<!-- example: two columns (simple) -->

<!--[if mso]>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr><td width="50%" valign="top"><![endif]-->

    <table width="264" border="0" cellpadding="0" cellspacing="0" align="left" class="force-row">
      <tr>
        <td class="col" valign="top" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333;width:100%">
          <strong><img width="100px" src="http://tokopandai.club/images/new-cli-logo-old.png"></strong>
         
        </td>
      </tr>
    </table>

    <!--[if mso]></td><td width="50%" valign="top"><![endif]-->

    <table width="264" border="0" cellpadding="0" cellspacing="0" align="right" class="force-row">
      <tr>
        <td class="col" valign="top" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:right;color:#333333;width:100%">
        <strong><img width="100px" src="http://tokopandai.club/images/logo-halodoc.png"></strong>
      
        </td>
      </tr>
    </table>

<!--[if mso]></td></tr></table><![endif]-->


<!--/ end example -->

<br>



<div class="body-text" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
 <div class="text-center col-md-8 col-md-offset-2 "style="text-align: center;margin-top: 50px;"><b style="font-size: 18px;"> Terima kasih telah bergabung dengan kami</b><br>
                         Dengan menjadi member asuransi Capital life, kamu mendapatkan 3 keuntungan dari Halodoc-aplikasi yang memberikan kemudahan dengan memberikan solusi layanan kesehatan yang lengkap dan terpercaya melalu fitur Hubungi Dokter, Apotik Antar & Lab Services.
                         </div> 
                          
                          <div class="col-md-8 col-md-offset-2 " style="margin-bottom: 50px;margin-top: 50px; background-color:#d61945; -webkit-border-radius: 10px;
-moz-border-radius: 10px;
border-radius: 10px;"><br><div style="text-align: center;"><b style="color: #ffffff!important" >3 Keuntungan, yaitu</b> : </div>
                              <ul style="color: #ffffff!important"><li>Gratis 4x konsultasi kesehatan dengan dokter specialis</li>
                              <li>Gratis konsultasi kesehatan dengan 24/7 standby dokter umum</li>
                              <li>Gratis ongkos kirim untuk setip pembelian obat/vitamin</li></ul>
                       
                         </div> <div style="text-align: center;"><u><a href="https://ch5xj.app.goo.gl/SUYj" target="_blank" style="color: #53d4d6;"> Gunakan Sekarang!</a></u></div>
</div>

<br>
          </td>
        </tr>
        <tr>
          <td class="container-padding footer-text" align="left"  style="text-align: center;background-color: #161356; font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
           <b>Costumer Care</b><br>
                      Telp : (62)(21) 290 23 688<br>
                      WA : (62) (89) 919 19 898<br>
                      Fax : (62) (21) 290 23 338 <br>
                      E-mail :care@tokopandai.club
          </td>
        </tr>
      </table>
<!--/600px container -->
    </td>
  </tr>
</table>
<!--/100% background wrapper-->

</body>
</html>
');
        $this->email->set_mailtype("html");
        if ($this->email->send()) {
            echo 'Success';
        }
        //$this->email->send();
    }

}
