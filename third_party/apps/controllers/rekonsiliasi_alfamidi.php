<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rekonsiliasi_alfamidi extends CI_Controller {

    function __construct() {
        parent:: __construct();
        $this->load->model('m_cstmr');
    }

    function index() {
        $key = md5(sha1(md5("valdo-sms-bls-key")));
        
       /*url--http://tokopandai.club/rekonsiliasi_alfamidi?key=fa1b08166895825c39feec06a92bfca3--*/
       if($_GET['key'] != $key)
       {
           redirect(base_url());
       }
        
        $t['lis'] = $this->m_cstmr->get_cron_mail_sms_alfamidi();
        
        
        if( count($t['lis'])>0){
        $a['data'] = $this->load->view('print/data', $t, true);
        $this->load->view('print/excel', $a, FALSE);
        for($i=0;$i<count($t['lis']);$i++)
        {
             $e = array(
                    'midi' => '1',
                );
                $where = array(
                    'ktp_no' => $t['lis'][$i]['ktp_no'],
                );
                $this->db->update('customers', $e, $where);
        }
        
        $this->sendmailtocapital();}
        $this->m_cstmr->anu();
        
    }

    function sendmailtocapital() {
        $config['protocol'] = "smtp";
        $config['smtp_host'] = 'mail.valdoinfo.com';
        $config['smtp_port'] = '587';
        $config['smtp_user'] = 'tokopandai.club@valdoinfo.com';
        $config['smtp_pass'] = 'Password01';
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;

        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from('tokopandai.club@valdoinfo.com', 'ALFAMIDI-VALDO Reconcile');
        $this->email->to(array('mktgroup.support@capitallife.co.id'));
        $this->email->cc(array('Danus.Saputra@valdo-intl.com','ferrysukarto@gmail.com'));
        //'mktgroup.support@capitallife.co.id','Danus.Saputra@valdo-intl.com',
        $this->email->subject('VALDO Reconcile');
        $this->email->message('
            Dear Capital Life, <br><br><br>
            Berikut kami lampirkan hasil reconcile data dari Alfamidi untuk tanggal <br>'.date('d-m-Y').'
            <br><br><br>
            Regards,<br><br><br>
            Valdo inc.');
        $dir = FCPATH . "recon/xls_to_capital";
        chdir($dir);
        $date = date("d-m-Y");
        array_multisort(array_map("2017-06-06", ($files = glob("*_" . $date . ".*"))), SORT_DESC, $files);
        $batch = count($files);
        $this->email->attach(FCPATH . "recon/xls_to_capital/batch" . $batch . "_" . $date . ".xls");
        $this->email->set_mailtype("html");
        $this->email->send();
    }
}