<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . '/third_party/PHPExcel/IOFactory.php';
require_once APPPATH . '/third_party/PHPExcel.php';

class Registrasi extends CI_Controller {

    function __construct() {
        parent:: __construct();
        $this->load->model('m_cstmr');
    }
    
    function index()
    {
        $a = array();
        
        $key = 'uvuvwevwevweonyetenyevweugwemubwemosas';
         if($_GET['token'] != $key){redirect(base_url());}
        $a['pesan'] = '';
        if (isset($_POST["submit"])) {
            if (isset($_FILES["file"])) {
                if ($_FILES["file"]["error"] > 0) {
                    // echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
                } else {
                    if (file_exists($_FILES["file"]["name"])) {
                        unlink($_FILES["file"]["name"]);
                    }
                    $storagename = "images/discussdesk.xlsx";
                    move_uploaded_file($_FILES["file"]["tmp_name"], $storagename);
                    $inputFileName = 'images/discussdesk.xlsx';
                    try {
                        $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
                    } catch (Exception $e) {
                        die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
                    }
                    $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                    $arrayCount = count($allDataInSheet);
                    for ($i = 2; $i <= $arrayCount; $i++) {
                        
                        $ktp = trim($allDataInSheet[$i]["A"]);
                        $name = trim($allDataInSheet[$i]["B"]);
                        $tanggal_lahir = trim($allDataInSheet[$i]["C"]);
                        $email = trim($allDataInSheet[$i]["D"]);
                        $gender = trim($allDataInSheet[$i]["E"]);
                        $alamat = trim($allDataInSheet[$i]["F"]);
                        $nohp = trim($allDataInSheet[$i]["G"]);
                       // var_dump($ktp);
                        date_default_timezone_set('asia/jakarta');
                        $query = $this->m_cstmr->get_member_by_ktp($ktp);
                        if ($query[0]['ktp_no'] == "" && $ktp!="") 
                            {
                            $this->m_cstmr->upload_member($ktp,$name,$tanggal_lahir,$email,$gender,$alamat,$nohp);
//                            $this->m_cstmr->upload_member_tes($ktp,$name,$tanggal_lahir,$email,$gender,$alamat,$nohp);

                         $a['pesan'] ='
                          <strong><div class="alert alert-success btn-delay">
                          <strong></strong> Success! Upload  new member
                        </div>';
                            } 
                        else {
                         //  echo '<script>alert("Duplicate Data!")</script>';
                            
                        }
                        
                    }
                    
//                    $a['pesan'] ='<div class="alert alert-success">
//                    <strong></strong> You Have  '. (count($a['new'])).' new member
//                  </div>';
               // redirect('http://www.tokopandai.club/registrasi?token=uvuvwevwevweonyetenyevweugwemubwemosas');
                }
            }
        } else {
          //  echo "No file selected <br />";
        }
        
        $a['html']['css'] = add_css('/file/bootstrap.css');
        $a['html']['js'] = add_js('/file/jquery.js');
        $a['html']['js'] .= add_js('/file/bootstrap.js');
        $this->load->view('vupload', $a, FALSE);
    
    }
    
}