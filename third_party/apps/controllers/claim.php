<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Claim extends CI_Controller {

    function __construct() {
        parent:: __construct();
        $this->load->model('m_cstmr');
    }
    
    function index() {
       $ket = "Mengunjungi Halaman http://tokopandai.club/claim";
        $this->m_cstmr->user_visitor($ket);
        //        $a['html']['css'] = add_css('css/combine.css');
        //        $a['html']['css'] = add_css('css/animate.css');
        //        $a['html']['css'] .= add_css('css/icomoon.css');
        //        $a['html']['css'] .= add_css('css/themify-icons.css');
        //        $a['html']['css'] .= add_css('css/bootstrap.css');
        //        $a['html']['css'] .= add_css('css/magnific-popup.css');
        //        $a['html']['css'] .= add_css('css/owl.carousel.min.css');
        //        $a['html']['css'] .= add_css('css/owl.theme.default.min.css');
        //        $a['html']['css'] .= add_css('css/style.css');
        //        $a['combine']['css'] = $this->load->view('template/combinecss', NULL, true);
        $a['combine']['css'] = $this->load->view('template/combinecss', NULL, true);
        $a['combine']['js'] = $this->load->view('template/combinejs', NULL, true);
        $a['html']['title'] = "Toko Pandai Club :: Claim";
        $metaname = array(
            'description' => "PT Capital Life Indonesia adalah bagian dari Capital Financial Group dan merupakan sister company PT Bank Capital Indonesia Tbk. Grup perusahaan nasional ini berfokus pada industri jasa keuangan yang terintegrasi. PT Capital Life Indonesia terdaftar dan diawasi oleh Otoritas Jasa Keuangan (OJK)
              Capital Life berkomitmen memberikan pelayanan cepat dan terbaik, mendahulukan kepentingan pemegang polis serta pihak yang berhak memperoleh manfaat. Hal ini sesuai dengan visi dan misi perusahaan. 
              Kinerja Capital Life tahun 2015, memberikan efek positif bagi perusahaan yang pada akhirnya Capital Life mendapatkan beberapa penghargaan. Penghargaan yang didapat antara lain “The Best Insurance 2016” dari Majalah Media Asuransi and predikat “Sangat Bagus” atas kinerja keuangan perusahaan asuransi selama tahun 2015 dari Majalah Infobank.",
            'title' => "Selamat Datang Toko Pandai Club",
            'image_src' => base_url()."images/promo_nov.jpg",
            'googlebot-news' => 'index,follow',
        );
        
        /* create facebook meta tags */
        $metafb = array(
            'title' => $a['html']['title'],
            'type' => 'article',
            'url' => current_url(),
            'image' => base_url()."images/promo_nov.jpg",
            'description' => html_entity_decode($metaname['description']),
            'site_name' => 'tokopandai.club'
        );


        /* create twitter card tags */
        $metatwit = array(
            'twitter:card' => 'summary_large_image',
            'twitter:site' => '@Capitallife',
            'twitter:url' => current_url(),
            'twitter:domain' => 'tokopandai.club',
            'twitter:title' => $metaname['title'],
            'twitter:description' => $metaname['description'],
            'twitter:image:src' => base_url()."images/promo_nov.jpg",
            
        );

        $a['html']['metaname'] = metaname($metaname);
        $a['html']['metaname'] .= metafb($metafb);
        $a['html']['metaname'] .= metaname($metatwit);
        if ($this->session->userdata('policy_no') !=""){
            $id = $this->session->userdata('policy_no');
            $a['cos'] = $this->m_cstmr->get_customers($id);}
        $a['template']['navigation'] = $this->load->view('template/vmenu', NULL, true);
        $s= $this->m_cstmr->get_today_visitor();
        $d['visitor'] =$s[0]['visitor'];
         $r= $this->m_cstmr->get_month_visitor();
        $d['visitorm'] =$r[0]['visitor'];
        $a['template']['footer'] = $this->load->view('template/vfooter', $d, true);
       
//        mysql_query("UPDATE page_visitor SET hit = hit + 1 where id_page_visitor =4");
//        $e = array(
//            'hit'=>+1,
//          );
//          $where = array(
//              'id_page_visitor' => '4',
//          );
//          $this->db->update('page_visitor', $e, $where);
       $this->m_cstmr->hit_claim();
        $this->load->view('claim', $a);
    }
}