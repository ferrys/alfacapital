<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rekonsiliasi extends CI_Controller {

    function __construct() {
        parent:: __construct();
        $this->load->model('m_cstmr');
    }

    function index() {
        $key = md5(sha1(md5("valdo-sms-bls-key")));
        
       /*url--http://tokopandai.club/rekonsiliasi?key=fa1b08166895825c39feec06a92bfca3--*/
       if($_GET['key'] != $key)
       {
           redirect(base_url());
       }
        $date = date('dmy', strtotime(' -1 day'));
        if (file_exists(FCPATH . "recon/archive/icapital_" . $date . ".txt")) {redirect("");}
        else{if (file_exists(FCPATH . "recon/data/icapital_" . $date . ".txt")) {
            $data = file_get_contents(FCPATH . "recon/data/icapital_" . $date . ".txt", true);
            $l = preg_split("/\\r\\n|\\r|\\n/", $data);

            $t['li'] = array_filter($l);
            if (count($t['li']) == 0) {
                redirect("http://localhost");
            }

            for ($x = 0; $x < count($t['li']); $x++) {
                $dat = explode(',', $t['li'][$x]);
                $ids = $dat[0];
                $kode_toko = $dat[5];
                $nama_toko = $dat[6];
                $company_id = $dat[7];
                $premi = $dat[4];
                $fee = $dat[3];
                //echo $premi;
                $e = array(
                    'data_exists' => '1',
                    'payment_status' => '1',
                    'kode_toko' => $kode_toko,
                    'nama_toko' => $nama_toko,
                    'company_id ' => $company_id
                );
                $where = array(
                    'ktp_no' => $ids,
                );
                $this->db->update('customers', $e, $where);
            }

            if ($t['li'][0] == NULL) {
                $files = glob(FCPATH . "recon/data/icapital_" . $date . ".txt");
                foreach ($files as $file) {
                    if (is_file($file))
                        unlink($file);
                }
            }

            preg_match_all('/\d{16}/', implode(", ", (array) $t['li']), $res);
            $params = (implode(",", (array) $res[0]));
            $t['lis'] = $this->m_cstmr->get_cron_mail_sms($params);

            $a['data'] = $this->load->view('print/data', $t, true);
            $this->load->view('print/excel', $a, FALSE);
            $this->sendmailtocapital();
            $oldDir = FCPATH . 'recon/data/';
            $newDir = FCPATH . 'recon/archive/';
            $file = "icapital_" . $date . ".txt";
            rename($oldDir . $file, $newDir . $file);
        }
        }
    }

    function sendmailtocapital() {
        $config['protocol'] = "smtp";
        $config['smtp_host'] = 'mail.valdoinfo.com';
        $config['smtp_port'] = '587';
        $config['smtp_user'] = 'tokopandai.club@valdoinfo.com';
        $config['smtp_pass'] = 'Password01';
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;

        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from('tokopandai.club@valdoinfo.com', 'ALFAMART-VALDO Reconcile');
        $this->email->to(array('mktgroup.support@capitallife.co.id'));
        $this->email->cc(array('Danus.Saputra@valdo-intl.com','ferrysukarto@gmail.com'));
        //'mktgroup.support@capitallife.co.id','Danus.Saputra@valdo-intl.com',
        $this->email->subject('VALDO Reconcile');
        $this->email->message('
            Dear Capital Life, <br><br><br>
            Berikut kami lampirkan hasil reconcile data dari Alfamart untuk tanggal <br>'.date('d-m-Y').'
            <br><br><br>
            Regards,<br><br><br>
            Valdo inc.');
        $dir = FCPATH . "recon/xls_to_capital";
        chdir($dir);
        $date = date("d-m-Y");
        array_multisort(array_map("2017-06-06", ($files = glob("*_" . $date . ".*"))), SORT_DESC, $files);
        $batch = count($files);
        $this->email->attach(FCPATH . "recon/xls_to_capital/batch" . $batch . "_" . $date . ".xls");
        $this->email->set_mailtype("html");
        $this->email->send();
    }
   
//
//    private function update_expired() {
//        $this->m_cstmr->update_expired_payment();
//    }
    
    function expired_new_web_member()
    {
        $key = md5(sha1(md5("expired")));
        
        /*url--http://tokopandai.club/expired-new-member?key=e2aa24fd9ea290ab055973ccd4421c59--*/
        if($_GET['key'] != $key)

        {
            redirect(base_url());
        }
        $ex = $this->m_cstmr->get_expired_payment_for_new_cus();
        $total = count($ex);
        for ($i = 0; $i < $total; $i++) 
        {
            
            $config['protocol'] = "smtp";
            $config['smtp_host'] = 'mail.valdoinfo.com';
            $config['smtp_port'] = '587';
            $config['smtp_user'] = 'tokopandai.club@valdoinfo.com';
            $config['smtp_pass'] = 'Password01';
            $config['mailtype'] = 'text';
            $config['validate'] = TRUE;
            $config['smtp_timeout'] = 5;
            $config['charset'] = 'utf-8';
            $config['newline'] = "\r\n";
            $config['wordwrap'] = TRUE;

            $this->load->library('email');

            $this->email->initialize($config);
            $this->email->from('tokopandai.club@valdoinfo.com', 'care@tokopandai.club');
            $this->email->to($ex[$i]['email']);
            $this->email->subject('Pemberitahuan batas waktu pembayaran Asuransi Capital Life');
            $this->email->message('<div align="center"><b>SELAMAT DATANG DI TOKO PANDAI CLUB</b></div><br>
            Waktu pembayaran Asuransi Capital Life hanya tinggal 1 minggu, silahkan membayar di Alfamart terdekat 
            <br>Apabila Anda memiliki pertanyaan atau memerlukan tambahan informasi, dapat menghubungi :
            <div align="center"><br><b>Customer Care</b>
            <br>Telp      : (62) (21) 290 23 688
            <br>WA        : (62) (89) 919 19 898
            <br>Fax       : (62) (21) 290 23 338
            <br>E-mail    : care@tokopandai.club</div>
            <br><br>Kami akan selalu mendengar dan memberikan pelayanan terbaik untuk Anda dan Keluarga.
            <br>Hormat kami,
            <br><br><br><br>Protected by PT CAPITAL LIFE INDONESIA');
            $this->email->set_mailtype("html");
            $this->email->set_crlf("\r\n");
            $this->email->send();
        }
    }
    
    public function expired_payment_member()
    {
        $key = md5(sha1(md5("expired")));
        /*url--http://tokopandai.club/expired_payment_member?key=e2aa24fd9ea290ab055973ccd4421c59--*/
        if($_GET['key'] != $key)

        {
            redirect(base_url());
        }
        $ex = $this->m_cstmr->get_expired_payment();
        
        
        $total = count($ex);
        for ($i = 0; $i < $total; $i++) 
        {
            
            $config['protocol'] = "smtp";
            $config['smtp_host'] = 'mail.valdoinfo.com';
            $config['smtp_port'] = '587';
            $config['smtp_user'] = 'tokopandai.club@valdoinfo.com';
            $config['smtp_pass'] = 'Password01';
            $config['mailtype'] = 'text';
            $config['validate'] = TRUE;
            $config['smtp_timeout'] = 5;
            $config['charset'] = 'utf-8';
            $config['newline'] = "\r\n";
            $config['wordwrap'] = TRUE;

            $this->load->library('email');

            $this->email->initialize($config);
            $this->email->from('tokopandai.club@valdoinfo.com', 'care@tokopandai.club');
            $this->email->to($ex[$i]['email']);
            $this->email->subject('Pemberitahuan Berakhirnya Masa Aktif Polis');
            $this->email->message('<div align="center"><b>Kepada Pelanggan Capital Eka Proteksi </b></div><br>
            masa aktif polis asuransi anda tinggal 30 hari lagi.<br>
            setelah itu anda bisa memperpanjang polis anda dengan membayar di Alfamart Terdekat
            <div align="center"><br><b>Customer Care</b>
            <br>Telp      : (62) (21) 290 23 688
            <br>WA        : (62) (89) 919 19 898
            <br>Fax       : (62) (21) 290 23 338
            <br>E-mail    : care@tokopandai.club</div>
            <br><br>Kami akan selalu mendengar dan memberikan pelayanan terbaik untuk Anda dan Keluarga.
            <br>Hormat kami,
            <br><br><br><br>Protected by PT CAPITAL LIFE INDONESIA');
            $this->email->set_mailtype("html");
            $this->email->set_crlf("\r\n");
            $this->email->send();
            
            $message = "Pelanggan Capital Eka Proteksi, masa aktif polis anda akan segera berakhir, segera perpanjang di alfamart terdekat dengan membayar premi";
            $c = 'http://192.168.252.1/sendsms?username=sms123&password=sms123&phonenumber='.$ex[$i]['phone_no'].'&message='.$message.'&port=gsm-1.1&report=JSON&timeout=20';
            $data = json_decode(file_get_contents(str_replace(" ", "%20", $c)),true);
            $report=$data['report'];
            $e = array(
                'ktp_no' => $ex[$i]['ktp_no'],
                'sms_status' => $report[0][1][0]['result'],
                'no_hp' =>  $report[0][1][0]['phonenumber'],
                'message' => $message
            );
            $this->db->insert('customers_sms_report', $e);
        }
    }
    
//    function move_expired_data_from_web()
//    {
//        $key = md5(sha1(md5("move-data-expired")));
//        /*url--http://tokopandai.club/move?key=7f165096daa496f3334a2476df85069b--*/
//        if($_GET['key'] != $key)
//        {
//            redirect(base_url());
//        }
//        $move = $this->m_cstmr->get_expired_payment_move_for_new_cus();
//        $total = count($move);
//        
//        for ($i = 0; $i < $total; $i++) 
//        {
//           $e = array(
//                'ktp_no' =>  $move[$i]['ktp_no'],
//                'dob' =>  $move[$i]['dob'],
//                'cname' =>  $move[$i]['cname'],
//                'age' =>  $move[$i]['age'],
//                'gender' =>  $move[$i]['gender'],
//                'phone_no' =>  $move[$i]['phone_no'],
//                'email' =>  $move[$i]['email'],
//                'address' =>  $move[$i]['address'],
//                'policy_no' =>  $move[$i]['policy_no'],
//                'tanggal_daftar' =>  $move[$i]['created'],
//            );
//            $this->db->insert('customers_expired', $e);
//        }
//        $this->m_cstmr->delete_expired_payment_move_for_new_cus();
//    }
}