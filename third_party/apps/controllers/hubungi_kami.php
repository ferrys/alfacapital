<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hubungi_kami extends CI_Controller {

    function __construct() {
        parent:: __construct();
        $this->load->model('m_cstmr');
        $this->load->library(array('form_validation', 'curl', 'email'));
    }

    function index() {
        $ket = "Mengunjungi Halaman http://tokopandai.club/contact-us";
        $this->m_cstmr->user_visitor($ket);
        if ($this->session->userdata('policy_no') != "") {
            $id = $this->session->userdata('policy_no');
            $a['cos'] = $this->m_cstmr->get_customers($id);
        }
        $a['combine']['css'] = $this->load->view('template/combinecss', NULL, true);
        $a['combine']['js'] = $this->load->view('template/combinejs', NULL, true);



        $a['template']['navigation'] = $this->load->view('template/vmenu', NULL, true);
        $s = $this->m_cstmr->get_today_visitor();
        $d['visitor'] = $s[0]['visitor'];
        $r = $this->m_cstmr->get_month_visitor();
        $d['visitorm'] = $r[0]['visitor'];
        $a['template']['footer'] = $this->load->view('template/vfooter', $d, true);

        $a['html']['title'] = "Toko Pandai Club :: Hubungi Kami";
        $metaname = array(
            'description' => "PT Capital Life Indonesia adalah bagian dari Capital Financial Group dan merupakan sister company PT Bank Capital Indonesia Tbk. Grup perusahaan nasional ini berfokus pada industri jasa keuangan yang terintegrasi. PT Capital Life Indonesia terdaftar dan diawasi oleh Otoritas Jasa Keuangan (OJK)
                      Capital Life berkomitmen memberikan pelayanan cepat dan terbaik, mendahulukan kepentingan pemegang polis serta pihak yang berhak memperoleh manfaat. Hal ini sesuai dengan visi dan misi perusahaan. 
                      Kinerja Capital Life tahun 2015, memberikan efek positif bagi perusahaan yang pada akhirnya Capital Life mendapatkan beberapa penghargaan. Penghargaan yang didapat antara lain “The Best Insurance 2016” dari Majalah Media Asuransi and predikat “Sangat Bagus” atas kinerja keuangan perusahaan asuransi selama tahun 2015 dari Majalah Infobank.",
            'title' => "Selamat Datang Toko Pandai Club",
            'image_src' => base_url() . "images/promo_nov.jpg",
            'googlebot-news' => 'index,follow',
        );

        /* create facebook meta tags */
        $metafb = array(
            'title' => $a['html']['title'],
            'type' => 'article',
            'url' => current_url(),
            'image' => base_url() . "images/promo_nov.jpg",
            'description' => html_entity_decode($metaname['description']),
            'site_name' => 'tokopandai.club'
        );


        /* create twitter card tags */
        $metatwit = array(
            'twitter:card' => 'summary_large_image',
            'twitter:site' => '@Capitallife',
            'twitter:url' => current_url(),
            'twitter:domain' => 'tokopandai.club',
            'twitter:title' => $metaname['title'],
            'twitter:description' => $metaname['description'],
            'twitter:image:src' => base_url() . "images/promo_nov.jpg",
        );

        $a['html']['metaname'] = metaname($metaname);
        $a['html']['metaname'] .= metafb($metafb);
        $a['html']['metaname'] .= metaname($metatwit);

        $this->m_cstmr->hit_contact();
        $this->load->view('hubungi_kami', $a, FALSE);
    }

    public function hub_kami() {
        $config['protocol'] = "smtp";
        $config['smtp_host'] = 'mail.valdoinfo.com';
        $config['smtp_port'] = '587';
        $config['smtp_user'] = 'capitallife@valdoinfo.com';
        $config['smtp_pass'] = 'Password01';

        $config['mailtype'] = 'text';
        $config['validate'] = TRUE;
        $config['smtp_timeout'] = 5;
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;

        $this->load->library('email');
        //$email = $this->input->post("email");
        $subject = $this->input->post("subject");
        $message = $this->input->post("message");
        $email = $this->input->post("email");

        $this->email->initialize($config);
        $this->email->from('capitallife@valdoinfo.com', 'Capital Life');
        $this->email->to("sdkdevelopers@gmail.com");
        $this->email->subject($subject);

        $this->email->message('Send From : ' . "'$email'" . "<br>" . $message);
        $this->email->set_mailtype("html");
        $this->email->set_crlf("\r\n");
        $this->email->send();

        redirect(base_url() . "contact-us");
    }

}
