<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile_member extends CI_Controller {

    function __construct() {
        parent:: __construct();
        $this->load->model('m_cstmr');   
    }
    
    function index() {
        $this->session->sess_destroy();
        $a = array();
        //        $a['html']['css'] = add_css('css/animate.css');
        //        $a['html']['css'] .= add_css('css/icomoon.css');
        //        $a['html']['css'] .= add_css('css/themify-icons.css');
        //        $a['html']['css'] .= add_css('css/bootstrap.css');
        //        $a['html']['css'] .= add_css('css/bootstrap-datepicker.css');
        //        $a['html']['css'] .= add_css('css/magnific-popup.css');
        //        $a['html']['css'] .= add_css('css/owl.carousel.min.css');
        //        $a['html']['css'] .= add_css('css/owl.theme.default.min.css');
        //        $a['html']['css'] .= add_css('css/style.css');
        
        //$a['html']['js'] = add_js('js/modernizr-2.6.2.min.js');
        
        //        $a['html']['js'] = add_js('js/jquery.min.js');
        //        $a['html']['js'] .= add_js('js/jquery.easing.1.3.js');
        //        $a['html']['js'] .= add_js('js/bootstrap.min.js');
        //        $a['html']['js'] .= add_js('js/bootstrap-datepicker.js');
        //        $a['html']['js'] .= add_js('js/jquery.waypoints.min.js');
        //        $a['html']['js'] .= add_js('js/owl.carousel.min.js');
        //        $a['html']['js'] .= add_js('js/jquery.countTo.js');
        //        $a['html']['js'] .= add_js('js/jquery.magnific-popup.min.js');
        //        $a['html']['js'] .= add_js('js/magnific-popup-options.js');
        //        $a['html']['js'] .= add_js('js/main.js');
        $a['template']['navigation'] = $this->load->view('template/vmenu', NULL, true);
        $a['combine']['css'] = $this->load->view('template/combinecss', NULL, true);
        $a['combine']['js'] = $this->load->view('template/combinejs', NULL, true);
        $a['title'] = 'Selamat Datang di Capital Life ND';
        
        
        //$idx = $this->uri->segment(3);
        if($this->uri->segment(2)=="")
        {
            redirect("");
        }
        else if($this->uri->segment(3) == "")
        {
            $id =$this->uri->segment(2);
        }
        
        else 
        {
            $id =$this->uri->segment(3);
        }
        
        //var_dump($id);
        $a['cos'] = $this->m_cstmr->get_customers_url($id);
        if ($a['cos'][0]['policy_no'] != $id)
        {
            redirect("");
        }
        if(strlen($id) != 10)
             {
            redirect("");
        }
        
        
//        var_dump($_GET['t']);
//        var_dump(substr(md5(sha1($a['cos'][0]['policy_no'])),0,5) );
//        exit();
        if($_GET['t']==""){
            redirect("");
        }
        else if(substr(md5(sha1($a['cos'][0]['policy_no'])),0,5) != $_GET['t'])
        {redirect("");}
        
        $a['template']['navigation'] = $this->load->view('template/vmenu', $a, true);
        $s= $this->m_cstmr->get_today_visitor();
        $d['visitor'] =$s[0]['visitor'];
         $r= $this->m_cstmr->get_month_visitor();
        $d['visitorm'] =$r[0]['visitor'];
        $a['template']['footer'] = $this->load->view('template/vfooter', $d, true);
        
        $this->load->view('profile', $a, FALSE);
    }
    
     function is_logged_in() {
        $is_logged_in = $this->session->userdata('policy_no');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            redirect(base_url());
        }
    }
    function logout() {
       //$this->log_out_log();
        $this->session->sess_destroy();
        header("Expires: " . gmdate("D, d M Y H:i:s", time()) . " GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        header("Pragma: no-cache");
        redirect(current_url());
    }
}