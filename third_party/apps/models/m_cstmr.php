<?php

class m_cstmr extends CI_Model {

    function input_c($cname, $dob, $age, $phone_no, $email, $address, $gender, $ktp_no,$start,$deadline) {
        $time = date("Y-m-d h:i:s");
        $id = $this->auto_id();
        $juma = count($id);
        for ($i = 0; $i < $juma; $i++) {
            $x = $id[$i]['policy_no']+1;
             $data = sprintf('%010d', $x);
        }
        if($juma=="")
        {
            $data ='0000000001';
        }
        
       $sql = "INSERT INTO customers(cname,dob,age,phone_no,email,address,created,gender,ktp_no,policy_no,start_date,deadline_time,expired)VALUES('$cname','$dob','$age','$phone_no','$email','$address','$time','$gender','$ktp_no','$data','$start','$deadline','$deadline')";
       
       $this->db->query($sql);
       return TRUE;
    }

    function update_c($cname, $phone_no, $email, $address, $id) {
        $sql = "UPDATE customers SET cname='" . $cname . "', phone_no ='" . $phone_no . "',email='" . $email . "',address='" . $address . "', update_send_status = 1  WHERE policy_no = " . $id;
        
        $this->db->query($sql);
        return TRUE;
    }
    
    function get_customers_url($id) {
        
        $sql = "SELECT * FROM customers WHERE payment_status=1 AND policy_no =".$id;
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
     function get_customers_all() {
        
        $sql = "SELECT * FROM customers WHERE payment_status=1";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function get_customers() {
        
        $id = $this->session->userdata('policy_no');
        $sql = "SELECT * FROM customers WHERE policy_no =".$id;
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function auto_id() {

        $sql = "SELECT policy_no FROM customers WHERE policy_no=(SELECT max(policy_no) FROM customers)";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }
    
    function auto_ids() {

        $sql = "SELECT policy_no FROM customers_tes WHERE policy_no=(SELECT max(policy_no) FROM customers_tes)";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }
    
    function user($policy_no,$dob) {
        $sql = "SELECT ktp_no,cname,dob,age,address,email,gender,phone_no,policy_no FROM customers WHERE policy_no ='$policy_no' AND DATE_FORMAT(dob,'%d%m%y') ='$dob' AND payment_status=1";
        $query = $this->db->query($sql);
        
        $result = $query->result_array();
        return $result;
    }
    
    function validate($policy_no,$dob) {
        $hasil = $this->user($policy_no,$dob);
        $total = count($hasil);
        for ($i = 0; $i < $total; $i++) {
            if ($total > 0) {
                $data = array(
                   
                    'ktp_no' => $hasil[$i]['ktp_no'],
                    'cname' => $hasil[$i]['cname'],
                    'dob' => date('dmy', strtotime($hasil[$i]['dob'])),
                    'age' => $hasil[$i]['age'],
                    'address' => $hasil[$i]['address'],
                    'email' => $hasil[$i]['email'],
                    'gender' => $hasil[$i]['gender'],
                    'phone_no' => $hasil[$i]['phone_no'],
                    'policy_no' => $hasil[$i]['policy_no'],
                    'is_logged_in' => TRUE
                );
                 $this->session->set_userdata($data);
                return true;
            }
        }
    }
    function check_ktp_availablity()  {
        $ktp_no = $this->input->post('ktp_no');
        $query = $this->db->query('SELECT ktp_no FROM customers WHERE ktp_no = "'.$ktp_no.'"');      
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    public function check_ktp_exist($nama)
    {
        $this->db->where("ktp_no",$nama);
        $query=$this->db->get("customers");
        if($query->num_rows()>0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function rekon_customers() {
        $sql = "SELECT ktp_no,phone_no FROM customers";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function sms_customers($id) {
        $sql = "SELECT phone_no FROM customers WHERE ktp_no IN($id)";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function get_expired_payment_for_new_cus() {
        $sql = 'SELECT * FROM customers WHERE DATE_FORMAT((created + INTERVAL 22 DAY), "%Y%m%d") = DATE_FORMAT(CURDATE(), "%Y%m%d") AND payment_status = 0';
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
//    function update_expired_payment() {
//        $sql = "UPDATE customers SET payment_status = 3 WHERE  DATEDIFF(NOW(), deadline_time) >= 0 AND (payment_status=1)";
//        //$this->update_expired_customers();
//        $query = $this->db->query($sql);
//        $data = array();
//        if ($query !== FALSE && $query->num_rows() > 0) {
//            $data = $query->result_array();
//        }
//        return $data;
//    }
    
    function get_expired_payment() {
        $sql ="SELECT email,phone_no,ktp_no,NOW(), deadline_time, DATEDIFF(NOW(), deadline_time) FROM customers WHERE  DATEDIFF(NOW(), deadline_time) = -30 AND(payment_status=1) ";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
//    function get_expired_payment_move_for_new_cus() {
//        $sql ="SELECT cname,dob,age,gender,phone_no,address,email,ktp_no, NOW(),created,DATEDIFF(NOW(), created) FROM customers WHERE DATEDIFF(NOW(),created)>30 AND(payment_status=0) ";
//        $query = $this->db->query($sql);
//        $data = array();
//        if ($query !== FALSE && $query->num_rows() > 0) {
//            $data = $query->result_array();
//        }
//        return $data;
//    }
//    
//    function delete_expired_payment_move_for_new_cus() {
//        $sql ="DELETE FROM customers WHERE DATEDIFF(NOW(),created)>30 AND(payment_status=0)";
//        $query = $this->db->query($sql);
//        $data = array();
//        if ($query !== FALSE && $query->num_rows() > 0) {
//            $data = $query->result_array();
//        }
//        return $data;
//    }
    
    function anu($id) {
        $sql = "UPDATE customers SET midi = '1' WHERE ktp_no IN($id)";
        //$this->update_expired_customers();
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function update_customers($id) {
        $sql = "UPDATE customers SET data_exists = 1 WHERE ktp_no IN($id)";
        //$this->update_expired_customers();
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function get_cron_mail_sms($ktp_no) {
        $sql = "SELECT * FROM customers WHERE ktp_no IN ($ktp_no) AND agent_id='Alfamart'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function get_cron_mail_sms_alfamidi($ktp_no) {
        $sql = "SELECT * FROM customers WHERE payment_status='1' AND data_exists='1' AND midi='0'  AND agent_id='Alfamidi'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function get_rekon_monthly($ktp_no) {
        $sql = "SELECT * FROM customers WHERE payment_status ='1' AND (DATE_FORMAT( created,'%m') = DATE_FORMAT( NOW( ) ,  '%m' )-1)";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function get_sms_monthly($ktp_no) {
        $sql = "SELECT * FROM customers_sms_report WHERE (DATE_FORMAT( time,'%m') = DATE_FORMAT(NOW(),'%m' )-1)";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function get_sms_blast_karyawan_capital() {
        $sql = "SELECT * FROM karyawan_capital WHERE sms_status='success'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
     function get_karyawan_valdo() {
        $sql = "SELECT * FROM karyawan_capital";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function get_blast_sms() {
        $sql ="SELECT no_hp, message FROM customers_sms_report WHERE sms_status='fail'";
       //sql = 'SELECT no_hp FROM karyawan_capital WHERE sms_status = "fail"';
       //$sql ='SELECT * FROM karyawan_valdo WHERE sms_status = "fail" AND LENGTH( no_hp ) > 10 AND LENGTH( no_hp )';
       //$sql ='SELECT no_hp FROM karyawan_valdo WHERE sms_status != "success" AND LENGTH( no_hp ) > 15 AND LENGTH( no_hp ) < 40 limit 500';
       // $sql ='SELECT * FROM karyawan_valdo WHERE sms_status != "success" AND LENGTH( no_hp ) > 9 AND LENGTH( no_hp ) < 15 AND  (SUBSTRING(no_hp,1,4)=0814 OR SUBSTRING(no_hp,1,4)=0815 OR SUBSTRING(no_hp,1,4)=0816 OR SUBSTRING(no_hp,1,4)=0855 OR SUBSTRING(no_hp,1,4)=0856 OR SUBSTRING(no_hp,1,4)=0857 OR SUBSTRING(no_hp,1,4)=0858) limit 500';
       // $sql ='SELECT * FROM karyawan_valdo WHERE sms_status != "success" AND LENGTH( no_hp ) > 10 AND LENGTH( no_hp ) < 15 AND  (SUBSTRING(no_hp,1,4)=0817 OR SUBSTRING(no_hp,1,4)=0818 OR SUBSTRING(no_hp,1,4)=0819 OR SUBSTRING(no_hp,1,4)=0877 OR SUBSTRING(no_hp,1,4)=0878) limit 100';
       //
       // $sql = 'SELECT * FROM karyawan_valdo WHERE sms_status != "success" AND LENGTH( no_hp ) > 10 AND LENGTH( no_hp ) < 15 AND  (SUBSTRING(no_hp,1,4)=0813 OR SUBSTRING(no_hp,1,4)=0812 OR SUBSTRING(no_hp,1,4)=0811 OR SUBSTRING(no_hp,1,4)=0821 OR SUBSTRING(no_hp,1,4)=0822 OR SUBSTRING(no_hp,1,4)=0823 OR SUBSTRING(no_hp,1,4)=0852 OR SUBSTRING(no_hp,1,4)=0853) limit 500';
       
       //       $sql ="SELECT DISTINCT(no_hp)FROM karyawan_valdo
       //                WHERE sms_status !=  "success"
       //                AND LENGTH( no_hp ) >15
       //                AND LENGTH( no_hp ) <45'; 
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
//    function update_blast_sms($id) {
//        //$sql = "UPDATE karyawan_valdo SET(sms_status=1)WHERE no_hp IN($id)";
//        $sql = "UPDATE blast SET sms_status=1 WHERE no_hp IN($id)";
//        //$sql = "SELECT * FROM  karyawan_valdo WHERE LENGTH( no_hp ) >10 AND LENGTH( no_hp ) <15";
//        $query = $this->db->query($sql);
//        $data = array();
//        if ($query !== FALSE && $query->num_rows() > 0) {
//            $data = $query->result_array();
//        }
//        return $data;
//    }
    
    function get_cron_mail_sms_update() {
        $sql = "SELECT email, phone_no FROM customers WHERE update_send_status = 1";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    
//    function update_expired_customers() {
//        $sql = "UPDATE customers SET payment_status = 3 WHERE deadline_time < CURDATE()";
//        $query = $this->db->query($sql);
//        $data = array();
//        if ($query !== FALSE && $query->num_rows() > 0) {
//            $data = $query->result_array();
//        }
//        return $data;
//    }
    function log_login($policy,$berhasil) {
        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
        $json = json_decode($location, true);
       
        $this->load->library('user_agent');
        if ($this->agent->is_browser())
        {
            $agent = $this->agent->browser().' '.$this->agent->version();
        }
        elseif ($this->agent->is_robot())
        {
            $agent = $this->agent->robot();
        }
        elseif ($this->agent->is_mobile())
        {
            $agent = $this->agent->mobile();
        }
        else
        {
            $agent = 'Unidentified User Agent';
        }
        $platform= $this->agent->platform();
        $sql = "INSERT INTO customers_log(keterangan,no_peserta,ip_address,browser,flatform,status,city,isp) VALUES ('LOGIN','$policy','" . $this->input->ip_address() . "','$agent','$platform.','$berhasil','". $json['region_name']."','$hostname')";
        $this->db->query($sql);
        return TRUE;
    }
    
    function log_out() {
        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
        $json = json_decode($location, true);
       
        $this->load->library('user_agent');
        if ($this->agent->is_browser())
        {
            $agent = $this->agent->browser().' '.$this->agent->version();
        }
        elseif ($this->agent->is_robot())
        {
            $agent = $this->agent->robot();
        }
        elseif ($this->agent->is_mobile())
        {
            $agent = $this->agent->mobile();
        }
        else
        {
            $agent = 'Unidentified User Agent';
        }
        $platform= $this->agent->platform();
        $sql = "INSERT INTO customers_log(keterangan,no_peserta,ip_address,browser,flatform,status,city,isp) VALUES ('LOGOUT','".$this->session->userdata('policy_no')."','" . $this->input->ip_address() . "','$agent','$platform.','SUCCESS','". $json['region_name']."','$hostname')";
        $this->db->query($sql);
        return TRUE;
    }
    
     function user_visitor($ket) {
        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
        $json = json_decode($location, true);
       
        $this->load->library('user_agent');
        if ($this->agent->is_browser())
        {
            $agent = $this->agent->browser().' '.$this->agent->version();
        }
        elseif ($this->agent->is_robot())
        {
            $agent = $this->agent->robot();
        }
        elseif ($this->agent->is_mobile())
        {
            $agent = $this->agent->mobile();
        }
        else
        {
            $agent = 'Unidentified User Agent';
        }
        $platform= $this->agent->platform();
        $sql = "INSERT INTO user_log(keterangan,ip_address,browser,flatform,city,isp) VALUES ('$ket','" . $this->input->ip_address() . "','$agent','$platform.','". $json['region_name']."','$hostname')";
        $this->db->query($sql);
        return TRUE;
    }
    
    function log_update_data() {
        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
        $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
        $json = json_decode($location, true);
        $this->load->library('user_agent');
        if ($this->agent->is_browser())
        {
            $agent = $this->agent->browser().' '.$this->agent->version();
        }
        elseif ($this->agent->is_robot())
        {
            $agent = $this->agent->robot();
        }
        elseif ($this->agent->is_mobile())
        {
            $agent = $this->agent->mobile();
        }
        else
        {
            $agent = 'Unidentified User Agent';
        }
        $platform= $this->agent->platform();
        $sql = "INSERT INTO customers_log(keterangan,no_peserta,ip_address,browser,flatform,status,city,isp) VALUES ('UPDATE DATA','".$this->session->userdata('policy_no')."','" . $this->input->ip_address() . "','$agent','$platform.','SUCCESS','". $json['region_name']."','$hostname')";
        $this->db->query($sql);
        return TRUE;
        
    }
    
    function hit_claim() {
        $sql = "UPDATE capital_page_visitor SET hit = hit + 1 where id_page_visitor =4";
        $this->db->query($sql);
        return TRUE;
    }
    function hit_home() {
        $sql = "UPDATE capital_page_visitor SET hit = hit + 1 where id_page_visitor =1";
        $this->db->query($sql);
        return TRUE;
    }
    function hit_about() {
        $sql = "UPDATE capital_page_visitor SET hit = hit + 1 where id_page_visitor =2";
        $this->db->query($sql);
        return TRUE;
    }
    function hit_produk() {
        $sql = "UPDATE capital_page_visitor SET hit = hit + 1 where id_page_visitor =3";
        $this->db->query($sql);
        return TRUE;
    }
    function hit_contact() {
        $sql = "UPDATE capital_page_visitor SET hit = hit + 1 where id_page_visitor =5";
        $this->db->query($sql);
        return TRUE;
    }
    function hit_register() {
        $sql = "UPDATE capital_page_visitor SET hit = hit + 1 where id_page_visitor =6";
        $this->db->query($sql);
        return TRUE;
    }
    function hit_faq() {
        $sql = "UPDATE capital_page_visitor SET hit = hit + 1 where id_page_visitor =7";
        $this->db->query($sql);
        return TRUE;
    }
    function hit_home_login() {
        $sql = "UPDATE capital_page_visitor SET hit = hit + 1 where id_page_visitor = 8";
        $this->db->query($sql);
        return TRUE;
    }
    function hit_inf_keu() {
        $sql = "UPDATE capital_page_visitor SET hit = hit + 1 where id_page_visitor =9";
        $this->db->query($sql);
        return TRUE;
    }
    function get_page_visitor() {
        $sql = "SELECT * FROM capital_page_visitor";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function send_to_halodoc(){
        $sql = "SELECT * FROM customers WHERE payment_status='1' and send_to_halodoc ='0'  AND phone_no !=''";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    function get_today_visitor() {
        $sql = "SELECT COUNT(*) AS visitor,time,DATE_FORMAT(NOW(), '%d/%m/%Y'),DATE_FORMAT(time, '%d/%m/%Y') FROM user_log WHERE DATE_FORMAT(time, '%d/%m/%Y')= DATE_FORMAT(NOW(), '%d/%m/%Y')";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    function get_month_visitor() {
        $sql = "SELECT COUNT(*) AS visitor FROM user_log WHERE DATE_FORMAT(time, '%m')= DATE_FORMAT(NOW(), '%m')";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }

    
    function get_member_by_ktp($no_polis) {
        $sql = "SELECT * FROM customers where ktp_no ='$no_polis'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function upload_member($ktp,$name,$tanggal_lahir,$email,$gender,$alamat,$nohp)
    {
         $time = date("Y-m-d h:i:s");
        $id = $this->auto_id();
        $juma = count($id);
        for ($i = 0; $i < $juma; $i++) {
           $x = $id[$i]['policy_no']+1;
            $data = sprintf('%010d', $x);
        }
        if($juma=="")
        {
           $data ='0000000001';
       }
        $start =date("Y-m-d");
        $expired = date("Y-m-d", mktime(0, 0, 0, date("m") + 6, date("d") - 1, date("Y")));
        
        $sql = "INSERT INTO customers(ktp_no,cname,dob,email,gender,address,phone_no,additional_customer_info,policy_no,payment_date,start_date,deadline_time,expired,payment_status,agent_id,charge) VALUES ('$ktp','$name','$tanggal_lahir','$email','$gender','$alamat','$nohp','Selamat, Anda sudah terproteksi AJ Capital Life. Lengkapi data di www.tokopandai.club 02129023688-08991919898(WA only)','$data','$start','$start','$expired','$expired','1','alfamart','promo')";
        $this->db->query($sql);
        return TRUE;
    }
}