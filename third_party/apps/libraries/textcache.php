<?php

class textcache {

    function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->driver('cache');
    }

    function grabData($name_cache, $method, $cachetime) {
        $cache = $this->CI->cache->memcached->get($name_cache);
        if (!$cache) {
            $cache = $method;
            $this->CI->cache->memcached->save($name_cache, $cache, $cachetime);
        }
        $result = $this->CI->cache->memcached->get($name_cache);

        return $result;
    }

    function setHeader() {
        $this->CI->output->set_header("HTTP/1.0 200 OK");
        $this->CI->output->set_header("HTTP/1.1 200 OK");
        $this->CI->output->set_header('Last-Modified: ' . gmdate('D, d M Y H:i:s', time()) . ' GMT');
        $this->CI->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->CI->output->set_header("Cache-Control: post-check=0, pre-check=0");
        $this->CI->output->set_header("Pragma: no-cache");
    }

}
