<? if (!defined('BASEPATH')) exit('No direct script access allowed'); 
class UserAgent {

	var $mobile_browser;
    var $wap_enabled;
    var $agent;
    var $is_mobile_browser;
    var $agent_string;
	function userAgent(){
		$this->CI =& get_instance();
        $this->CI->load->helper('url');
		$this->mobile_browser='0';
        $this->wap_browser='0';
        $this->agent    = '';
        $this->is_mobile_browser=false;
        $this->agent_string = '';
    
	}

    function detect() {
    
        $is_mobile    = $this->mobile_device_detect();
        return $is_mobile;
    
    }
    
    function is_mobile() {
        return $this->is_mobile_browser;
    }
    //function mobile_device_detect($iphone=true,$android=true,$opera=true,$blackberry=true,$palm=true,$windows=true,$mobileredirect='http://m.okezone.com',$desktopredirect='http://www.okezone.com'){
    function mobile_device_detect(){
          $mobile_browser   = false;
          $user_agent       = $_SERVER['HTTP_USER_AGENT'];
          $this->agent_string= $user_agent;
          $accept           = $_SERVER['HTTP_ACCEPT'];
          switch(true){
            
             //ipad stick to desktop
            case (preg_match('/(ipad)/i',$user_agent)); 
                  $mobile_browser = true;
                  $this->agent    = 'ipad';
                  //if(substr($desktopredirect,0,4)=='http'){ $mobileredirect = $desktopredirect; }
                break;
                            case (preg_match('/(ipod)/i',$user_agent)||preg_match('/(iphone)/i',$user_agent)); 
                  $mobile_browser = true;
                  //if(substr($iphone,0,4)=='http'){ $mobileredirect = $iphone; }
                  $this->agent    = 'iphone';
                break;
           
                        case (preg_match('/(android)/i',$user_agent));
                  $mobile_browser = true;
                  $this->agent    = 'android';
                  //if(substr($android,0,4)=='http'){ $mobileredirect = $android; } 
                break;
            
            case (preg_match('/(blackberry)/i',$user_agent));
                  $mobile_browser = true;
                  //if(substr($blackberry,0,4)=='http'){ $mobileredirect = $blackberry; } 
                  $this->agent    = 'blackberry';
                  break;
          } 
          
          $this->is_mobile_browser  = $mobile_browser;
          return $mobile_browser;
    
          //echo $mobile_browser;
          /*          //header('Cache-Control: no-transform'); 
          //header('Vary: User-Agent, Accept');
          if($redirect = ($mobile_browser==true) ? $mobileredirect : $desktopredirect){
            header('Location: '.$redirect); 
            exit;
            //return  "true";
          }else{ 
            return $mobile_browser;
            //return "false"; 
          }
        */
     }

    function get_agent() {
        return $this->agent;
    
    }
    
    function get_fullstring() {
        
        return $this->agent_string;

    }

}

?>