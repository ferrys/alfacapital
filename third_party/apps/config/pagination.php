<?php

$config['paging']['full_tag_open'] = '<ul class="pagination">';
$config['paging']['full_tag_close'] = '</ul>';

$config['paging']['first_link'] = FALSE;
$config['paging']['first_tag_open'] = '<li>';
$config['paging']['first_tag_close'] = '</li>';

$config['paging']['prev_link'] = '&lt;';
$config['paging']['prev_tag_open'] = '<li>';
$config['paging']['prev_tag_close'] = '</li>';

$config['paging']['last_link'] = FALSE;
$config['paging']['last_tag_open'] = '<li>';
$config['paging']['last_tag_close'] = '</li>';

$config['paging']['next_link'] = '&gt;';
$config['paging']['next_tag_open'] = '<li>';
$config['paging']['next_tag_close'] = '</li>';

$config['paging']['cur_tag_open'] = '<li class="active"><a href="#">';
$config['paging']['cur_tag_close'] = '</a></li>';

$config['paging']['num_tag_open'] = '<li>';
$config['paging']['num_tag_close'] = '</li>';