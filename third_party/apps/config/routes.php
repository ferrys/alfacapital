<?php
$route['default_controller'] = "welcome";
$route['product-information'] = 'informasi_product';
$route['info-keuangan'] = 'informasi_keuangan';
$route['contact-us'] = 'hubungi_kami';
$route['member-register'] = 'register';
$route['home-login'] = 'dashboard';
$route['claim-page'] = 'claim';
$route['API-DATA'] = 'welcome/hit';
$route['home'] = 'home';
$route['faq'] = 'benefit/faq';
$route['benefit'] = 'benefit';
$route['ads'] = 'ads';
$route['about-us'] = 'tentang';
$route['thankyou'] = 'terimakasih';
$route['p/(.*)'] = 'profile_member/index/$1';
$route['p'] = 'profile_member';
$route['update-data'] = 'update_cos/update_url';

$route['export-data'] = 'rekonsiliasi_bulanan/export';

$route['get-data'] = 'update_cos/alldata';

$route['404_override'] = '';
$route['move'] = 'rekonsiliasi/move_expired_data_from_web';
$route['expired-new-member'] = 'rekonsiliasi/expired_payment_member';