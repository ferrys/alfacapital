<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class InquiryStatus extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('customer_model');
    }

    public function index_get() {
        //api.capital.dev/inquirydata?AgentID=Alfamart&AgentPIN=ee8a10a9&AgenttrxID=10&AgentstoreID=K100&ProductID=paketA&CustomerID=9999928384785&DatetimeRequest=20120910181504&Signature=6fff9b4a9431bd73dfbb9816e36c5e90bd165b19
        //=IF(AND(MONTH(E5)-MONTH(D5)>=6|DAY(E5)-DAY(D5)>=0)|(YEAR(E5)-YEAR(D5))+1|IF(AND(MONTH(E5)-MONTH(D5)>6|DAY(E5)-DAY(D5)<0)|(YEAR(E5)-YEAR(D5))+1|IF(AND(MONTH(E5)-MONTH(D5)=6|DAY(E5)-DAY(D5)<0)|(YEAR(E5)-YEAR(D5))|IF(AND(MONTH(E5)-MONTH(D5)<6|MONTH(E5)-MONTH(D5)>-6)|(YEAR(E5)-YEAR(D5))|IF(AND(MONTH(E5)-MONTH(D5)=-6|DAY(E5)-DAY(D5)>=0)|(YEAR(E5)-YEAR(D5))|IF(AND(MONTH(E5)-MONTH(D5)=-6|DAY(E5)-DAY(D5)<0)|(YEAR(E5)-YEAR(D5))-1|(YEAR(E5)-YEAR(D5)-1)))))))
        
        $agent_id = $this->get('AgentID');
        $agent_pin = $this->get('AgentPIN');
        $agent_trxid = $this->get('AgentTrxID');
        $agent_storeid = $this->get('AgentStoreID');
        $product_id = $this->get('ProductID');
        $customer_id = $this->get('CustomerID');
        $datetime_request = $this->get('DateTimeRequest');
       
        $signature = $this->get('Signature') ;
        
        $secretkey = "AlfaG4t3";
        
        $expired_res = "000000";
        $policy_no = "0"; //nomer polis
        $stcode = 0;
        //error lainnya
        //validasi signature

        $stval = sha1(strtolower("$agent_id$agent_pin$agent_trxid$agent_storeid$product_id$customer_id$datetime_request").$secretkey);
//        
        var_dump($stval);
        //die($stval == $signature);
        if ($customer_id != '' && ($stval == $signature)) {
            //validasi KTP, valid KTP 16digit
            $ktp_len = strlen(trim($customer_id));
            //die($ktp_len);
            if ($ktp_len != 16) {
                $stcode = "12";
            } else {
                //die("dorr");
                $rows = $this->customer_model->get_cos_byid($customer_id);
                $row = $this->customer_model->get_customer($customer_id);
                $stcode = "";
                
                if ($row && $rows[0]['payment_status'] == 1)//Jika no ktp ada didatabase dan status==0
                    {
                    //policy sudah terdaftar dan sudah bayar
                    $stcode = 2;
                    $policy_no = $row['policy_no'];
                }
                else if($row && $rows[0]['payment_status'] == 0)
                {
                    $stcode="SEGERA LAKUKAN PEMBAYARAN";
                    //Jika no ktp ada didatabase dan dan belum bayar
                }
                else if($row && $rows[0]['payment_status'] == 2)
                {
                    $stcode="BELUM BAYAR DENGAN STATUS REVERSAL";
                    //Jika no ktp ada didatabase dan dan belum bayar
                }
                else if(count($rows) == 0)
                {
                    $stcode="NOMOR TIDAK TERDAFTAR";
                    //Jika no ktp ada didatabase dan dan belum bayar
                }
                else {
                    $dt_dob = get_dob_date($customer_id);
                    $st_gender = get_gender($customer_id);
                    //curdate - 6 bulan -> 18 s/d 64
                    $start_req = date("Ymd", mktime(0, 0, 0, date("m") - 6, date("d"), date("Y")));
                    $int_age = 0;
                    $sql = "SELECT YEAR((FROM_DAYS(TO_DAYS('$start_req') - TO_DAYS('$dt_dob')))) AS umur_req, YEAR((FROM_DAYS(TO_DAYS(curdate()) - TO_DAYS('$dt_dob')))) AS umur";

                    //die($sql);
                    $query = $this->db->query($sql);

                    $row = $query->row_array();
                    $int_age_req = $row['umur_req'];
                    $int_age = $row['umur'];

                    //screening usia 18-64
                    //die($int_age_req);
                    if ($int_age_req < 18 || $int_age_req > 64) {
                        $stcode = 1;
                    }
                    //die('d'.$stcode);
                    //registrasi polis
                    if ($customer_id != '' && $stcode == 0) {
                        //proses insert
                        $expired = date("Y-m-d", mktime(0, 0, 0, date("m") + 6, date("d"), date("Y")));
                        $expired_res = date("Ymd", mktime(0, 0, 0, date("m") + 6, date("d"), date("Y")));

                        $arrdata = array(
                            "agent_pin" => $agent_pin,
                            "agent_id" => $agent_id,
                            "agent_trxid" => $agent_trxid,
                            "product_id" => $product_id,
                            "ktp_no" => $customer_id,
                            "gender" => $st_gender,
                            "dob" => $dt_dob,
                            "payment_status" => 0,
                            "age" => $int_age,
                            "expired" => $expired,
                            "created" => atom_date()
                        );

//                        var_dump($arrdata);
//                        die();
                       // $this->db->insert("customers", $arrdata);

                        $idx = $this->db->insert_id();

                        //generate nomer peserta
                        $policy_no = number_pad($idx, 10);

                        //proses update polis no
                        $toupdate = array(
                            "policy_no" => $policy_no
                        );
                        $where = array(
                            "idx" => $idx
                        );
                        //$this->db->update("customers", $toupdate, $where);
                        $stcode = 0; //sukses
                    }
                }
            }
            //var_dump($row);
            //die();    			
        } else {
            $stcode = 30; //error lainnya
        }

        //die('sdd'.$policy_no);
        //Alfamart|ee8a10a9|10|K100|9999928384785|20120910181504|00|TransaksiSukses|20120910181505|18|BudiWahono|BEAT CW#B1111CDE#jl MH Thamrin Tangerang|20120928|500000|0|500000|paketA|1|    		
        //Agent ID|Agent PIN|Agent trx ID|Agent store ID|Customer ID|DatetimeRequest|Response Code|Response Desc|Datetime Resp|Payment Period|Customer Name|CustomerInformation|Deadline Time|Amount|Charge|Total|Product ID|QtyTrx
        //generate signature
        //
   
        $st_res = response_message($stcode);
        $st_date = date("Ymdhis");
        //die("$agent_id$agent_pin$agent_trxid$agent_storeid$customer_id$datetime_request$stcode$st_res$st_date$policy_no");
        //$stval = sha1(strtolower("$agent_id$agent_pin$agent_trxid$agent_storeid$product_id$customer_id$datetime_request").$secretkey);
       // $st_signature = sha1(strtolower("$agent_id$agent_pin$agent_trxid$agent_storeid$product_id$customer_id$datetime_request").$secretkey);
        $arr_result = array(
            $agent_id, $agent_pin, $agent_trxid, $agent_storeid, $customer_id, $datetime_request,
            $stcode, $st_date, $policy_no, $expired_res, $stval, $product_id
        );
        $message = implode("|", $arr_result);

        //$this->response('My first API response = GET METHOD');
        $this->response($message);
    }

    public function index_post() {
        $agent_id = $this->post('AgentID');
        $agent_pin = $this->post('AgentPIN');
        $agent_trxid = $this->post('AgenttrxID');
        $agent_storeid = $this->post('AgentstoreID');
        $product_id = $this->post('ProductID');
        $customer_id = $this->post('CustomerID'); //KTP NO
        $datetime_request = $this->post('DatetimeRequest');
        $signature = $this->post('Signature');
        $is_valid = 0;
        $policy_no = "";
        $stcode = 0;
        $policy_no = 0;

        $arr_result = array(
            $agent_id, $agent_pin, $agent_trxid, $agent_storeid, $customer_id, $datetime_request, $stcode,
            response_message($stcode), date("Ymdhis"), $policy_no
        );
        $message = implode("|", $arr_result);
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
        //$this->response($message);   
    }

    function index_put() {

        $ktp = $this->put('CustomerID');
        $data = array(
            //'ktp_no'=> $this->put('CustomerID'),
            'payment_status' => 222);
        $this->db->where('ktp_no', $ktp);
        $this->db->update('customers', $data);

        $message = implode("|", $data);
        $this->set_response($message);
    }

}
