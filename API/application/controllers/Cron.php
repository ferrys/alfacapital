<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller {
    function index()
    {
        $this->load->model('customer_model');
        $this->customer_model->update_payment();
    }
}