<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class InquiryData extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('customer_model');
    }

    public function index_get() {
        //api.capital.dev/inquirydata?AgentID=Alfamart&AgentPIN=ee8a10a9&AgenttrxID=10&AgentstoreID=K100&ProductID=paketA&CustomerID=9999928384785&DatetimeRequest=20120910181504&Signature=6fff9b4a9431bd73dfbb9816e36c5e90bd165b19
        //=IF(AND(MONTH(E5)-MONTH(D5)>=6|DAY(E5)-DAY(D5)>=0)|(YEAR(E5)-YEAR(D5))+1|IF(AND(MONTH(E5)-MONTH(D5)>6|DAY(E5)-DAY(D5)<0)|(YEAR(E5)-YEAR(D5))+1|IF(AND(MONTH(E5)-MONTH(D5)=6|DAY(E5)-DAY(D5)<0)|(YEAR(E5)-YEAR(D5))|IF(AND(MONTH(E5)-MONTH(D5)<6|MONTH(E5)-MONTH(D5)>-6)|(YEAR(E5)-YEAR(D5))|IF(AND(MONTH(E5)-MONTH(D5)=-6|DAY(E5)-DAY(D5)>=0)|(YEAR(E5)-YEAR(D5))|IF(AND(MONTH(E5)-MONTH(D5)=-6|DAY(E5)-DAY(D5)<0)|(YEAR(E5)-YEAR(D5))-1|(YEAR(E5)-YEAR(D5)-1)))))))
        
        $agent_id = $this->get('AgentID');
        $agent_pin = $this->get('AgentPIN');
        $agent_trxid = $this->get('AgentTrxID');
        $agent_storeid = $this->get('AgentStoreID');
        $product_id = $this->get('ProductID');
        $customer_id = $this->get('CustomerID');
        $datetime_request = $this->get('DateTimeRequest');
        $secretkey = "AlfaG4t3";
        $signature = $this->get('Signature');
        $expired_res = "000000";
        $policy_no = "0"; //nomer polis
        $stcode = "00";
        $additional_customer_info = ""; 
        $quantity_trx = "";
        $pay_period = $this->get('PaymentPeriod');
        $amount = "20000";
        $charge = $this->get('Charge');
        $total = "20000";
        //$admin_fee = $this->get('AdminFee');
        //error lainnya
        //validasi signature
        $stval = sha1("$agent_id$agent_pin$agent_trxid$agent_storeid$product_id$customer_id$datetime_request$secretkey");
        // var_dump("AgentID : ".$this->get('AgentID'),"AgentPIN : ".$this->get('AgentPIN'),"AgentTrxID : ".$this->get('AgentTrxID'),"AgentStoreID : ".$this->get('AgentStoreID'),"ProductID : ".$this->get('ProductID'),"CustomerID : ".$this->get('CustomerID'),"DateTimeRequest : ".$this->get('DateTimeRequest'),"URL GET SIGNATURE: ".$this->get('Signature'),"sha1 SIGNATURE: ".$stval);
        //var_dump($stval);
           //die($stval == $signature);
        if($customer_id == ""){
                        redirect("http://110.5.109.166:8073");}
        if ($customer_id != '' && ($stval == $signature)) {
            //validasi KTP, valid KTP 16digit
            $ktp_len = strlen(trim($customer_id));
            //die($ktp_len);
            if ($ktp_len != 16) {
                $stcode = "12";
            } 
            else {
                $rows = $this->customer_model->get_cos_byid($customer_id);
                $row = $this->customer_model->get_customer($customer_id);
                $stcode = "";
                if ($row && $rows[0]['payment_status'] == 0) 
                {
                    $stcode = "00";
                }
                else if ($row && $rows[0]['payment_status'] == 1 ) {
                    $stcode = 88;
                    $policy_no = $row['policy_no'];
                }
                
                else if ($row && $rows[0]['payment_status'] == 2 ) 
                {//Jika no ktp ada didatabase dan status==0
                    $policy_no = $row['policy_no'];
                    $dt_dob = get_dob_date($customer_id);
                    $st_gender = get_gender($customer_id);
                    //curdate - 6 bulan -> 18 s/d 64
                    $start_req = date("Ymd", mktime(0, 0, 0, date("m") - 6, date("d"), date("Y")));
                    $int_age = 0;
                    $sql = "SELECT YEAR((FROM_DAYS(TO_DAYS('$start_req') - TO_DAYS('$dt_dob')))) AS umur_req, YEAR((FROM_DAYS(TO_DAYS(curdate()) - TO_DAYS('$dt_dob')))) AS umur";
                    //die($sql);
                    $query = $this->db->query($sql);

                    $row = $query->row_array();
                    $int_age_req = $row['umur_req'];
                    $int_age = $row['umur'];

                    //screening usia 18-64
                    //die($int_age_req);
                    if ($int_age_req < 18 || $int_age_req > 64) {
                        $stcode = 1;
                    }
                    //die('d'.$stcode);
                    //registrasi polis
                    if ($customer_id != '' && $stcode == 0) {
                        //proses insert
                       $expired = date("Y-m-d", mktime(0, 0, 0, date("m") + 6, date("d") - 1, date("Y")));
                        //$expires = date($expired, mktime(0, 0, 0, date("m"), date("d") - 1, date("Y")));
                        $expired_res = date("Ymd", mktime(0, 0, 0, date("m") + 6, date("d"), date("Y")));

                        $arrdata = array(
                            "payment_status" => 0,
                        );
                        
                        $where = array(
                            "ktp_no" => $customer_id
                        );
                        
                        $this->db->update("customers", $arrdata, $where);
                        $stcode = "00"; 
                    }
                }
                else {
                    $dt_dob = get_dob_date($customer_id);
                    $st_gender = get_gender($customer_id);
                    //curdate - 6 bulan -> 18 s/d 64
                    $start_req = date("Ymd", mktime(0, 0, 0, date("m") - 6, date("d"), date("Y")));
                    $int_age = 0;
                    $sql = "SELECT YEAR((FROM_DAYS(TO_DAYS('$start_req') - TO_DAYS('$dt_dob')))) AS umur_req, YEAR((FROM_DAYS(TO_DAYS(curdate()) - TO_DAYS('$dt_dob')))) AS umur";

                    //die($sql);
                    $query = $this->db->query($sql);

                    $row = $query->row_array();
                    $int_age_req = $row['umur_req'];
                    $int_age = $row['umur'];

                    //screening usia 18-64
                    //die($int_age_req);
                    if ($int_age_req < 18 || $int_age_req > 64) {
                        $stcode = 1;
                    }

                    //die('d'.$stcode);
                    //registrasi polis
                    if ($customer_id != '' && $stcode == 0) {
                        //proses insert
                        //$expired = date("Y-m-d", mktime(0, 0, 0, date("m") + 6, date("d") - 1, date("Y")));
                        $expired = date("Y-m-d", mktime(0, 0, 0, date("m") + 6, date("d") - 1, date("Y")));
                        //$expires = date($expired, mktime(0, 0, 0, date("m"), date("d") - 1, date("Y")));
                        $expired_res = date("Ymd", mktime(0, 0, 0, date("m") + 6, date("d"), date("Y")));

                        $arrdata = array(
                            "agent_pin" => $agent_pin,
                            "agent_id" => $agent_id,
                            "agent_trxid" => $agent_trxid,
                            "product_id" => $product_id,
                            "ktp_no" => $customer_id,
                            "gender" => $st_gender,
                            "dob" => $dt_dob,
                            "start_date" => date("Y-m-d"),
                            "payment_status" => 0,
                            "deadline_time" => $expired,
                            "age" => $int_age,
                            "expired" => $expired,
                            "created" => atom_date()
                        );
                        
                        $hist = array(
                            "ktp_no" => $customer_id,
                            "keterangan" => "INQUIRY DATA SUCCESS",
                        );
                        $this->db->insert("api_activity_history", $hist);
//                        var_dump($arrdata);
//                        die();
                        $this->db->insert("customers", $arrdata);
                        
                        $idx = $this->db->insert_id();

                        //generate nomer peserta
                        $policy_no = number_pad($idx, 10);

                        //proses update polis no
                        $toupdate = array(
                            "policy_no" => $policy_no
                        );
                        $where = array(
                            "idx" => $idx
                        );
                        $this->db->update("customers", $toupdate, $where);
                        $stcode = "00"; //sukses
                    }
                }
            }  			
        } else {
            $stcode = 30;
        }
        
        $row = $this->customer_model->get_customer($customer_id);
        $rows = $this->customer_model->get_cos_byid($customer_id);
        if($row)
        {
            $x = $rows[0]['policy_no'];
            $customer_name = $rows[0]['cname'];
            $start = $rows[0]['start_date'];
            $expired = date('Ymd', strtotime(strtr($rows[0]['deadline_time'], '/', '-')));
        }
        else 
        {
            $x = $policy_no;
            $customer_name ="";
            $start = date("Y-m-d");
            $expired = date("Y-m-d", mktime(0, 0, 0, date("m") + 6, date("d"), date("Y")));
        }
        
        $st_res = response_message($stcode);
        $st_date = date("Ymdhis");
        //$st_signature = sha1(strtolower("$agent_id$agent_pin$agent_trxid$agent_storeid$product_id$customer_id$datetime_request$stcode$st_res$st_date$policy_no$expired_res") . $secretkey);
        $arr_result = array(                
            $agent_id, $agent_pin, $agent_trxid, $agent_storeid,$customer_id, $datetime_request,$stcode, $st_res,$st_date, $pay_period,$customer_name,$start."#".$expired."#"."Tunai 9 juta dan Voucher Alfamart 6 juta",
            $expired,$amount,$charge,$total,$product_id,$quantity_trx
        );
        $message = implode("|", $arr_result);
        $this->response($message);
    }

    public function index_post() {
        $agent_id = $this->post('AgentID');
        $agent_pin = $this->post('AgentPIN');
        $agent_trxid = $this->post('AgenttrxID');
        $agent_storeid = $this->post('AgentstoreID');
        $product_id = $this->post('ProductID');
        $customer_id = $this->post('CustomerID'); //KTP NO
        $datetime_request = $this->post('DatetimeRequest');
        $signature = $this->post('Signature');
        $is_valid = 0;
        $policy_no = "";
        $stcode = 0;
        $policy_no = 0;

        $arr_result = array(
            $agent_id, $agent_pin, $agent_trxid, $agent_storeid, $product_id, $customer_id, $datetime_request, $stcode,
            response_message($stcode), date("Ymdhis"), $policy_no
        );
        $message = implode("|", $arr_result);
        $this->set_response($message, REST_Controller::HTTP_CREATED);
    }
}