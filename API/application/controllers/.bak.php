<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Payments extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model("customer_model", "customer");
        $this->load->model('customer_model');
    }

    public function index_get() {

       $agent_id = $this->get('AgentID');
        $agent_pin = $this->get('AgentPIN');
        $agent_trxid = $this->get('AgentTrxID');
        $agent_storeid = $this->get('AgentStoreID');
        $product_id = $this->get('ProductID');
        $customer_id = $this->get('CustomerID');
        $datetime_request = $this->get('DateTimeRequest');

        //http://110.5.109.166:8073/Payment?AgentID=Alfamart&AgentPIN=ee8a10a9&AgentTrxID=13359&AgentStoreID=K329&ProductID=ACL&CustomerID=3171011203760001&PaymentPeriod=18&Amount=500000&Charge=0&Total=500&AdminFee=0&DateTimeRequest=20170116181504&Signature=a77a350d2eff86384f0e49f9eb3633486f154fb6
        $pay_period = $this->get('PaymentPeriod');
        $amount = $this->get('Amount');
        $charge = $this->get('Charge');
        $total = $this->get('Total');
        $admin_fee = $this->get('AdminFee');
        $additional_customer_info = "Selamat Anda sudah terproteksi Segera lengkapi data anda di www.asuransi.capitallife.co.id (+6221-2902-3688)"; 
        $quantity_trx = "";
        //var_dump(randomString(8));
        $reff_code = randomString(8);
        $secretkey = "AlfaG4t3";
        $signature = $this->get('Signature');
        $expired_res = "000000";
        $policy_no = "0"; //nomer polis
        $stcode = "00";
          $stval = sha1("$agent_id$agent_pin$agent_trxid$agent_storeid$product_id$customer_id$datetime_request$pay_period$amount$charge$total$admin_fee$secretkey");
        if($customer_id == ""){
                        redirect("http://110.5.109.166:8073");}
        if ($customer_id != '' && ($stval == $signature)) {
            //validasi KTP, valid KTP 16digit
            $ktp_len = strlen(trim($customer_id));
            //die($ktp_len);
            if ($ktp_len != 16) {
                $stcode = "12";
            } 
            else {
                $rows = $this->customer_model->get_cos_byid($customer_id);
                $row = $this->customer_model->get_customer($customer_id);
                $stcode = "";

                if ($row && $rows[0]['payment_status'] == 1) {//Jika no ktp ada didatabase dan status==0
                    //policy sudah terdaftar dan sudah bayar
                    $stcode = 88;
                    $policy_no = $row['policy_no'];
                }
//              else if ($row && $rows[0]['payment_status'] == 2) {//Jika no ktp ada didatabase dan status==0
//            //policy sudah terdaftar dan sudah bayar
//              $stcode = 88;
//              $policy_no = $row['policy_no'];
//               }
                
                else if ($row && $rows[0]['payment_status'] == 0){
                //die("dorr");
                $row = $this->customer->get_customer($customer_id);
                //$rows = $this->customer->get_cos_byid();
                $stcode = "";
                
                    $dt_dob = get_dob_date($customer_id);
                    $st_gender = get_gender($customer_id);
                    //curdate - 6 bulan -> 18 s/d 64
                    $start_req = date("Ymd", mktime(0, 0, 0, date("m") - 6, date("d"), date("Y")));
                    $int_age = 0;
                    $sql = "SELECT YEAR((FROM_DAYS(TO_DAYS('$start_req') - TO_DAYS('$dt_dob')))) AS umur_req, YEAR((FROM_DAYS(TO_DAYS(curdate()) - TO_DAYS('$dt_dob')))) AS umur";
                    //die($sql);
                    $query = $this->db->query($sql);

                    $row = $query->row_array();
                    $int_age_req = $row['umur_req'];
                    $int_age = $row['umur'];

                    //screening usia 18-64
                    //die($int_age_req);
                    if ($int_age_req < 18 || $int_age_req > 64) {
                        $stcode = 1;
                    }
                    //die('d'.$stcode);
                    //registrasi polis
                    if ($customer_id != '' && $stcode == 0) {
                        //proses insert
                        $expired = date("Y-m-d", mktime(0, 0, 0, date("m") + 6, date("d"), date("Y")));
                        $expired_res = date("Ymd", mktime(0, 0, 0, date("m") + 6, date("d"), date("Y")));

                        $arrdata = array(
                            "agent_pin" => $agent_pin,
                            "agent_id" => $agent_id,
                            "agent_trxid" => $agent_trxid,
                            "ktp_no" => $customer_id,
                            "gender" => $st_gender,
                            "dob" => $dt_dob,
                            "payment_status" =>1,
                            "age" => $int_age,
                            "reff_code_pay" => $reff_code,
                            "expired" => $expired,
                            
                            "amount" => $amount,
                            "charge" => $charge,
                            "admin_fee" =>$admin_fee,
                            "total" => $total,
                            "pay_period" => $pay_period,
                            "additional_customer_info" =>"Selamat Anda sudah terproteksiSegera lengkapi data anda di www.asuransi.capitallife.co.id (+6221-2902-3688)
                            ",
                            "created" => atom_date()
                        );
                        $hist = array(
                            "ktp_no" => $customer_id,
                            "keterangan" => "PAYMENT DATA SUCCESS",
                        );
                        $this->db->insert("api_activity_history", $hist);
                        $where = array(
                            "ktp_no" => $customer_id
                        );
                        $this->db->update("customers", $arrdata, $where);

                        $stcode = "00"; //sukses
                    }
            }
            else
            {
                $stcode = 98;
            }
             }
            //var_dump($row);
            //die();    			
        } else {
            $stcode = 30; //error lainnya
        }
        
        //die('sdd'.$policy_no);
        //Alfamart|ee8a10a9|10|K100|9999928384785|20120910181504|00|TransaksiSukses|20120910181505|18|BudiWahono|BEAT CW#B1111CDE#jl MH Thamrin Tangerang|20120928|500000|0|500000|paketA|1|    		
        //Agent ID|Agent PIN|Agent trx ID|Agent store ID|Customer ID|DatetimeRequest|Response Code|Response Desc|Datetime Resp|Payment Period|Customer Name|CustomerInformation|Deadline Time|Amount|Charge|Total|Product ID|QtyTrx
        //generate signature
        
        $row = $this->customer_model->get_customer($customer_id);
        $rows = $this->customer_model->get_cos_byid($customer_id);
        if($row)
        {
            $x = $rows[0]['policy_no'];
            $customer_name = $rows[0]['cname'];
            $reff_code = $rows[0]['policy_no'];
            $expired = $rows[0]['expired'];
            
            //$expired = date('Ymd', strtotime(strtr($rows[0]['expired'], '/', '-')));
        }
        else 
        {
            $x = $policy_no;
            $customer_name ="";
        }
        
        $st_res = response_message($stcode);
        $st_date = date("Ymdhis");
        
        //die("$agent_id$agent_pin$agent_trxid$agent_storeid$customer_id$datetime_request$stcode$st_res$st_date$policy_no");

        //$st_signature= sha1(strtolower("$agent_id$agent_pin$agent_trxid$agent_storeid$product_id$pay_period$amount$charge$total$admin_fee$customer_id$datetime_request") . $secretkey);
        $arr_result = array(
            $agent_id, $agent_pin, $agent_trxid, $agent_storeid, $customer_id, $datetime_request,
            $pay_period,$amount,$charge,$total, $admin_fee,$stcode, $st_res,$st_date, $reff_code,
            $additional_customer_info,$product_id
             
        );
        $message = implode("|", $arr_result);

        //$this->response('My first API response = GET METHOD');
        $this->response($message);
    }

    public function index_post() {
        $agent_id = $this->post('AgentID');
        $agent_pin = $this->post('AgentPIN');
        $agent_trxid = $this->post('AgenttrxID');
        $agent_storeid = $this->post('AgentstoreID');
        $product_id = $this->post('ProductID');
        $customer_id = $this->post('CustomerID'); //KTP NO
        $datetime_request = $this->post('DatetimeRequest');
        $signature = $this->post('Signature');
        $is_valid = 0;
        $policy_no = "";
        $stcode = 0;
        $policy_no = 0;
        $arr_result = array(
            $agent_id, $agent_pin, $agent_trxid, $agent_storeid, $customer_id, $datetime_request, $stcode,
            response_message($stcode), date("Ymdhis"), $policy_no
        );
        $message = implode("|", $arr_result);
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }
    
    
    
}
