<?php

/**
 * Gets atom standard time.
 * @param string $time[optional] The time value, or null to get current system time.
 * @return the atom standard time.
 */
function atom_date($time = null)
{
  if($time === null) $time = time();
  return standard_date('DATE_ATOM', $time);
}

function randomString($length) {
	$str = "";
	$characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
	$max = count($characters) - 1;
	for ($i = 0; $i < $length; $i++) {
		$rand = mt_rand(0, $max);
		$str .= $characters[$rand];
	}
	return $str;
}

/**
 * DOB Data
 */
function get_dob_date($ktp_no)
{
	$st_date = substr($ktp_no,6,6);
	//die($st_date);
	
	$dd = get_dob_day(substr($st_date,0,2));
	$mm = substr($st_date,2,2);
	$yy = get_dob_year(substr($st_date,4,2));
	
	$st_dob = "$yy-$mm-$dd";
	
	//die($st_date . '---' . $yy);
	//die($st_dob);
	
	return $st_dob;
}

/**
 * Tahun Lahir, jika lebih kecil dari tahun current, maka tahun lahir di atas 2000
 */
function get_dob_year($yy)
{
	$ret = "";
	$year_now = date("y");
	if($yy <=  $year_now)
		$ret = "20$yy";
	else
		$ret = "19$yy";
	
	return $ret;
}

/**
 * Tgl Lahir, Jika P tgl lahir dikurangi 40
 */
function get_dob_day($dd)
{
	$ret = "";
	if($dd < 31)
		$ret = $dd;
	else
	{
		$ret = $dd - 40;
	}
				
	return $ret;
}

/**
 * Penentuan Jenis Kelamin
 */
function get_gender($ktp_no)
{
	$icont = 40;
	$dd = substr($ktp_no,6,2);
	//die($dd);
	if($dd <= 31)
		$stval = "L";
	else
		$stval = "P";
		
	return $stval;	
}
/**
 * Pads given number.
 * @param object $number The number to pad.
 * @param int $n How many characters.
 * @param string $leading_char[optional] The character used to pad.
 * @return string Padded given number.
 */
function number_pad($number, $n, $leading_char = '0')
{
  return str_pad((int)$number, $n, $leading_char, STR_PAD_LEFT);
}
/**
 * Keterangan Validasi
 */
function reason_valid($intval, $policy_no)
{
	$res = "";
	switch($intval)
	{
		case 0:
			$res = "Selamat Anda sudah terproteksi Segera lengkapi data anda di www.alfa.capitallife.co.id";
		break;
		case 1:
			$res = "USIA TIDAK VALID,USIA YANG DIPERBOLEHKAN ANTARA 18-64 TAHUN";
		break;
		case 2:
			$res = "DATA POLIS MASIH AKTIF DAN SUDAH PERNAH TERDAFTAR";
		break;
		case 3:
			$res = "ERROR - No KTP Salah";
		break;
                
		default:
		break;
	}
	
	return $res;
}

function payment_status($data)
{
	$pay = "";
	switch($data)
	{
		case 0:
		$pay = "BELUM BAYAR";
		break;
            
		case 1:
		$pay = "LUNAS";
		break;
            
                case 2:
		$pay = "REVERSAL";
		break;

	}
	return $pay;
}


/** 
 * Response Code & Message
 */

function response_message($ival)
{
	//die('d'.$ival);
	$msg = "";
	switch($ival)
	{
		case 00:
			$msg = "TRANSAKSI SUKSES";
		break;
		case 1:
			$msg = "USIA TIDAK VALID,USIA YANG DIPERBOLEHKAN ANTARA 18-64 TAHUN";
		break;
		case 2:
			$msg = "DATA POLIS MASIH AKTIF DAN SUDAH PERNAH TERDAFTAR";
		break;
                case 3:
			$msg = "DATA POLIS BELUM AKTIF DAN SUDAH PERNAH TERDAFTAR";
		break;
                
		case 5:
			$msg = "ERROR - Lainnya";
		break;
		case 6:
			$msg = "ERROR - No KTP Sudah Terdaftar";
		break;
		case 12:
			$msg = "ERROR - No KTP Salah";
		break;		
		case 30:
			$msg = "ERROR - Signature Invalid";
		break;
		case 31:
			$msg = "ERROR - Kode Agen tidak terdaftar";
		break;
		case 33:
			$msg = "ERROR - Produk tidak terdaftar";
		break;
		case 40:
			$msg = "ERROR - Pembayaran terakhir hanya bisa dilakukan di loket Provider";
		break;
		case 41:
			$msg = "ERROR - 2 Pembayaran terakhir hanya bisa dilakukan di loket Provider";
		break;
		case 63:
			$msg = "ERROR - Tidak ada pembayaran";
		break;
		case 68:
			$msg = "ERROR - Timeout";
		break;
		case 88:
			$msg = "ERROR - Masa asuransi anda masih aktif";
		break;
		case 90:
			$msg = "ERROR - Sedang proses Cut-off";
		break;
		case 92:
			$msg = "ERROR - Provider Receipt Reference Number tidak tersedia";
		break;
		case 93:
			$msg = "ERROR - Switcher Trace Audit Number Salah (invalid)";
		break;
		case 97:
			$msg = "ERROR - Original data element salah (invalid)";
		break;
                case 04:
			$msg = "REVERSAL";
		break;
                case 98:
			$msg = "ERROR - Ktp tidak terdaftar";
		break;
                case 99:
			$msg = "ERROR - Masa asuransi expired";
		break;
               
	}
	
	return $msg;
}