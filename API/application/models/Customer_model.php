<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Customer_model extends CI_Model {

    public $ktp_no;

    public function get_customer($customer_id) {
        $this->ktp_no = $customer_id;
        $this->db->whare("payment_status =", "1");
        $this->db->where("ktp_no", trim($this->ktp_no));
        $this->db->where("expired >", date("Y-m-d"));
        $q = $this->db->get("customers");
        $row = array();
        if ($q->num_rows() > 0) {
            $row = $q->row_array();
        }
        $q->free_result();
        return $row;
    }

    public function get_cos_byid($customer_id) {
        $this->ktp_no = $customer_id;
        $sql = "SELECT * FROM customers WHERE ktp_no =".$this->ktp_no;
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }

}
